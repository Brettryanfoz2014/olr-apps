
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Schools;
using InAppService;

namespace AssessmentAlert.Droid
{
	public class BuySubscriptionView : LinearLayout
	{
		LayoutInflater inflater;
		LinearLayout llMainView;

		Button _btnBuy;
		TextView _tvPurchaseStatus;

		Context _context;
		InAppBillingHelper _billing;

		public BuySubscriptionView (Context context, InAppBillingHelper billing, IAttributeSet attrs) : base(context, attrs)
		{
			this._context = context;
			_billing = billing;

			this.Orientation = Android.Widget.Orientation.Vertical;
			
			inflater = (LayoutInflater)context.GetSystemService (Context.LayoutInflaterService);
					
			llMainView = (LinearLayout)inflater.Inflate (Resource.Layout.SubscriptionRequiredLayout, null);
			AddView (llMainView);

			// Get the controls from the layout
			_btnBuy = llMainView.FindViewById<Button> (Resource.Id.btnBuySubscription);
			_btnBuy.Click += HandlePurchase;

			_tvPurchaseStatus = llMainView.FindViewById<TextView> (Resource.Id.tvPurchaseStatus);
		}

		AssessmentSearcher _searcher;

		public void SetAssessmentSearcher(AssessmentSearcher searcher)
		{
			_searcher = searcher;
		}

		void HandlePurchase (object sender, EventArgs e)
		{
			_billing.LaunchPurchaseFlow(ProductNames.AnnualSubscription, ItemType.InApp, Guid.NewGuid().ToString());
		}

		void SetPurchaseStatus(string status)
		{
			_tvPurchaseStatus.Text = status;
			_tvPurchaseStatus.Visibility = ViewStates.Visible;
		}
	}
}