using System;
using Google.GData.Calendar;
using Google.GData.AccessControl;
using Google.GData.Client;
using Google.GData.Extensions;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace GData.Fascades
{
	public class CalendarFacade
	{
		CalendarService _calendarService;
		string _username;
		string _pwd;
		IEnumerable<string> _eventTitles;
		
		public CalendarFacade (string applicationName, string username, string pwd)
		{
			_username = username;
			_pwd = pwd;
			
			ServicePointManager.CertificatePolicy = new Trustee ();
			
			_calendarService = new CalendarService (applicationName);
			_calendarService.setUserCredentials (_username, _pwd);
		}
		
		class Trustee : ICertificatePolicy
		{
			public bool CheckValidationResult (ServicePoint sp, X509Certificate certificate, WebRequest request, int error)
			{
				return true;
			}
		}
		
		public IEnumerable<string> Events
		{
			get
			{
				if (_eventTitles == null)
				{
					string calUri = "http://www.google.com/calendar/feeds/default/private/full";
					
					EventQuery calQuery = new EventQuery (calUri);
					
					EventFeed calEvents = (EventFeed)_calendarService.Query (calQuery);


					_eventTitles = calEvents.Entries.Select (calEventEntry => calEventEntry.Title.Text).ToList ();
				}
				
				return _eventTitles;
			}
		}

		public Dictionary<string, CalendarEntry> GetCalendars()
		{
			Dictionary<string, CalendarEntry> result = new Dictionary<string, CalendarEntry>();
			CalendarQuery query = new CalendarQuery();

			query.Uri = new Uri("https://www.google.com/calendar/feeds/default/owncalendars/full");
			CalendarFeed feed = (CalendarFeed) _calendarService.Query(query);

			bool stillHasEntries = true;

			do
			{
				foreach (CalendarEntry entry in feed.Entries)
				{
					string calendarName = entry.Title.Text;
					result[calendarName] = entry;
				}
				
				if (!String.IsNullOrEmpty(feed.NextChunk))
				{
					query.BaseAddress = feed.NextChunk;
					stillHasEntries = true;
				}
				else
					stillHasEntries = false;
				
			} while (stillHasEntries);

			return result;
		}

		public string GetFeedForCalendar(CalendarEntry cal)
		{
			var absUri = cal.Id.AbsoluteUri;
			var index = absUri.LastIndexOf('/');
			var feedstring = absUri.Substring(index+1);
			return "https://www.google.com/calendar/feeds/" + feedstring + "/private/full";
		}

		public bool IsEditable(CalendarEntry cal)
		{
			return cal.ReadOnly == false;
		}

		public void GetEntriesWithDateRange(out EventQuery query, CalendarEntry calendar, 
		                                    DateTime fromDate, DateTime toDate, string searchTerm, 
		                                    EventFeedHandler getFeedHandler,
		                                    EventFeedIteratorHandler handler)
		{
			query = new EventQuery(GetFeedForCalendar(calendar));
			query.StartTime = fromDate;
			query.EndTime = toDate;
			query.NumberToRetrieve = 5;
			if (searchTerm != "")
				query.Query = searchTerm;

			bool stillHasEntries = true;
			var feed = _calendarService.Query(query);

			if (getFeedHandler != null)
				getFeedHandler(feed);

			do
			{
				foreach (EventEntry singleEntry in feed.Entries)
					handler(singleEntry);
				
				if (!String.IsNullOrEmpty(feed.NextChunk))
				{
					query.BaseAddress = feed.NextChunk;

					try
					{
					feed = _calendarService.Query(query);
					} catch (Exception calExc) {
						Console.WriteLine("Could not query the next step in the calendar: " + calExc.Message);
					}
					stillHasEntries = true;
				}
				else
					stillHasEntries = false;
				
			} while (stillHasEntries);
		}

		public void CreateEvent (CalendarEntry calendar, string Title, string Description, string EventLocation, DateTime startDate, DateTime endDate)
		{
			EventEntry entry = new EventEntry ();
			
			// Set the title and content of the entry.
			entry.Title.Text = Title;
			entry.Content.Content = Description;
			
			// Set a location for the event.
			if (!String.IsNullOrEmpty (EventLocation)) {
				Where eventLocation = new Where ();
				eventLocation.ValueString = EventLocation;
				entry.Locations.Add (eventLocation);
			}

			When eventTime = new When(startDate, endDate);
			entry.Times.Add(eventTime);

			Uri postUri = new Uri(calendar.FeedUri);
			
			// Send the request and receive the response:
			AtomEntry insertedEntry = _calendarService.Insert(postUri, entry);
		}

		public void CreateAllDayEvent (CalendarEntry calendar, string Title, string Description, string EventLocation, DateTime startDate, DateTime endDate)
		{
			EventEntry entry = new EventEntry ();
			
			// Set the title and content of the entry.
			entry.Title.Text = Title;
			entry.Content.Content = Description;
			
			// Set a location for the event.
			if (!String.IsNullOrEmpty (EventLocation)) {
				Where eventLocation = new Where ();
				eventLocation.ValueString = EventLocation;
				entry.Locations.Add (eventLocation);
			}
			
			When eventTime = new When(startDate, endDate, true);
			entry.Times.Add(eventTime);

			Uri postUri = new Uri(calendar.FeedUri);
			
			// Send the request and receive the response:
			AtomEntry insertedEntry = _calendarService.Insert(postUri, entry);
		}

		public void DeleteEvent(EventEntry item)
		{
			item.Delete();
		}

		DateTime Get5pmClosestTo (DateTime newDate)
		{
			return newDate.Date.AddDays (-1).AddHours (12 + 5);
		}

		public bool InsertEvents(EventFeed originalFeed, CalendarEntry calendar, SimpleCalendarEntry[] entries, int[] reminderDays)
		{
			if (entries.Length == 0)
				return true;

			AtomFeed batchFeed = new AtomFeed(originalFeed);
			
			int currentNumber = 1;
			foreach (var e in entries)
			{
				EventEntry toCreate = new EventEntry(e.Title);
				toCreate.Content.Content = e.Description;
				if (!String.IsNullOrEmpty(e.Location))
					toCreate.Locations.Add(new Where("", "", e.Location));

				toCreate.Times.Add(new When(e.StartDate, e.EndDate, e.AllDay));

				// Add the reminders
				foreach (int dayIncrement in reminderDays)
				{
					var newDate = e.StartDate.AddDays(dayIncrement);
					var r = new Reminder();
					r.Method = Reminder.ReminderMethod.alert;
					r.AbsoluteTime = Get5pmClosestTo (newDate);
					//r.Minutes = 60 * 24 * dayIncrement;
					toCreate.Reminders.Add(r);
				}

				toCreate.BatchData = new GDataBatchEntryData(currentNumber.ToString(), GDataBatchOperationType.insert);
				batchFeed.Entries.Add(toCreate);
			}
			
			EventFeed batchResultFeed = (EventFeed)_calendarService.Batch(batchFeed, new Uri(originalFeed.Batch));
			
			//check the return values of the batch operations to make sure they all worked.
			//the insert operation should return a 201 and the rest should return 200
			foreach (EventEntry entry in batchResultFeed.Entries)
			{
				if (entry.BatchData.Status.Code != 200 && entry.BatchData.Status.Code != 201)
				{
					return false;
					//Console.WriteLine("The batch operation with ID " + entry.BatchData.Id + " failed.");
				}
			}
			
			return true;
		}

		public bool DeleteEvents(EventFeed originalFeed, CalendarEntry calendar, EventEntry[] events)
		{
			if (events.Length == 0)
				return true;

			AtomFeed batchFeed = new AtomFeed(originalFeed);

			int currentNumber = 1;
			foreach (var e in events)
			{
				EventEntry toDelete = (EventEntry)e;

				toDelete.Id = new AtomId(toDelete.EditUri.ToString());
				toDelete.BatchData = new GDataBatchEntryData(currentNumber.ToString(), GDataBatchOperationType.delete);

				batchFeed.Entries.Add(toDelete);
			}

			EventFeed batchResultFeed = (EventFeed)_calendarService.Batch(batchFeed, new Uri(originalFeed.Batch));
			
			//check the return values of the batch operations to make sure they all worked.
			//the insert operation should return a 201 and the rest should return 200
			foreach (EventEntry entry in batchResultFeed.Entries)
			{
				if (entry.BatchData.Status.Code != 200 && entry.BatchData.Status.Code != 201)
				{
					return false;
					//Console.WriteLine("The batch operation with ID " + entry.BatchData.Id + " failed.");
				}
			}

			return true;
		}
	}

	public class SimpleCalendarEntry
	{
		public string Title;
		public string Description;
		public string Location;
		public DateTime StartDate;
		public DateTime EndDate;
		public bool AllDay;
	}

	public delegate void EventFeedIteratorHandler(EventEntry entry);
	public delegate void EventFeedHandler(EventFeed feed);
}

