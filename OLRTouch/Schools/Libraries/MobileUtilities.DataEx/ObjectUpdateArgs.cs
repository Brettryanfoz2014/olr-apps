using System;
using System.Collections.Generic;

namespace MobileUtilities.Data
{
	public enum DataObjectOperation
	{
		Insert,
		Update,
		Delete
	}
	
	public class DataObjectUpdateArgs
	{
		public DataObjectUpdateArgs (object item, DataObjectOperation operation)
		{
			this.Item = item;
			this.Operation = operation;
		}
		
		public object Item { get; set; }
		public DataObjectOperation Operation { get; set; }
	}
	
	public interface IDataObjectUpdate
	{
		void DataObjectNotify(object sender, DataObjectUpdateArgs args);
	}
	
	public class DataObjectObservers: List<IDataObjectUpdate>
	{
		object _sender;
		
		public DataObjectObservers (object sender) : base()
	
		{
			_sender = sender;
		}
		
		public void Notify (object item, DataObjectOperation operation)
		{
			DataObjectUpdateArgs args = new DataObjectUpdateArgs (item, operation);
			
			foreach (IDataObjectUpdate i in this)
				i.DataObjectNotify (_sender, args);
		}
	}
	
	/// <summary>
	/// Data object propogator. It handles requests from objects and send notifications through to others
	/// </summary>
	public class DataObjectPropogator 
	{
		public DataObjectPropogator ()
		{
			observers = new DataObjectObservers(this);
		}
		
		public void Notify (object item, DataObjectOperation operation)
		{
			// Receive Notifications and send through details to those
			observers.Notify(item, operation);
		}
		
		public DataObjectObservers observers = null;
	}
}

