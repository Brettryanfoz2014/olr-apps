using System;

namespace MobileUtilities.Data
{
	public class PathHelper
	{
		public static string GetDocumentsRelativePath (string Filename)
		{
			// The Path may come in like this: /private/var/mobile/Applications/D77E8C19-FADB-4C17-859E-F9EF8AF940C8/Documents/Processes/a82ff5eae35843d28648c247a9ee5e91.orchardprocess
			// This is especially important for an upgrade of an app
			if (System.IO.Path.IsPathRooted (Filename)) {
				// We need to get the real name here
				var folderName = "/Documents/";
				var index = Filename.IndexOf ("/Documents/");
				var relPath = Filename.Substring (index + folderName.Length);
				
				var documents = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
				
				return System.IO.Path.Combine (documents, relPath);
			}

			return Filename;
		}

		public static string GetRelativePathPart (string Filename)
		{
			// The Path may come in like this: /private/var/mobile/Applications/D77E8C19-FADB-4C17-859E-F9EF8AF940C8/Documents/Processes/a82ff5eae35843d28648c247a9ee5e91.orchardprocess
			// This is especially important for an upgrade of an app
			if (System.IO.Path.IsPathRooted (Filename)) {
				// We need to get the real name here
				var folderName = "/Documents/";
				var index = Filename.IndexOf ("/Documents/");
				Filename = Filename.Substring (index + folderName.Length);
			}
			return Filename;
		}

	}
}

