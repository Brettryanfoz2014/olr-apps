// WARNING
//
// This file has been generated automatically by MonoDevelop to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using UIKit;

namespace MobileUtilities.Forms
{
	[Register ("BaseSearcher")]
	partial class BaseSearcher
	{
		[Outlet]
		UISearchBar searcher { get; set; }

		[Outlet]
		UITableView tableview { get; set; }

		[Outlet]
		UIView overlayView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (searcher != null) {
				searcher.Dispose ();
				searcher = null;
			}

			if (tableview != null) {
				tableview.Dispose ();
				tableview = null;
			}

			if (overlayView != null) {
				overlayView.Dispose ();
				overlayView = null;
			}
		}
	}
}
