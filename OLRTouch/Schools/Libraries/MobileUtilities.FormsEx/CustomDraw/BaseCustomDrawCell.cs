using System;
using UIKit;
using Foundation;
using RectangleF = global::CoreGraphics.CGRect;

namespace MobileUtilities.Forms
{
	public class BaseCustomDrawCell : UITableViewCell 
	{
		public BaseCustomDrawCell () : base()
		{
		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);

			// Draw the graphics based on what is needed

		}
	}

	public class EventBasedCustomDrawCell : BaseCustomDrawCell
	{
		public Entry ItemEntry = null;

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);

			//if (ItemEntry != null && ItemEntry.CustomDraw != null)
			//	ItemEntry.CustomDraw(this, EventArgs.Empty);
		}
	}


}

