using System;
using UIKit;
using Foundation;
using System.Drawing;

namespace MobileUtilities.Forms
{
	public enum DateTimeViewControllerMode
	{
		Date,
		DateTime,
		Time
	}
	
	public enum SelectDateFormPart
	{
		Date,
		Time
	}	
}

