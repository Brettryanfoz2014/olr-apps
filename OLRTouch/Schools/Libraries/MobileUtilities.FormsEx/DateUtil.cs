﻿using System;
using Foundation;

namespace MobileUtilities.Forms
{
	public class DateUtil
	{
		public static DateTime NSDateToDateTime(NSDate date){
			double secs = date.SecondsSinceReferenceDate;
			if (secs < -63113904000)
				return DateTime.MinValue;
			if (secs > 252423993599)
				return DateTime.MaxValue;
			return (DateTime)date;
		}

		public static NSDate DateTimeToNSDate(DateTime date){
			if (date.Kind == DateTimeKind.Unspecified)
				date = DateTime.SpecifyKind (date, DateTimeKind.Local);
			return (NSDate)date;
		}
	}
}

