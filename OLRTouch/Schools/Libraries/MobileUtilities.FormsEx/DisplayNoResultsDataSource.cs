using System;
using UIKit;
using Foundation;

namespace MobileUtilities.Forms
{
	public class DisplayNoResultsDataSource : UITableViewDataSource
	{
		string _message = "No results found";
		
		public DisplayNoResultsDataSource (string message) : base()
		{
			_message = message;
		}
		
		public override nint RowsInSection (UITableView tableView, nint section)
		{
			return 3;
		}
		
		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}
			
		static NSString cellIdentifier = new NSString("NoResultsFoundCellIdent");
		
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(cellIdentifier);
			if (cell == null)
				cell = new UITableViewCell(UITableViewCellStyle.Default, cellIdentifier);
			
			if (indexPath.Row == 2)
			{
				cell.TextLabel.Text = _message;
				cell.TextLabel.TextAlignment = UITextAlignment.Center;
				cell.TextLabel.TextColor = UIColor.Gray;
			} else {
				cell.TextLabel.Text = "";
			}
			
			cell.Accessory = UITableViewCellAccessory.None;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			Theme.ApplyThemeToCell(cell);

			return cell;
		}
	}
}

