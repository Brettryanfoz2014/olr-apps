
using System;
using UIKit;
using Foundation;

namespace MobileUtilities.Forms
{
	public class DoNothingTableDelegate : UITableViewDelegate
	{
		public DoNothingTableDelegate(UITableViewController tvc)
		{
		}
		
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			// Do nothing while it is loading
		}
	}
}
