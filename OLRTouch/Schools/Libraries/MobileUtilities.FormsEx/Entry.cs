using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if __UNIFIED__
using Foundation;
using CoreGraphics;
using UIKit;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;
#else
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
#endif

using MonoTouch.Dialog;

namespace MobileUtilities.Forms
{
	public enum EntryType
	{
		String,
		Boolean,
		Combo,
		Integer,
		Decimal,
		Currency,
		Memo,
		Email,
		URL,
		Date,
		DateTime,
		Time,
		Custom,
		WebLink,
		Button,
		Map,
		Contact,
		Navigate,
		CheckNavigate,
		Document,
		NameValue,
		Collapsible,
		ImageFolder,
		AudioFolder,
		Segment, 
		Rollup,
		AudioFile,
		UpDown,
		InfoText,
		Password,
		OnOff,
		Ad
	}
	
	public class Entry
	{
		#region Constructors for the various types
		
		public Entry (Section section, string label, EntryType eType, 
		              string[] options, string[] optionValues, string defaultVal,
		              string placeholder, bool isReadOnly)
		{
			this.section = section;
			this.Label = label;
			this.entryType = eType;
			this.Options = options;
			this.OptionValues = optionValues;
			this.DefaultValue = defaultVal;
			this.Placeholder = placeholder;
			this.IsReadOnly = isReadOnly;
		}
		
		public string Label { get; set; }
		public EntryType entryType { get; set; }
		public string[] Options  { get; set; }
		public string[] OptionValues { get; set; }
		public string[] Descriptions { get; set; }
		public string[] OptionImages { get; set; }
		public string DefaultValue { get; set; }
		public string Placeholder { get; set; }
		
		public string NavigateImage = null;
		public bool NoSelect = false;

		public int MinimumValue = 0;
		public int MaximumValue = Int32.MaxValue;
		
		public int CustomFontSize = -1;
		
		public bool HideNavigation = false;
		
		// Custom Delete used on the form
		public bool CanDelete = false;
		
		public UIColor ButtonColor = new UIColor (0.00f, 0.45f, 0.91f, 1);
		public UIColor ButtonHighlightColor = new UIColor (0.00f, 0.45f, 0.91f, 1);

		public void SetButtonTitle (string title, UIControlState state = UIControlState.Normal)
		{
			Label = title;

			if (HasButtonSetup())
				(control as GlassButtonHolder).mainButton.SetTitle(title, state);
		}

		private bool _isReadOnly = false;
		
		public bool IsReadOnly { 
			get {
				return _isReadOnly;
			} 
			set {
				if (_isReadOnly != value) {
					_isReadOnly = value;
					
					SetControlReadOnly(value);
				}
			}
		}
		
		public bool runiOSMaps { get; set; }
		public object Tag = null;
		public bool ShowSubItemCount { get; set; }
		public string SubItemFormat { get; set; }
		
		public GlassButton button = null;
		public int RowHeight = 4;
		
		public SizeF CellSize;
		public bool CellSizeSet = false;
		public SizeF InfoTextTitleSize, InfoTextDescriptionSize;
		
		public void SetSubItemDisplay (string format)
		{
			SubItemFormat = format;
			ShowSubItemCount = true;
		}
		
		public Section section { get; set; }
		
		private string _Value = "";
		
		public bool HasValue
		{
			get
			{
				return !IsEmpty;
			}
		}
		
		public bool IsEmpty
		{
			get
			{
				return Value == null || Value.Length == 0;
			}
		}
		
		public string ValueOrEmpty {
			get {
				if (IsEmpty)
					return "";
				else
					return Value;
			}
		}
		
		public void SetControlReadOnly (bool value)
		{
			if (control != null) {
				switch (entryType) {
				case EntryType.String:
				case EntryType.Integer:
				case EntryType.Decimal:
				case EntryType.Currency:
				case EntryType.Email:
				case EntryType.URL:
				case EntryType.Rollup:
				case EntryType.Password:
					(this.control as UITextField).Enabled = !value;
					break;
				case EntryType.Memo:
					(this.control as UITextView).Editable = !value;
					break;
				case EntryType.Segment:
					(this.control as UISegmentedControl).Enabled = !value;
					break;
				}
			}
		}
		
		public string Value {
			get {
				if (control == null)
					return _Value;
				
				switch (entryType) {
				case EntryType.String:
				case EntryType.Integer:
				case EntryType.Decimal:
				case EntryType.Currency:
				case EntryType.Email:
				case EntryType.URL:
				case EntryType.Rollup:
				case EntryType.Password:
					return (this.control as UITextField).Text;
				case EntryType.Date:
				case EntryType.DateTime:
				case EntryType.Time:
					return _Value;
				case EntryType.Memo:
					return (this.control as UITextView).Text;
				case EntryType.Segment:
					nint selectedIndex = (this.control as UISegmentedControl).SelectedSegment;
					if (selectedIndex >= 0) {
						return Options [selectedIndex];
					} else
						return "";
				case EntryType.OnOff:
					return (control as OnOffHolder).control.On ? "true" : "false";
				case EntryType.Boolean:
				case EntryType.Combo:
				case EntryType.NameValue:
				default:
					return _Value;
				}
			}
			set {
				string currentValue = this.Value;
				
				if (control == null) {
					if (value == null)
						_Value = "";
					else
						_Value = value;
				} else {
					switch (entryType) {
					case EntryType.String:
					case EntryType.Integer:
					case EntryType.Decimal:
					case EntryType.Currency:
					case EntryType.Email:
					case EntryType.URL:
					case EntryType.Date:
					case EntryType.DateTime:
					case EntryType.Time:
					case EntryType.Rollup:
					case EntryType.Password:
						(this.control as UITextField).Text = value;
						break;
					case EntryType.OnOff:
						(control as OnOffHolder).control.On = value.ToLower ().Equals ("true");
						break;
					case EntryType.Memo:
						(this.control as UIPlaceholderTextView).Text = value;
						(this.control as UIPlaceholderTextView).DoUpdate ();
						break;
					case EntryType.NameValue:
						(this.control as UILabel).Text = value;
						_Value = value;
						break;
					case EntryType.Segment:
						// Find the item position
						for (int counter = 0; counter < this.Options.Length; counter++) {
							if (this.Options [counter] == value) {
								(this.control as UISegmentedControl).SelectedSegment = counter;
								break;
							}
						}
						
						break;
					case EntryType.UpDown:
						_Value = value;
						break;
					case EntryType.Boolean:
					case EntryType.Combo:
					default:
						if (value == null)
							_Value = "";
						else
							_Value = value;
						break;
					}
				}
				
				DoValueChange ();
			}
		}
		
		public void SetupControlChangeEvent ()
		{
			if (control is UITextField) {
				UITextField f = control as UITextField;
				f.EditingChanged += HandleFValueChanged;
			}
		}

		void HandleFValueChanged (object sender, EventArgs e)
		{
			DoValueChange();
		}
		
		public object control = null;
		
		// For list based entries (Audio Files, Image Files)
		public List<object> ListEntries = new List<object>();
		
		public static Entry AddString (Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.String, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddPassword (Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.Password, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddCombo (Section section, string label, string defaultValue, string[] items, string placeholder, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.Combo, items, items, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddCombo (Section section, string label, string defaultValue, string[] items, 
		                              string[] images, string placeholder, bool isReadOnly)
		{
			var e = new Entry (section, label, EntryType.Combo, items, images, defaultValue, placeholder, isReadOnly); 
			e.OptionImages = images;
			return e;
		}
		
		public static Entry AddRollup (Section section, string label, string defaultValue, string[] items, string placeholder, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.Rollup, items, items, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddSegment (Section section, string[] items)
		{
			return new Entry (section, "", EntryType.Segment, items, null, "", "", false);
		}
		
		public static Entry AddInteger (Section section, string label, int defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.Integer, null, null, defaultValue.ToString (), placeholder, isReadOnly); 
		}
		
		public static Entry AddUpDown (Section section, string label, int defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.UpDown, null, null, defaultValue.ToString (), placeholder, isReadOnly); 
		}
		
		public static Entry AddiAd (Section section)
		{
			return new Entry (section, "", EntryType.Ad, null, null, "", "", false); 			
		}
		
		public static Entry AddPicturesFolder (Section section, string folder)
		{
			Entry result = new Entry (section, "Images ({0})", EntryType.ImageFolder, null, null, "", "", false);
			result.Value = folder;
			return result;
		}
		
		public static Entry AddAudioFolder (Section section, string folder)
		{
			Entry result = new Entry (section, "Audio Notes ({0})", EntryType.AudioFolder, null, null, "", "", false);
			result.Value = folder;
			return result;
		}
		
		public static Entry AddAudio (Section section, string pathToFile)
		{
			Entry result = new Entry (section, "", EntryType.AudioFile, null, null, "", "", false);
			result.Value = pathToFile;
			return result;
		}
		
		public static Entry AddDecimal(Section section, string label, Decimal defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.Decimal, null, null, defaultValue.ToString(), placeholder, isReadOnly); 
		}
		
		public static Entry AddCurrency(Section section, string label, Decimal defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.Currency, null, null, defaultValue.ToString(), placeholder, isReadOnly); 
		}
		
		public static Entry AddMemo(Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.Memo, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public List<Entry> SubEntries;
		
		public bool HasAddButton { get; set; }

		public bool ShowDetailDisclosure {
			get;
			set; 
		}
		
		public static Entry AddCollapsible (Section section, string title, bool isCollapsed, bool hasAddButton, Entry[] subItems)
		{
			Entry result = new Entry (section, title, EntryType.Collapsible, null, null, "", "", false);
			result.SubEntries = new List<Entry> (subItems);
			result.AsBoolean = !isCollapsed;
			result.HasAddButton = hasAddButton;
			return result;
		}
		
		public static Entry AddEmail(Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.Email, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddURL(Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.URL, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddDate(Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.Date, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddDateTime(Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.DateTime, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddTime (Section section, string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.Time, null, null, defaultValue, placeholder, isReadOnly); 
		}
		
		public static Entry AddInfoText (Section section, string label, string text)
		{
			Entry newEntry = new Entry (section, label, EntryType.InfoText, null, null, "", "", true);
			newEntry.Value = text;
			return newEntry;
		}
		
		public static Entry AddWebLink(Section section, string label, string url)
		{
			return new Entry(section, label, EntryType.WebLink, null, null, url, "", false);	
		}
		
		public static Entry AddMap(Section section, string label, bool runiOSMaps)
		{
			Entry e = new Entry(section, label, EntryType.Map, null, null, "", "", false);
			e.runiOSMaps = runiOSMaps;
			return e;
		}
		
		public static Entry AddNameValue (Section section, string label, string value)
		{
			Entry e = new Entry (section, label, EntryType.NameValue, null, null, value, value, true);
			e.Value = value;
			return e;
		}
		
		public static Entry AddButton(Section section, string label)
		{
			return new Entry(section, label, EntryType.Button, null, null, "", "", false);
		}
		
		public static Entry AddContact(Section section, string label)
		{
			return new Entry(section, label, EntryType.Contact, null, null, "", "", false);	
		}
		
		public static Entry AddNavigate (Section section, string label)
		{
			return new Entry (section, label, EntryType.Navigate, null, null, "", "", false);
		}
		
		public static Entry AddCheckNavigate (Section section, string label)
		{
			return new Entry(section, label, EntryType.CheckNavigate, null, null, "", "", false);
		}
		
		public static Entry AddDocument(Section section, string label, string document)
		{
			return new Entry(section, label, EntryType.Document, null, null, document, "", false);	
		}
		
		public static Entry AddBoolean(Section section, string label, string defaultValue, bool isReadOnly)
		{
			return new Entry(section, label, EntryType.Boolean, null, null, "", "", isReadOnly);	
		}
		
		public static Entry AddBoolean (Section section, string label, bool defaultValue, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.Boolean, null, null, defaultValue ? "true" : "false", "", isReadOnly);	
		}
		
		public static Entry AddOnOff (Section section, string label, bool defaultValue, bool isReadOnly)
		{
			return new Entry (section, label, EntryType.OnOff, null, null, defaultValue ? "true" : "false", "", isReadOnly);				
		}
		
		#endregion
		
		#region Helper functions to get the values as particular data types
		
		public string AsString 
		{
			get 
			{
				return Value;
			}
			set
			{
				Value = value;
			}
		}
		
		public int AsInteger
		{
			get {
				return Convert.ToInt32(Value);
			}
			set
			{
				Value = value.ToString();
			}
		}
		
		public string ValueOrDefault
		{
			get
			{
				if (IsEmpty)
					return Value;
				else
					return DefaultValue;
			}
		}
		
		public double AsDouble {
			get {
				return Convert.ToDouble (ValueOrDefault);
			}
			set {
				Value = value.ToString ();
			}
		}
		
		public double AsDoubleOrDefault {
			get {
				double result = 0;
				if (Double.TryParse (Value, out result))
					return result;
				else
					return Convert.ToDouble(DefaultValue);
			}
		}
		
		public Decimal AsDecimal
		{
			get {
				return Convert.ToDecimal(ValueOrDefault);
			}
			set
			{
				Value = value.ToString();
			}
		}
		
		public Decimal AsCurrency
		{
			get {
				return Convert.ToDecimal(ValueOrDefault);
			}
			set
			{
				Value = value.ToString();
			}
		}
		
		public DateTime AsDateTime {
			get {
				DateTime val;
				if (DateTime.TryParse (Value, out val)) {
					return val;
				} else 
					return DateTime.MinValue;
			}
			set {
				Value = value.ToString ();
			}
		}
		
		public bool AsBoolean
		{
			get {
				return Value.Equals("true");
			}
			set {
				Value = value ? "true" : "false";	
			}
		}
		
		public void UpdateValue(string newValue)
		{
			if (control != null)
			{
				// Update the control based on what it contains
				if (control is UITextField)
					(control as UITextField).Text = newValue;
				else if (control is UITextView)
					(control as UITextView).Text = newValue;
			}
			
			this.Value = newValue;
		}
		
		public void ClearValue ()
		{
			UpdateValue ("");	
		}
		
		public event EventHandler OnValueChange = null;
		
		public void DoValueChange ()
		{
			if (OnValueChange != null)
				OnValueChange(this, EventArgs.Empty);
		}
		
		#endregion
		
		#region Method Helpers
		
		public event EventHandler OnClick = null;
		
		public void DoClick ()
		{
			if (OnClick != null)
				OnClick (this, EventArgs.Empty);
		}
		
		public bool HasClickHandler ()
		{
			return OnClick != null;
		}
		
		public ImageCapture OnImageCapture = null;
		
		public void DoImageCapture (CapturedImage img)
		{
			if (OnImageCapture != null)
				OnImageCapture (this, img);
			
			
		}
		
		public event RetrieveAddress OnRetrieveAddress = null;
		
		public void DoRetrieveAddress(AddressDetailsArgs args)
		{
			if (OnRetrieveAddress != null)
				OnRetrieveAddress(this, args);
		}
		
		#endregion
		
		#region Handlers for the Collapsing of panels etc
		
		public void Collapse ()
		{
			foreach (Entry en in this.SubEntries)
				this.section.entries.Remove (en);
			
			this.AsBoolean = false;
		}
		
		public void Expand ()
		{
			// Expand and collapse all the other items
			int indexToInsertAt = this.section.entries.IndexOf (this) + 1;
			this.section.entries.InsertRange (indexToInsertAt, this.SubEntries);
		
			this.AsBoolean = true;
		}
		
		public bool IsExpanded {
			get {
				return this.AsBoolean;
			}
		}
		
		public void SetChecked ()
		{
			Placeholder = "tick";
		}
		
		public void ClearChecked ()
		{
			Placeholder = "";	
		}
		
		public void SetChecked (bool state)
		{
			if (state)
				SetChecked ();
			else
				ClearChecked ();
		}
		
		public void RecreateCollapsibleItems ()
		{
			if (IsExpanded) {
				Collapse ();
				Expand ();
			}
		}
		
		public bool IsChecked()
		{
			return Placeholder == "tick";
		}
		
		#endregion

		private bool HasButtonSetup()
		{
			if (control == null)
				return false;

			return control is GlassButtonHolder;
		}

		public bool InitialButtonState = true;

		public void EnableButton()
		{
			InitialButtonState = true;
			if (HasButtonSetup())
				(control as GlassButtonHolder).mainButton.Enabled = true;
		}

		public void DisableButton()
		{
			InitialButtonState = false;
			if (HasButtonSetup())
				(control as GlassButtonHolder).mainButton.Enabled = false;
		}
	}
	
	public enum AddressType
	{
		None,
		StreetAddress,
		GPS
	}
	
	/// <summary>
	/// Used to navigate to a position on a map
	/// </summary>
	public class AddressDetailsArgs
	{
		public AddressType addrType { get; set; }
		public string Location { get; set; }
		public string Note { get; set; }
	}
	
	public delegate void RetrieveAddress(object sender, AddressDetailsArgs args);
}

