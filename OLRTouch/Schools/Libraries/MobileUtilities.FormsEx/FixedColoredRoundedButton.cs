using System;

using UIKit;
using CoreGraphics;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;

namespace MonoTouch.Dialog
{
	public class FixedColoredRoundedButton : UIButton
	{
		bool pressed;
		public UIColor NormalColor, HighlightedColor, DisabledColor;

		/// <summary>
		/// Invoked when the user touches 
		/// </summary>
		public event Action<UIButton> Tapped;

		/// <summary>
		/// Creates a new instance of the GlassButton using the specified dimensions
		/// </summary>
		public FixedColoredRoundedButton (RectangleF frame) : base (frame)
		{
			NormalColor = UIColor.FromRGB (38, 59, 112);  
			HighlightedColor = UIColor.FromRGB (58, 90, 171);
			DisabledColor = UIColor.Gray;
			BackgroundColor = UIColor.Clear;
		}

		/// <summary>
		/// Whether the button is rendered enabled or not
		/// </summary>
		public override bool Enabled { 
			get {
				return base.Enabled;
			}
			set {
				base.Enabled = value;
				SetNeedsDisplay ();
			}
		}

		public override bool BeginTracking (UITouch uitouch, UIEvent uievent)
		{
			SetNeedsDisplay ();
			pressed = true;
			return base.BeginTracking (uitouch, uievent);
		}

		public override void EndTracking (UITouch uitouch, UIEvent uievent)
		{
			if (pressed && Enabled) {
				if (Tapped != null)
					Tapped (this);
			}
			pressed = false;
			SetNeedsDisplay ();
			base.EndTracking (uitouch, uievent);
		}

		public override bool ContinueTracking (UITouch uitouch, UIEvent uievent)
		{
			var touch = uievent.AllTouches.AnyObject as UITouch;
			if (Bounds.Contains (touch.LocationInView (this)))
				pressed = true;
			else
				pressed = false;
			return base.ContinueTracking (uitouch, uievent);
		}

		public override void Draw (RectangleF rect)
		{
			var context = UIGraphics.GetCurrentContext ();
			var bounds = Bounds;

			UIColor background = Enabled ? pressed ? HighlightedColor : NormalColor : DisabledColor;
			float alpha = 1;

			CGPath container;
			container = GraphicsUtil.MakeRoundedRectPath (bounds, 14);
			context.AddPath (container);
			context.SetFillColor (background.CGColor);
			context.FillPath ();
		}

		public FixedColoredRoundedButton MakeGrayFormat()
		{
			NormalColor = UIColor.FromRGB (234, 234, 234);  
			HighlightedColor = UIColor.FromRGB (241, 241, 241);
			DisabledColor = UIColor.Gray;
			BackgroundColor = UIColor.Clear;

			SetTitleColor (UIColor.FromRGB (68, 68, 68), UIControlState.Normal);

			return this;
		}
	}
}