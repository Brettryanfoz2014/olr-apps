using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MonoTouch.Dialog
{
	
	/// <summary>
	/// GlassButton is a glossy/glass button.   User code can use either
	/// targets or can subscribe to the Tapped event.  Colors are customized
	/// by asssigning to the NormalColor, HighlightedColor and DisabledColor
	/// properties
	/// </summary>
	public class GlassButton : UIButton
	{
		bool pressed;
		public UIColor NormalColor, HighlightedColor, DisabledColor;
		
		/// <summary>
		/// Invoked when the user touches 
		/// </summary>
		public event Action<GlassButton> Tapped;
				
		/// <summary>
		/// Creates a new instance of the GlassButton using the specified dimensions
		/// </summary>
		public GlassButton (RectangleF frame) : base (frame)
		{
			NormalColor = new UIColor (0.00f, 0.45f, 0.91f, 1);
			HighlightedColor = new UIColor (0.00f, 0.45f, 0.91f, 1); //UIColor.Black;
			DisabledColor = UIColor.Gray;
			BackgroundColor = UIColor.Clear;
		}

		/// <summary>
		/// Whether the button is rendered enabled or not
		/// </summary>
		public override bool Enabled { 
			get {
				return base.Enabled;
			}
			set {
				base.Enabled = value;
				SetNeedsDisplay ();
			}
		}
		
		public override bool BeginTracking (UITouch uitouch, UIEvent uievent)
		{
			SetNeedsDisplay ();
			pressed = true;
			return base.BeginTracking (uitouch, uievent);
		}
		
		public override void EndTracking (UITouch uitouch, UIEvent uievent)
		{
			if (pressed && Enabled) {
				if (Tapped != null)
					Tapped (this);
			}
			pressed = false;
			SetNeedsDisplay ();
			base.EndTracking (uitouch, uievent);
		}
		
		public override bool ContinueTracking (UITouch uitouch, UIEvent uievent)
		{
			var touch = uievent.AllTouches.AnyObject as UITouch;
			if (Bounds.Contains (touch.LocationInView (this)))
				pressed = true;
			else
				pressed = false;
			return base.ContinueTracking (uitouch, uievent);
		}
		
		public override void Draw (RectangleF rect)
		{
			var context = UIGraphics.GetCurrentContext ();
			var bounds = Bounds;
			
			UIColor background = Enabled ? pressed ? HighlightedColor : NormalColor : DisabledColor;
			nfloat alpha = 1;
			
			CGPath container;
			container = GraphicsUtil.MakeRoundedRectPath (bounds, 14);
			context.AddPath (container);
			context.Clip ();
			
			using (var cs = CGColorSpace.CreateDeviceRGB ()) {
				var topCenter = new PointF (bounds.GetMidX (), 0);
				var midCenter = new PointF (bounds.GetMidX (), bounds.GetMidY ());
				var bottomCenter = new PointF (bounds.GetMidX (), bounds.GetMaxY ());

				using (var gradient = new CGGradient (cs, new nfloat [] { 0.23f, 0.23f, 0.23f, alpha, 0.47f, 0.47f, 0.47f, alpha }, new nfloat [] {0, 1})) {
					context.DrawLinearGradient (gradient, topCenter, bottomCenter, 0);
				}
				
				container = GraphicsUtil.MakeRoundedRectPath (bounds.Inset (1, 1), 13);
				context.AddPath (container);
				context.Clip ();
				using (var gradient = new CGGradient (cs, new nfloat [] { 0.05f, 0.05f, 0.05f, alpha, 0.15f, 0.15f, 0.15f, alpha}, new nfloat [] {0, 1})) {
					context.DrawLinearGradient (gradient, topCenter, bottomCenter, 0);
				}
				
				var nb = bounds.Inset (4, 4);
				container = GraphicsUtil.MakeRoundedRectPath (nb, 10);
				context.AddPath (container);
				context.Clip ();
				
				background.SetFill ();
				context.FillRect (nb);
				
				using (var gradient = new CGGradient (cs, new nfloat [] { 1, 1, 1, .35f, 1, 1, 1, 0.06f }, new nfloat [] { 0, 1 })) {		
					
					context.DrawLinearGradient (gradient, topCenter, midCenter, 0);
				}
				context.SetLineWidth (1);
				context.AddPath (container);
				context.ReplacePathWithStrokedPath ();
				context.Clip ();

				using (var gradient = new CGGradient (cs, new nfloat [] { 1, 1, 1, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f }, new nfloat [] { 0, 1 })) {
					context.DrawLinearGradient (gradient, topCenter, bottomCenter, 0);
				}
			}
		}
	}
	
	public static class GraphicsUtil
	{
		
		/// <summary>
		///    Creates a path for a rectangle with rounded corners
		/// </summary>
		/// <param name="rect">
		/// The <see cref="RectangleF"/> rectangle bounds
		/// </param>
		/// <param name="radius">
		/// The <see cref="System.Single"/> size of the rounded corners
		/// </param>
		/// <returns>
		/// A <see cref="CGPath"/> that can be used to stroke the rounded rectangle
		/// </returns>
		public static CGPath MakeRoundedRectPath (RectangleF rect, float radius)
		{
			nfloat minx = rect.Left;
			nfloat midx = rect.Left + (rect.Width) / 2;
			nfloat maxx = rect.Right;
			nfloat miny = rect.Top;
			nfloat midy = rect.Y + rect.Size.Height / 2;
			nfloat maxy = rect.Bottom;

			var path = new CGPath ();
			path.MoveToPoint (minx, midy);
			path.AddArcToPoint (minx, miny, midx, miny, radius);
			path.AddArcToPoint (maxx, miny, maxx, midy, radius);
			path.AddArcToPoint (maxx, maxy, midx, maxy, radius);
			path.AddArcToPoint (minx, maxy, minx, midy, radius);		
			path.CloseSubpath ();
			
			return path;
		}
		
		public static void FillRoundedRect (CGContext ctx, RectangleF rect, float radius)
		{
			var p = GraphicsUtil.MakeRoundedRectPath (rect, radius);
			ctx.AddPath (p);
			ctx.FillPath ();
		}

		public static CGPath MakeRoundedPath (float size, float radius)
		{
			float hsize = size / 2;
			
			var path = new CGPath ();
			path.MoveToPoint (size, hsize);
			path.AddArcToPoint (size, size, hsize, size, radius);
			path.AddArcToPoint (0, size, 0, hsize, radius);
			path.AddArcToPoint (0, 0, hsize, 0, radius);
			path.AddArcToPoint (size, 0, size, hsize, radius);
			path.CloseSubpath ();
			
			return path;
		}
	}
}

