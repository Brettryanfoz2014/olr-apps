using System;
using UIKit;
using System.Drawing;
using MonoTouch.Dialog;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;

namespace MobileUtilities.Forms
{
	public class ButtonHolder : AbstractHolder
	{
		public ButtonHolder (RectangleF bounds, Entry entry) : base(bounds, entry)
		{
		}
		
		public UIButton mainButton = null;
		
		public override void SetupControls ()
		{
			// We need the button, label button combination
			mainButton = new UIButton (_bounds);
			mainButton.TouchDown += ProcessClick;
		}
		
		protected void ProcessClick (object sender, EventArgs e)
		{
			_entry.DoClick();
		}
		
		public override UIView[] GetViews ()
		{
			return new UIView[] { mainButton };
		}
	}
	
	public class GlassButtonHolder : ButtonHolder
	{
		public GlassButtonHolder (RectangleF bounds, Entry entry) : base(bounds, entry)
		{
		}
		
		public override void SetupControls ()
		{
			// We need the button, label button combination
			mainButton = new GlassButton (_bounds);
			mainButton.SetTitle(_entry.Label, UIControlState.Normal);
			(mainButton as GlassButton).NormalColor = _entry.ButtonColor;
			(mainButton as GlassButton).HighlightedColor = _entry.ButtonHighlightColor;
			mainButton.TouchDown += ProcessClick;
		}

		public override void SetFont (UIFont font)
		{
			base.SetFont (font);

			mainButton.Font = font;
		}
	}
}
