
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using Foundation;
using UIKit;
using MobileUtilities.Data;
namespace MobileUtilities.Forms
{
	public class GroupEntry<T> where T : class
	{
		public GroupEntry (string groupName)
		{
			GroupName = groupName;
		}
			
		public string GroupName = "";
		public List<T> Items = new List<T> ();
	}
}
