using System;

namespace MobileUtilities.Forms
{
	public class GroupingHelper
	{
		public static string GetFirstLetter(string text)
		{
			foreach (char c in text) {
				if (Char.IsLetter(c))
					return c.ToString().ToUpper();
			}
			
			return "Z";
		}
	}
}

