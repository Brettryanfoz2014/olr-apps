using System;
using UIKit;
using MonoTouch.Dialog;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Forms
{
	public class ImageAndButtonViewController : UIViewController
	{
		string _imageFile;
		string _buttonText = "";

		nfloat _imageWidth, _imageHeight;
		
		public ImageAndButtonViewController (string imageFile, nfloat imageWidth, nfloat imageHeight) : base()
		{
			_imageFile = imageFile;

			_imageWidth = imageWidth;
			_imageHeight = imageHeight;
		}
		
		public ImageAndButtonViewController (string imageFile, string buttonText, nfloat imageWidth, nfloat imageHeight) : base()
		{
			_imageFile = imageFile;
			_buttonText = buttonText;

			_imageWidth = imageWidth;
			_imageHeight = imageHeight;
		} 
		
		UIImageView _img;
		GlassButton _btn;

		public float ButtonHeight = 45.0f;

		protected virtual void DoButtonClick()
		{
			// Subclasses to implement
		}
		
		private void InternalButtonClick(object sender, EventArgs e)
		{
			DoButtonClick();
		}

		void GetControlBounds (out RectangleF image, out RectangleF button)
		{
			if (_btn == null) {
				image = new RectangleF (
					0, (View.Bounds.Height - _imageHeight) / 2, _imageWidth, _imageHeight);
				button = RectangleF.Empty;
			}
			else {
				// Calculate the position for the image and button
				nfloat newHeight = _imageHeight + 8 + ButtonHeight;
				
				image = new RectangleF((View.Bounds.Width - _imageWidth) / 2, (View.Bounds.Height - newHeight) / 2, _imageWidth, _imageHeight);
				button = new RectangleF(8, (View.Bounds.Height - newHeight) / 2 + _imageHeight + 8, 
				                                       View.Bounds.Width - 16, ButtonHeight);
			}
		}

		void SetupControlBounds ()
		{
			RectangleF i, b;
			GetControlBounds (out i, out b);
			if (!b.IsEmpty)
				_btn.Frame = b;
			_img.Frame = i;
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			SetupControlBounds ();
		}
		
		public UIColor ButtonColor = new UIColor (0.00f, 0.45f, 0.91f, 1);
		public UIColor ButtonColorHighlighted = new UIColor (0.00f, 0.45f, 0.91f, 1);
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			UIImage img = UIImage.FromFile(_imageFile);
			_img = new UIImageView(img);
			this.View.AddSubview(_img);
			this.View.BackgroundColor = UIColor.White;
			
			if (_buttonText.Length > 0)
			{
				_btn = new GlassButton(new RectangleF(8, 8, View.Bounds.Width - 16, 32));
				_btn.SetTitle(_buttonText, UIControlState.Normal);
				_btn.TouchUpInside += InternalButtonClick;
				_btn.NormalColor = this.ButtonColor;
				_btn.HighlightedColor = this.ButtonColorHighlighted;

				if (Theme.ApplyTheme)
				{
					_btn.Font = UIFont.FromName(Theme.TextFontName, Theme.TextFontSize);
				}

				this.View.AddSubview(_btn);
			}

			SetupControlBounds ();
		}
	}
}

