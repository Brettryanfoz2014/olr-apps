
using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace MobileUtilities.Forms
{
	public partial class IntegerValueViewController : UIViewController
	{
		#region Constructors

		// The IntPtr and initWithCoder constructors are required for items that need 
		// to be able to be created from a xib rather than from managed code

		public IntegerValueViewController (IntPtr handle) : base(handle)
		{
			Initialize ();
		}

		[Export("initWithCoder:")]
		public IntegerValueViewController (NSCoder coder) : base(coder)
		{
			Initialize ();
		}

		public IntegerValueViewController () : base("IntegerValueViewController", null)
		{
			Initialize ();
		}

		void Initialize ()
		{
		}
		
		#endregion
	}
}

