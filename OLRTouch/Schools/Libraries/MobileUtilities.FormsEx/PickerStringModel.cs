using System;
using System.Collections.Generic;
using UIKit;

namespace MobileUtilities.Forms
{
	public class PickerDataModel : UIPickerViewModel
	{
		Entry _entry;
		
		public PickerDataModel (Entry entry)
		{
			_entry = entry;
			_items.AddRange(entry.Options);
		}
		
		public event EventHandler<EventArgs> ValueChanged;
		
		public List<string> Items {
			get { return this._items; }
			set { this._items = value; }
		}

		List<string> _items = new List<string> ();
		
		public string SelectedItem {
			get { return this._items [(int)this._selectedIndex]; }
		}

		protected nint _selectedIndex = 0;
		
		public PickerDataModel ()
		{
		}
		
		public override nint GetRowsInComponent (UIPickerView picker, nint component)
		{
			return this._items.Count;
		}

		public override string GetTitle (UIPickerView picker, nint row, nint component)
		{
			return this._items [(int)row];
		}

		public override nint GetComponentCount (UIPickerView picker)
		{
			return 1;
		}
		
		public override void Selected (UIPickerView picker, nint row, nint component)
		{
			this._selectedIndex = row;
			if (this.ValueChanged != null) {
				this.ValueChanged (this, new EventArgs ());
			}
		}
		
		public void WriteChanges ()
		{
			_entry.Value = SelectedItem;
		}
	}
}

