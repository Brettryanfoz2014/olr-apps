using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace MobileUtilities.Forms
{
	public partial class PreviewImageViewController : UIViewController
	{
		#region Constructors
		
		// The IntPtr and initWithCoder constructors are required for items that need 
		// to be able to be created from a xib rather than from managed code
		
		public PreviewImageViewController (IntPtr handle) : base (handle)
		{
			Initialize ();
		}
		
		[Export ("initWithCoder:")]
		public PreviewImageViewController (NSCoder coder) : base (coder)
		{
			Initialize ();
		}
		
		public PreviewImageViewController () : base ("PreviewImageViewController", null)
		{
			Initialize ();
		}
		
		void Initialize ()
		{
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			if (Title == null || Title.Length == 0)
				Title = "Preview";
			
			this.mainImage.Image = UIImage.FromFile (imageToView);
		}
		
		public string imageToView = "";
		
		public void SetImageFile (string filename)
		{
			imageToView = filename;
		}
		
		#endregion
	}
}

