using System;
using System.Collections.Generic;

namespace MobileUtilities.Forms
{
	public class Section
	{
		public Section (string header, string footer)
		{
			this.Header = header;
			this.Footer = footer;
		}
		
		public Section(string header)
		{
			this.Header = header;
			this.Footer = "";
		}
		
		public string Header = "";
		public string Footer = "";
		
		public List<Entry> entries = new List<Entry>();
		
		#region Handlers for adding the sections

		public Entry AddString (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddString (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		public Entry AddPassword (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddPassword (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddCombo (string label, string defaultValue, string[] items, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddCombo (this, label, defaultValue, items, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddCombo (string label, string defaultValue, string[] items, string[] images, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddCombo (this, label, defaultValue, items, images, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		public Entry AddRollup (string label, string defaultValue, string[] items, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddRollup (this, label, defaultValue, items, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		public Entry AddSegment (string[] items)
		{
			Entry item = Entry.AddSegment (this, items);
			entries.Add (item);
			return item;
		}
		
		public Entry AddInteger (string label, int defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddInteger (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		public Entry AddUpDown (string label, int defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddUpDown (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		public Entry AddiAd ()
		{
			Entry item = Entry.AddiAd (this);
			entries.Add (item);
			return item;
		}
		
		public Entry AddPicturesFolder (string folder)
		{
			Entry item = Entry.AddPicturesFolder (this, folder);
			entries.Add (item);
			return item;
		}
		
		public Entry AddAudio (string pathToFile)
		{
			Entry item = Entry.AddAudio (this, pathToFile);
			entries.Add (item);
			return item;
		}
		
		public Entry AddAudioFolder (string folder)
		{
			Entry item = Entry.AddAudioFolder (this, folder);
			entries.Add (item);
			return item;
		}
		
		public Entry AddDecimal (string label, Decimal defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddDecimal (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddCurrency (string label, Decimal defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddCurrency (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddMemo (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddMemo (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddCollapsible (string title, bool isCollapsed, bool hasAddButton, Entry[] subItems)
		{
			Entry item = Entry.AddCollapsible(this, title, isCollapsed, hasAddButton, subItems); 
			entries.Add (item);
			return item;
		}

		public Entry AddEmail (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddEmail (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddURL (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddURL (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddDate (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddDate (this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddDateTime (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddDateTime(this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddTime (string label, string defaultValue, string placeholder, bool isReadOnly)
		{
			Entry item = Entry.AddTime(this, label, defaultValue, placeholder, isReadOnly);
			entries.Add (item);
			return item;
		}

		public Entry AddWebLink (string label, string url)
		{
			Entry item = Entry.AddWebLink (this, label, url);
			entries.Add (item);
			return item;
		}

		public Entry AddMap (string label, bool runiOSMaps)
		{
			Entry item = Entry.AddMap(this, label, runiOSMaps);
			entries.Add (item);
			return item;
		}

		public Entry AddNameValue (string label, string value)
		{
			Entry item = Entry.AddNameValue (this, label, value);
			entries.Add (item);
			return item;
		}

		public Entry AddButton (string label)
		{
			Entry item = Entry.AddButton(this, label);
			entries.Add (item);
			return item;
		}

		public Entry AddContact (string label)
		{
			Entry item = Entry.AddContact(this, label);
			entries.Add (item);
			return item;
		}

		public Entry AddNavigate (string label)
		{
			Entry item = Entry.AddNavigate (this, label);
			entries.Add (item);
			return item;
		}
		
		public Entry AddCheckNavigate (string label)
		{
			Entry item = Entry.AddCheckNavigate (this, label);
			entries.Add (item);
			return item;			
		}

		public Entry AddDocument (string label, string document)
		{
			Entry item = Entry.AddDocument (this, label, document);
			entries.Add (item);
			return item;
		}
		
		public Entry AddInfoText (string label, string text)
		{
			Entry item = Entry.AddInfoText (this, label, text);
			entries.Add (item);
			return item;
		}
		
		public Entry AddBoolean (string label, string defaultValue, bool isReadOnly)
		{
			Entry item = Entry.AddBoolean(this, label, defaultValue, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		public Entry AddBoolean (string label, bool defaultValue, bool isReadOnly)
		{
			Entry item = Entry.AddBoolean (this, label, defaultValue, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		public Entry AddOnOff (string label, bool defaultValue, bool isReadOnly)
		{
			Entry item = Entry.AddOnOff (this, label, defaultValue, isReadOnly);
			entries.Add (item);
			return item;
		}
		
		#endregion
		
		public Entry ConvertToCollapsedHeader ()
		{
			Entry e = Entry.AddCollapsible (this, this.Header, true, false, entries.ToArray ());
			entries.Clear ();
			entries.Add (e);
			Header = "";
			
			return e;
		}
	}
}

