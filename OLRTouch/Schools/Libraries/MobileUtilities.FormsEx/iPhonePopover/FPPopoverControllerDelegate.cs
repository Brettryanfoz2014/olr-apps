using System;
using UIKit;
using Foundation;
using System.Drawing;

namespace FPPopoverSharp
{
	public abstract class FPPopoverControllerDelegate : NSObject
	{
		public abstract void PopoverControllerDidDismissPopover (FPPopoverController popoverController);
		
		public abstract void PresentedNewPopoverControllershouldDismissVisiblePopover (FPPopoverController newPopoverController, FPPopoverController visiblePopoverController);
		
	}
}

