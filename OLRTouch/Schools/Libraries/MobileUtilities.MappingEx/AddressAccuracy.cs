using System;

namespace MobileUtilities.Mapping
{
    public enum AddressAccuracy
    {
        Unknown,
        CountryLevel,
        StateLevel,
        CityLevel,
        PostalCodeLevel,
        StreetLevel,
        AddressLevel
    }
}
