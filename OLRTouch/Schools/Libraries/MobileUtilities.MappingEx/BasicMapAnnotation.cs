
using System;
using System.Collections.Generic;
using System.Linq;

#if __UNIFIED__
using Foundation;
using UIKit;
using MapKit;
using CoreLocation;
#else
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using MonoTouch.CoreLocation;
#endif

using MonoTouch.Dialog;


namespace MobileUtilities.Mapping
{
	public class BasicMapAnnotation : MKAnnotation {

		public override CLLocationCoordinate2D Coordinate { get{ return this.Coordinate; }}

		string title, subtitle;

		public override string Title { get{ return title; }}
		public override string Subtitle { get{ return subtitle; }}

		public BasicMapAnnotation (CLLocationCoordinate2D coordinate, string title, string subtitle) {
			this.SetCoordinate (coordinate);
			this.title = title;
			this.subtitle = subtitle;
		}
	}
}
