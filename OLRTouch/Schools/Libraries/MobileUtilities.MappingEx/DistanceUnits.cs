﻿using System;

namespace MobileUtilities.Mapping
{
    public enum DistanceUnits
    {
        Miles,
        Kilometers
    }
}
