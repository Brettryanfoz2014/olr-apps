using System;
using System.Collections.Generic;
using System.Linq;
#if __UNIFIED__
using Foundation;
using UIKit;
using AudioToolbox;
using AVFoundation;
#else
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.AudioToolbox;
using MonoTouch.AVFoundation;
#endif
using System.IO;

namespace MobileUtilities.Multimedia
{
	public class AudioRecording
	{
		// Early iTouches had no audio input.
		public static bool CanRecordAudio ()
		{
			return AVAudioSession.SharedInstance ().InputIsAvailable;
		}
		
		public static NSError audioSetupError = null;
		
		public static void Initialise ()
		{
			AVAudioSession.SharedInstance ().SetActive (true, out audioSetupError);
			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayAndRecord, out audioSetupError);
		}
		
		public AudioRecording ()
		{
		}
		
		public AVAudioRecorder recorder;
		public string audioFilePath = "";
		NSUrl url;
		NSDictionary settings;
		NSError error;
		
		public void StartRecording (string filename)
		{
			var dirName = System.IO.Path.GetDirectoryName (filename);
			if (!System.IO.Directory.Exists (dirName))
				System.IO.Directory.CreateDirectory (dirName);
			
			audioFilePath = filename;
			
			NSObject[] values = new NSObject[]
            {    
                NSNumber.FromInt32((int)AudioFormatType.AppleIMA4), 
			    NSNumber.FromFloat(11025.0f), 
			    NSNumber.FromInt32(1),
			    NSNumber.FromInt32((int)AVAudioQuality.Min)
			};

			NSObject[] keys = new NSObject[]
            {
                AVAudioSettings.AVFormatIDKey,
			    AVAudioSettings.AVSampleRateKey,
			    AVAudioSettings.AVNumberOfChannelsKey,
			    AVAudioSettings.AVEncoderAudioQualityKey
            };

			settings = NSDictionary.FromObjectsAndKeys (values, keys);
			url = NSUrl.FromFilename (filename);
            
			error = new NSError ();
			recorder = AVAudioRecorder.Create (url, new AudioSettings(settings), out error);
			
			recorder.FinishedRecording += HandleRecorderFinishedRecording;
			recorder.MeteringEnabled = false;
            
			recorder.Record ();
		}
		
		void HandleRecorderFinishedRecording (object sender, AVStatusEventArgs e)
		{
			recorder.Dispose ();
		}
    
        public void Stop()
        {
            recorder.Stop();
        }
	}
}

