using System;
using System.Json;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;

namespace MobileUtilities.Network
{
	public class JsonUtilities
	{
		public static List<T> LoadCollectionFromWebLink<T>(string url, ProcessJsonEntry handler) where T : class
		{
			return LoadCollectionFromWebLink<T>(url, handler, null);
		}

		public static T LoadItemFromWebLink<T>(string url, ProcessJsonEntry handler) where T : class
		{
			T result = default(T);

			var request = HttpWebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "GET";
			request.Timeout = 45000;
			
			try
			{
				if (NetworkIndicator != null)
					NetworkIndicator.Start();
				
				using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
				{
					if (response.StatusCode != HttpStatusCode.OK)
					{
						// Present the errir details
						Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
					} else {
						using (StreamReader reader = new StreamReader(response.GetResponseStream()))
						{
							var content = reader.ReadToEnd();
							
							var data = JsonObject.Parse(content);
							T item = handler(data) as T;

							return item;
						}
					}
				} 
			} finally {
				if (NetworkIndicator != null)
					NetworkIndicator.Stop();
			}
			
			return result;
		}

		public static List<T> LoadCollectionFromWebLink<T>(string url, ProcessJsonEntry handler, string transactionId
		                                                   ) where T : class
		{
			List<T> result = new List<T>();
			
			var request = HttpWebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "GET";
			request.Timeout = 45000;
			
			if (transactionId != null)
			{
				request.Headers["AssessmentsTransaction"] = transactionId;
			}
			
			try
			{
				if (NetworkIndicator != null)
					NetworkIndicator.Start();
				
				using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
				{
					if (response.StatusCode != HttpStatusCode.OK)
					{
						// Present the errir details
						Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
					} else {
						using (StreamReader reader = new StreamReader(response.GetResponseStream()))
						{
							var content = reader.ReadToEnd();
							
							var data = JsonObject.Parse(content);
							if (data is JsonArray)
							{
								var array = data as JsonArray;
								foreach (JsonObject o in array)
								{
									T item = handler(o) as T;
									if (item != null)
										result.Add(item);
								}
							}
						}
					}
				} 
			} finally {
				if (NetworkIndicator != null)
					NetworkIndicator.Stop();
			}
			
			return result;
		}
		
		public static List<T> LoadCollectionFromWebLinkWithIds<T>(string urlPrefix, List<string> ids, int maxBatchSize, 
		                                                          Dictionary<string, string> additionalHeaders,
		                                                          ProcessJsonEntry handler
		                                                          ) where T : class
		{
			List<T> result = new List<T>();
			
			try
			{
				if (NetworkIndicator != null)
					NetworkIndicator.Start();
				
				// Start the process of reading in the Ids
				List<string> AllIds = ids.Select(o => o.ToString()).ToList();
				
				while (AllIds.Count > 0)
				{
					List<string> currentBatch = new List<string>();
					int numberToRemove;
					
					if (AllIds.Count > maxBatchSize)
					{
						currentBatch.AddRange(AllIds.GetRange(0, maxBatchSize));
						numberToRemove = maxBatchSize;
					} else {
						currentBatch.AddRange(AllIds);
						numberToRemove = AllIds.Count;
					}
					
					string batchIds = String.Join(",", currentBatch.ToArray());
					
					string url = urlPrefix + batchIds;
					
					// Load the details now
					var request = HttpWebRequest.Create(url);
					request.ContentType = "application/json";
					request.Method = "GET";
					request.Timeout = 45000;
					
					foreach (string key in additionalHeaders.Keys)
						request.Headers[key] = additionalHeaders[key];
					
					using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
					{
						if (response.StatusCode != HttpStatusCode.OK)
						{
							// Present the errir details
							Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
						} else {
							using (StreamReader reader = new StreamReader(response.GetResponseStream()))
							{
								var content = reader.ReadToEnd();
								
								var data = JsonObject.Parse(content);
								if (data is JsonArray)
								{
									var array = data as JsonArray;
									foreach (JsonObject o in array)
									{
										T item = handler(o) as T;
										if (item != null)
											result.Add(item);
									}
								}
							}
						}
					}
					
					// Remove the items
					AllIds.RemoveRange(0, numberToRemove);
				}
			} finally {
				if (NetworkIndicator != null)	
					NetworkIndicator.Stop();
			}
			
			return result;
		}

		public static string Serialize<T>(T t)
		{
			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			MemoryStream ms = new MemoryStream();
			ser.WriteObject(ms, t);
			string jsonString = Encoding.UTF8.GetString(ms.ToArray());
			ms.Close();
			return jsonString;
		}
		
		public static string JsonSerializerByObject(object o)
		{
			DataContractJsonSerializer ser = new DataContractJsonSerializer(o.GetType());
			MemoryStream ms = new MemoryStream();
			ser.WriteObject(ms, o);
			string jsonString = Encoding.UTF8.GetString(ms.ToArray());
			ms.Close();
			return jsonString;
		}
		
		/// <summary>
		/// JSON Deserialization
		/// </summary>
		public static T Deserialize<T>(string jsonString)
		{
			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
			T obj = (T)ser.ReadObject(ms);
			return obj;
		}
		
		public static string PostDataToURL(string url, string method, object item)
		{
			using (var client = new WebClient())
			{
				if (NetworkIndicator != null)	
					NetworkIndicator.Start();

				var dataToPost = Encoding.Default.GetBytes(JsonSerializerByObject(item));
				var result = client.UploadData(url, method, dataToPost);

				if (NetworkIndicator != null)	
					NetworkIndicator.Stop();

				return result.ToString();
			}
		}

		public static string PostDataToURL(string url, string method, string details)
		{
			try
			{
				using (var client = new WebClient())
				{
					if (NetworkIndicator != null)	
						NetworkIndicator.Start();
					
					var dataToPost = Encoding.Default.GetBytes(details);
					var result = client.UploadData(url, method, dataToPost);
					
					if (NetworkIndicator != null)	
						NetworkIndicator.Stop();
					
					return result.ToString();
				}
			} catch {
				if (NetworkIndicator != null)	
					NetworkIndicator.Stop();
				
				return "";
			}
		}

		public static DownloadProcessingInfo NetworkIndicator = null;
	}

	public delegate object ProcessJsonEntry(JsonValue o);

	public class DownloadProcessingInfo
	{
		public virtual void Start()
		{
			
		}
		
		public virtual void Stop()
		{
			
		}
	}
}

