using System;
using System.Drawing;
using System.Xml;
using MobileUtilities.XML;
using UIKit;
using CoreGraphics;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Reporting.Loader
{
	public abstract class BaseShape : DrawableObject
	{
		private int _penWidth = 1;

		public int OutlineWidth
		{
			get
			{
				return _penWidth;
			}
			set
			{
				if (_penWidth != value)
				{
					_penWidth = value;
					RedrawDesigner();
				}
			}
		}

		protected bool _fill = true;

		public bool Fill
		{
			get
			{
				return _fill;
			}
			set
			{
				if (_fill != value)
				{
					_fill = value;
					RedrawDesigner();
				}
			}
		}

		public override Size GetDefaultSize()
		{
			return new Size(64, 64);
		}

		UIColor _penColor = UIColor.Black;
		UIColor _brushColor = UIColor.White;

		public UIColor OutlineColor
		{
			get
			{
				return _penColor;
			}
			set
			{
				_penColor = value;
				RedrawDesigner();
			}
		}

		public UIColor FillColor
		{
			get
			{
				return _brushColor;
			}
			set
			{
				_brushColor = value;
				RedrawDesigner();
			}
		}

		public override void LoadFromXML(XmlElement e)
		{
			base.LoadFromXML(e);

			_penColor = GraphicsStorage.ReadColor(e, "OutlineColor", _penColor);
			_brushColor = GraphicsStorage.ReadColor(e, "FillColor", _brushColor);
			_penWidth = XMLHelper.ReadXMLInt(e, "OutlineWidth", 1);
		}
	}

	public class RectangleShape : BaseShape
	{
		public override void Draw (CGContext context)
		{
			FillColor.SetFill ();
			OutlineColor.SetStroke ();
			context.SetLineWidth (OutlineWidth);
			
			CGPath path = new CGPath ();
			path.AddLines (new PointF[] {
				new PointF (Left, Top),
				new PointF (Left + Width, Top),
				new PointF (Left + Width, Top + Height),
				new PointF (Left, Top + Height),
				new PointF (Left, Top) });
			
			path.CloseSubpath ();
  
			context.AddPath (path);
			context.DrawPath (CGPathDrawingMode.FillStroke);
		}
	}

	public class EllipseShape : BaseShape
	{
		public override void Draw (CGContext context)
		{
			FillColor.SetFill ();
			OutlineColor.SetStroke ();
			context.SetLineWidth (OutlineWidth);
			
			CGPath path = new CGPath ();
			path.AddEllipseInRect(new RectangleF(Left, Top, Width, Height));
     		path.CloseSubpath ();
  
			context.AddPath (path);
			context.DrawPath (CGPathDrawingMode.FillStroke);
		}
	}
}
