using System;
using CoreGraphics;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Reporting
{
	public class PdfScene : Scene
	{
		PDFDocument _document = null;
		
		public PdfScene (PDFDocument document) : base()
		{
			DrawBackground = false;
			_document = document;
		}
		
		public int PageNumber = 1;
		public string PageName = "";
		
		public string PageCondition = "";
		
		public void SetPageName (string pageName)
		{
			string[] list = pageName.Split (new char[] { '.' });
			if (list.Length == 1) {
				PageName = pageName;
				PageCondition = "";
			} else if (list.Length == 2){
				PageName = pageName;
				PageCondition = list[1];				
			}
		}
		
		/// <summary>
		/// Convert a scene to offsets. Used for repeatible sections
		/// </summary>
		public void ConvertToOffsets ()
		{
			nfloat minTop = float.MaxValue;
			
			foreach (Loader.DrawableObject item in Items) {
				if (item.Top < minTop)
					minTop = item.Top;
			}
			
			foreach (Loader.DrawableObject item in Items) {
				item.Top -= minTop;
			}
		}
		
		public nfloat CalculateHeight ()
		{
			nfloat max = 0;
			
			foreach (Loader.DrawableObject item in Items) {
				if (item.Top + item.Height > max)
					max = item.Top + item.Height;
			}
			
			return max;
		}
		
		public override void Draw (CGContext context, RectangleF bounds)
		{
			if (DrawBackground) {
				backgroundColor.SetColor ();
				context.FillRect (bounds);
			}
			
			DoBeforeDraw (context, bounds);
			
			// Need to sort the items in a top down fashion so that we can manage them appropriatly
			Items.Sort (new ItemTopDownSorter ());
			
			nfloat originalCurrentY = currentY;
			
			nfloat afterFooterLimit = _document.GetFooterTop ();
			
			for (int counter = 0; counter < Items.Count; counter++) {
				Item i = Items [counter];
				
				bool isRepeatingSection = i is RepeatingSection;
			
				if (!isRepeatingSection) {
					i.Draw (context);				
				} else {
					// Draw the repeating section
					RepeatingSection repeater = i as RepeatingSection;
					currentY = repeater.Top;
					DrawRepeatingSection (bounds, repeater);
					
					for (int secondCounter = counter+1; secondCounter < Items.Count; secondCounter++) {
						Loader.DrawableObject o = Items [secondCounter] as Loader.DrawableObject;
						
						nfloat newTop = o.Top + currentY - originalCurrentY;
						bool isOverThePage = newTop > afterFooterLimit;
						
						o.Top += currentY - originalCurrentY;
						
						// Need to compensate here for entries that move over t the next page
						
					}
				}
			}
			
			DoAfterDraw (context, bounds);
		}
		
		public nfloat currentY = 0;
		
		#region Code for the repeating section
		
		public string GroupByExpression = "";
		
		public string CurrentGroupByValue = "";
		
		private void DrawRepeatingSection (RectangleF bounds, RepeatingSection section)
		{
			CurrentGroupByValue = "";
			
			// Need to get the data for the section
			PdfScene rs = _document.repeatibleSections [section.SectionName];
			
			// See if there is a group by section
			PdfScene groupBySection = _document.GetGroupBySection (section.SectionName, out GroupByExpression);
			bool hasGroupBy = groupBySection != null;
			
			// Get the Datasource that we will be using 
			_document.SetupItemDataSource (section.DataSource, section.Argument);
			
			while (_document.ReadDataSourceItem()) {
				
				// Get the new group by value
				if (hasGroupBy) {
					string newGroupByValue = _document.GetExpressionValue (GroupByExpression);
				
					if (newGroupByValue != CurrentGroupByValue) {
						// Draw the Group By section
						DrawSingleSection (section, groupBySection);
					
						CurrentGroupByValue = newGroupByValue;
					}
				}
				
				// Draw the item with the appropriate offset
				DrawSingleSection (section, rs);
			}
		}

		void DrawSingleSection (RepeatingSection section, PdfScene rs)
		{
			nfloat maxHeight = 0;
			
			foreach (Loader.DrawableObject i in rs.Items) {
				
				RectangleF originalDimensions = i.GetRectangle ();
				
				i.Top += currentY;
				
				i.Draw (_document.currentContext);
				
				// Work out if the item was drawn or not
				nfloat currentHeightMax = (i.Top + i.Height) - currentY;
				if (i.HasDimensions () && (currentHeightMax > maxHeight))
					maxHeight = currentHeightMax;
				
				i.RestoreDimensions (originalDimensions);
			}
			
			currentY += maxHeight + section.VerticalPadding;
			
			if (currentY > _document.GetFooterTop () - rs.CalculateHeight ()) {
				// We need a new page
				_document.MakeNewPage ();
				
				currentY = _document.GetHeaderHeight () + section.VerticalPadding;
			}
		}
		
		#endregion
		
		#region Add Items Helpers
		
		public void AddLabel (nfloat x, nfloat y, nfloat width, nfloat height, string text)
		{
			Loader.PrintLabel lbl = new Loader.PrintLabel ();
			lbl.Left = x;
			lbl.Top = y;
			lbl.Width = width;
			lbl.Height = height;
			lbl.Text = text;
			Items.Add (lbl);
		}
		
		public void AddHyperLink (nfloat x, nfloat y, nfloat width, nfloat height, string text, string url)
		{
			Loader.HyperLinkText hl = new Loader.HyperLinkText ();
			hl.Left = x;
			hl.Top = y;
			hl.Width = width;
			hl.Height = height;
			hl.Text = text;
			hl.Url = url;
			Items.Add (hl);
		}
		
		public void AddImage (nfloat x, nfloat y, nfloat width, nfloat height, string filename)
		{
			Loader.PrintImage img = new Loader.PrintImage ();
			img.Left = x;
			img.Top = y;
			img.Width = width;
			img.Height = height;
			img.Path = filename;
			Items.Add (img);
		}
		
		#endregion
	}

}

