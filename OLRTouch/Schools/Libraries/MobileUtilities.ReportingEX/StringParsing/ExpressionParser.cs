using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Expression processor, taken from the Orchard.Text framework (c) 2010 Orchard ebusiness Pty Ltd
/// Allows for processing of a [ delimiter based processing 
/// </summary>
namespace MobileUtilities.Reporting
{
	public class ExpressionProcessor
	{
		public ExpressionProcessor ()
		{
		}

		public string EvaluateString (string expressionToEvaluate)
		{
			string result = "";

			int LeftBracketPos = expressionToEvaluate.IndexOf ("[");
			if (LeftBracketPos == -1) {
				//Write the text only to the element
				result = expressionToEvaluate;
			} else {
				string content = expressionToEvaluate;

				bool TextMode = true;
				string CurrentContent = "";

				//Search through the string 
				for (int i = 0; i < content.Length; i++) {
					char c = content [i];
					switch (c) {
					case '[':
						if (TextMode) {
							TextMode = false;
							if (!CurrentContent.Equals ("")) {
								result += CurrentContent;
								CurrentContent = "";
							}
						} else { //Column Mode
							if (CurrentContent.Equals ("")) {
								//Add the bracket character
								result += "[";
								CurrentContent = "";
								TextMode = true;
							} else {
								//See if the content is nothing
								CurrentContent += c;
							}
						}
						break;
					case ']':
						if (TextMode) {
							CurrentContent += c;
						} else {
							result += EvaluateInnerExpression (CurrentContent);

							CurrentContent = "";
							TextMode = true;
						}
						break;
					default:
						CurrentContent += c;
						break;
					}
				}

				//If we have anything left over, we should consider it as text
				if (!CurrentContent.Equals (""))
					result += CurrentContent;
			}

			return result;
		}

		protected string EvaluateInnerExpression (string value)
		{
			return Evaluate (value.Split ('.'));
		}

		/// <summary>
		/// This is the method that should be overriden to implement the evaluation
		/// </summary>
		/// <param name="values"></param>
		/// <returns></returns>
		protected virtual string Evaluate (string[] values)
		{
			return "";
		}
	}
}

