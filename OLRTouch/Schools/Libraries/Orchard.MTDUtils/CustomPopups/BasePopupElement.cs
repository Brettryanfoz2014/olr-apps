using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace Orchard.MTDUtils
{
	public abstract class BasePopupElement : StringElement
	{
		public BasePopupElement (string caption, string value) : base(caption, value)
		{
			ShowTapToSelectIfNeeded ();
		}

		public abstract BaseCustomPopup GetPopup ();

		public override void Selected (DialogViewController dvc, UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			base.Selected (dvc, tableView, indexPath);

			ShowThePopup ();
		}

		public UIPopoverController MainPopoverController = null;

		protected void ShowThePopup()
		{
			// Get the View we want to show the popup over
			UINavigationController navCtrl = new UINavigationController ();

			// Get the view of who was selected
			var view = this.GetContainerTableView ().CellAt (IndexPath);

			// If the master list popover is showing, dismiss it before presenting the popover from the bar button item.
			if (MainPopoverController != null)
				MainPopoverController.Dismiss (true);

			var popup = GetPopup ();
			popup.OnSetValue += SetPopupValue;
			popup.OnClearValue += DoClearPopupValue;
			popup.View.Frame = popup.GetView ().Frame;
			navCtrl.PushViewController (popup, false);

			MainPopoverController = new UIPopoverController (navCtrl);

			// If the popover is already showing from the bar button item, dismiss it. Otherwise, present it.
			if (!MainPopoverController.PopoverVisible) {
				MainPopoverController.PresentFromRect (view.Bounds, view, 
				                                      UIPopoverArrowDirection.Any, true);
			}
			else
				MainPopoverController.Dismiss (true);
		}

		public string PopupValue {
			get;
			set;
		}

		public virtual void DoClearPopupValue ()
		{
			PopupValue = "";
			UpdateDisplay ();

			if (MainPopoverController.PopoverVisible) 
				MainPopoverController.Dismiss (true);
		}

		public virtual void SetPopupValue (string obj)
		{
			PopupValue = obj;
			UpdateDisplay ();

			if (MainPopoverController.PopoverVisible) 
				MainPopoverController.Dismiss (true);

			// Fire an event if we need to listen to some of these
			if (OnPopupValueSet != null)
				OnPopupValueSet ();
		}

		public Action OnPopupValueSet = null;

		void ShowTapToSelectIfNeeded ()
		{
			if (String.IsNullOrEmpty (this.PopupValue))
				this.Value = "Tap to select";
		}

		protected void UpdateDisplay()
		{
			this.Value = PopupValue;

			ShowTapToSelectIfNeeded ();

			if (GetContainerTableView () != null) {
				var root = GetImmediateRootElement ();
				root.Reload (this, UITableViewRowAnimation.Fade);
			}
		}

		public UIFont CaptionFont = null;
		public UIFont ValueFont = null;

		public override UITableViewCell GetCell (UITableView tv)
		{
			var result = base.GetCell (tv);
			if (CaptionFont != null)
				result.TextLabel.Font = CaptionFont;
			if (ValueFont != null)
				result.DetailTextLabel.Font = ValueFont;
			return result;
		}
	}
}

