using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Orchard.MTDUtils
{
	public abstract class BaseDatePopupElement : BasePopupElement {

		public BaseDatePopupElement(string caption, string value) : base(caption, value)
		{

		}

		private DateTime _dateValue = DateTime.Now;

		public DateTime DateValue
		{
			get
			{
				return _dateValue;
			}
			set
			{
				this._dateValue = value;
				this._hasValue = true;

				UpdateDisplay ();
			}
		}

		private void UpdateDisplay()
		{
			if (HasValue)
				this.Value = FormatDateForDisplay (_dateValue);
			else
				this.Value = "Tap to select";

			GetContainerTableView ().ReloadData ();
		}

		public DateTime minDate = DateTime.MinValue;
		public DateTime maxDate = DateTime.MaxValue;

		private bool _hasValue = false;

		public bool HasValue
		{
			get
			{
				return _hasValue;
			} 
			set
			{
				_hasValue = true;

			}
		}

		public override void DoClearPopupValue ()
		{
			base.DoClearPopupValue ();
		}

		public override void SetPopupValue (string obj)
		{
			if (String.IsNullOrEmpty(obj))
			{
				// We have a clear value
				HasValue = false;
				DateValue = DateTime.MinValue;
				Value = "";
			} else {
				// We have an actual date
				HasValue = true;
				DateValue = ConvertStringToDateTime (obj);
				Value = FormatDateForDisplay (DateValue);
			}
		}

		public abstract DateTime ConvertStringToDateTime (string value);

		public virtual string FormatDateForDisplay(DateTime dt)
		{
			return dt.ToString ();
		}
	}

	public class DatePopupElement : BaseDatePopupElement
	{
		public override BaseCustomPopup GetPopup ()
		{
			DatePopup _popup = new DatePopup (this, UIDatePickerMode.Date, DateValue.ToString());
			return _popup;
		}

		public DatePopupElement(string caption, string value) : base(caption, value)
		{

		}

		public override string FormatDateForDisplay(DateTime dt)
		{
			return dt.ToString ("ddd dd MMM yyyy");
		}

		public override void SetPopupValue (string obj)
		{
			base.SetPopupValue (obj);

			DateValue = MobileUtilities.XML.DataTypeStorage.ReadXmlDate (obj);

			MainPopoverController.Dismiss (true);
		}

		public override DateTime ConvertStringToDateTime (string value)
		{
			return MobileUtilities.XML.DataTypeStorage.ReadXmlDate (value);
		}
	}

	public class DateTimePopupElement : BaseDatePopupElement
	{
		public override BaseCustomPopup GetPopup ()
		{
			DatePopup _popup = new DatePopup (this, UIDatePickerMode.DateAndTime, DateValue.ToString());
			return _popup;
		}

		public DateTimePopupElement(string caption, string value) : base(caption, value)
		{

		}

		public override string FormatDateForDisplay (DateTime dt)
		{
			return dt.ToString ("ddd dd MMM yyyy") + " " + dt.ToShortTimeString ();
		}

		public override void SetPopupValue (string obj)
		{
			base.SetPopupValue (obj);

			DateValue = MobileUtilities.XML.DataTypeStorage.ReadXmlDateTime (obj, DateTime.Now);

			MainPopoverController.Dismiss (true);
		}

		public override DateTime ConvertStringToDateTime (string value)
		{
			return MobileUtilities.XML.DataTypeStorage.ReadXmlDateTime (value, DateTime.Now);
		}
	}

	public class TimePopupElement : BaseDatePopupElement
	{
		public override BaseCustomPopup GetPopup ()
		{
			DatePopup _popup = new DatePopup (this, UIDatePickerMode.Time, DateValue.ToString());
			return _popup;
		}

		public TimePopupElement(string caption, string value) : base(caption, value)
		{

		}

		public override string FormatDateForDisplay (DateTime dt)
		{
			return dt.ToShortTimeString ();
		}

		public override void SetPopupValue (string obj)
		{
			base.SetPopupValue (obj);

			DateValue = MobileUtilities.XML.DataTypeStorage.ReadXmlTime (obj, DateTime.Now);

			MainPopoverController.Dismiss (true);
		}

		public override DateTime ConvertStringToDateTime (string value)
		{
			return MobileUtilities.XML.DataTypeStorage.ReadXmlTime (value, DateTime.Now);
		}
	}

	public class DatePopup : BaseCustomPopup
	{
		UIDatePicker _picker;
		BaseDatePopupElement _dateElement;

		UIDatePickerMode _mode = UIDatePickerMode.Date;

		public DatePopup(BaseDatePopupElement dateElement, UIDatePickerMode mode, string InitialValue) 
			: base(InitialValue)
		{
			_dateElement = dateElement;
			_mode = mode;
		}

		protected override string GetTitle ()
		{
			return _dateElement.Caption;
		}

		public override UIView DoGetView ()
		{
			_picker = new UIDatePicker ();
			_picker.TimeZone = NSTimeZone.LocalTimeZone;
			_picker.Date = _dateElement.DateValue;
			_picker.MinimumDate = _dateElement.minDate;
			_picker.MaximumDate = _dateElement.maxDate;
			_picker.Mode = _mode;

			return _picker;
		}

		protected override string GetValue ()
		{
			var date = _picker.Date.ToDateTime ().ToLocalTime ();

			switch (_mode)
			{
				case UIDatePickerMode.Date:
				default:
					return MobileUtilities.XML.DataTypeStorage.WriteXmlDate (date);
				case UIDatePickerMode.DateAndTime:
					return MobileUtilities.XML.DataTypeStorage.WriteXmlDateTime (date);
				case UIDatePickerMode.Time:
					return MobileUtilities.XML.DataTypeStorage.WriteXmlTime (date);
			}
		}
	}
}
