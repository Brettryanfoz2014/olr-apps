using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Threading.Tasks;

namespace Orchard.MTDUtils
{
	public class ItemPopupElement<T> : BasePopupElement where T : class {

		public List<T> _items;
		internal T _initialValue;

		public ItemPopupElement(string caption, List<T> items, T initialValue) : base(caption, initialValue.ToString())
		{
			_items = items;
			_initialValue = initialValue;
		}

		public override BaseCustomPopup GetPopup ()
		{
			if (OnNeedItems != null)
				OnNeedItems (_items);

			ItemPopup<T> _popup = new ItemPopup<T> (this);
			return _popup;
		}

		public string SearchForText = "Search for...";

		public UIFont ListItemFont = null;

		public Action<List<T>> OnNeedItems = null;

	}

	public class ItemPopup<T> : BaseCustomPopup where T : class
	{
		List<T> _items;

		public Action<T> ItemSelected = null;

		public void DoItemSelected(T item)
		{
			if (ItemSelected != null)
				ItemSelected (item);

			_parent.SetPopupValue (item.ToString ());
		}

		internal ItemPopupElement<T> _parent;

		public ItemPopup(ItemPopupElement<T> parent) : base(parent.Value)
		{
			_parent = parent;
		}

		ListSearchForm<T> _searchForm;

		public override UIView DoGetView ()
		{
			_searchForm = new ListSearchForm<T> (this);
			return _searchForm.View;
		}

		protected override string GetValue ()
		{
			if (_searchForm != null)
				return _searchForm.SelectedItem != null ? _searchForm.SelectedItem.ToString () : "";
			else
				return _parent.Value;
		}

		protected override string GetTitle ()
		{
			return _parent.Caption;
		}
	}

	public class ListSearchForm<T> : SearchForm<T> where T : class
	{
		ItemPopup<T> _owner;

		public ListSearchForm(ItemPopup<T> owner) : base("List", UITableViewStyle.Plain)
		{
			_owner = owner;
			EnableSearch = true;
			SearchPlaceholder = owner._parent.SearchForText;

			Title = owner._parent.Caption;
		}

		public override bool CanEditItem (T item)
		{
			return false;
		}

		public override MonoTouch.Dialog.Element GetElementForItem (T item)
		{
			var element = new StyledMultilineElement (item.ToString ());
			element.BackgroundColor = UIColor.Clear;

			if (item.ToString() == _owner._parent.Value) {
				element.Accessory = UITableViewCellAccessory.Checkmark;
				SelectedItem = item;
			} else
				element.Accessory = UITableViewCellAccessory.None;

			if (_owner._parent.ListItemFont != null)
				element.Font = _owner._parent.ListItemFont;

			element.Tapped += () => {
				SelectedItem = item;
				_owner.DoItemSelected(item);
			};

			return element;
		}

		public T SelectedItem = default(T);

		protected async override Task<T[]> GetItems (ItemRequest details)
		{
			return _owner._parent._items.ToArray ();
		}
	}
}
