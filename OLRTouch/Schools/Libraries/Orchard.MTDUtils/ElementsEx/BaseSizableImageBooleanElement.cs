using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading;
using System.Drawing;
using MonoTouch.Dialog;

namespace Orchard.MTDUtils
{
	public abstract class BaseSizableImageBooleanElement : BoolElement
	{
		private static NSString key = new NSString ("SizableBooleanImageElement");

		protected override NSString CellKey
		{
			get
			{
				return BaseSizableImageBooleanElement.key;
			}
		}

		protected SizeF imageSize;

		public UIFont Font = UIFont.SystemFontOfSize(17f);

		public BaseSizableImageBooleanElement (string caption, bool value, SizeF imageSize) : base (caption, value)
		{
			this.imageSize = imageSize;
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			BaseSizableImageBooleanElement.TextWithImageCellView textWithImageCellView = tv.DequeueReusableCell (this.CellKey) as BaseSizableImageBooleanElement.TextWithImageCellView;
			if (textWithImageCellView == null)
			{
				textWithImageCellView = new BaseSizableImageBooleanElement.TextWithImageCellView (this);
			}
			else
			{
				textWithImageCellView.UpdateFrom (this);
			}
			return textWithImageCellView;
		}

		protected abstract UIImage GetImage ();

		public event NSAction Tapped;


		public class TextWithImageCellView : UITableViewCell {
			const int fontSize = 17;
			static UIFont font = UIFont.BoldSystemFontOfSize (fontSize);
			BaseSizableImageBooleanElement parent;
			UILabel label;
			UIButton button;
			const int ImageSpace = 24;
			const int Padding = 16;

			public TextWithImageCellView (BaseSizableImageBooleanElement parent_) : base (UITableViewCellStyle.Value1, parent_.CellKey)
			{
				parent = parent_;
				label = new UILabel () {
					TextAlignment = UITextAlignment.Left,
					Text = parent.Caption,
					Font = parent.Font,
					BackgroundColor = UIColor.Clear
				};
				button = UIButton.FromType (UIButtonType.Custom);
				button.TouchDown += delegate {
					parent.Value = !parent.Value;
					UpdateImage ();
					if (parent.Tapped != null)
						parent.Tapped ();
				};
				ContentView.Add (label);
				ContentView.Add (button);
				UpdateImage ();
			}

			void UpdateImage ()
			{
				button.SetImage (parent.GetImage (), UIControlState.Normal);
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();
	
				var full = ContentView.Bounds;
				var frame = full;
				frame.Height = 22;
				frame.X = Padding;
				frame.Y = (full.Height-frame.Height) / 2;
				frame.Width -= ImageSpace + Padding;

				label.Frame = frame;

				button.Frame = new RectangleF (full.Width - ImageSpace - parent.imageSize.Width, 2, 
				                               parent.imageSize.Width, parent.imageSize.Height);
			}

			public void UpdateFrom (BaseSizableImageBooleanElement newParent)
			{
				label.Font = parent.Font;

				parent = newParent;
				UpdateImage ();
				label.Text = parent.Caption;
				SetNeedsDisplay ();
			}
		}
	}

	public class SizableBooleanImageElement : BaseSizableImageBooleanElement {
		UIImage onImage, offImage;

		public SizableBooleanImageElement (string caption, bool value, UIImage onImage, UIImage offImage, SizeF buttonSize) 
			: base (caption, value, buttonSize)
		{
			this.onImage = onImage;
			this.offImage = offImage;
		}

		protected override UIImage GetImage ()
		{
			if (Value)
				return onImage;
			else
				return offImage;
		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			onImage = offImage = null;
		}
	}
}

