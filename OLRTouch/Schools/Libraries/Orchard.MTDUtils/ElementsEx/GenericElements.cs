using System;
using MonoTouch.Dialog;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Orchard.MTDUtils.Generic
{
	public class StringElementEx<T> : StringElement
	{
		public T Tag = default(T);

		public StringElementEx (string caption, NSAction tapped) : base (caption, tapped)
		{
		}

		public StringElementEx (string caption, string value) : base (caption, value)
		{
		}

		public StringElementEx (string caption) : base (caption)
		{
		}
	}

	public class StyledStringElementEx<T> : StyledStringElement
	{
		public T Tag = default(T);

		public StyledStringElementEx (string caption, NSAction tapped) : base (caption, tapped)
		{
		}

		public StyledStringElementEx (string caption, string value) : base (caption, value)
		{
		}

		public StyledStringElementEx (string caption) : base (caption)
		{
		}

		public StyledStringElementEx (string caption, string value, UITableViewCellStyle style) : base (caption, value)
		{
		}
	}

	public class BadgeElementEx<T> : BadgeElement
	{
		public T Tag = default(T);

		public BadgeElementEx (UIImage badgeImage, string cellText, NSAction tapped) : base (badgeImage, cellText, tapped)
		{
		}

		public BadgeElementEx (UIImage badgeImage, string cellText) : base (badgeImage, cellText)
		{
		}
	}

	public class MessageElementEx<T> : MessageElement
	{
		public T Tag = default(T);

		public MessageElementEx () : base ()
		{
		}

		public MessageElementEx (Action<DialogViewController, UITableView, NSIndexPath> tapped) : base (tapped)
		{
		}
	}

	public abstract class OwnerDrawnElementEx<T> : OwnerDrawnElement
	{
		public T Tag = default(T);

		public OwnerDrawnElementEx (UITableViewCellStyle style, string cellIdentifier) : base (style, cellIdentifier)
		{
		}
	}
}

