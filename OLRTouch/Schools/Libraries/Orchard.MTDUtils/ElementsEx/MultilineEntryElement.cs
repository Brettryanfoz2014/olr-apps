using System;
using MonoTouch.Dialog;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace Orchard.MTDUtils
{
	public class MultilineEntryElement : Element, IElementSizing {
		/// <summary>
		///   The value of the EntryElement
		/// </summary>
		public string Value { 
			get {
				if (entry == null)
					return val;
				var newValue = entry.Text;
				if (newValue == val)
					return val;
				val = newValue;

				if (Changed != null)
					Changed (this, EventArgs.Empty);
				return val;
			}
			set {
				val = value;
				if (entry != null)
					entry.Text = value;
			}
		}
		protected string val;

		/// <summary>
		/// The key used for reusable UITableViewCells.
		/// </summary>
		static NSString entryKey = new NSString ("MultlineEntryElementOrchardEx");
		protected virtual NSString EntryKey {
			get {
				return entryKey;
			}
		}

		int _maxChars = 0;

		public void SetMaxCharacters (int maxChars)
		{
			_maxChars = maxChars;
		}

		// Dimensions for the edit
		public float CaptionMaxWidth = 160f;
		public float EditControlWidth = 160f;

		public bool ReadOnly = false;

		public int Rows = 4;

		/// <summary>
		/// The type of keyboard used for input, you can change
		/// this to use this for numeric input, email addressed,
		/// urls, phones.
		/// </summary>
		public UIKeyboardType KeyboardType {
			get {
				return keyboardType;
			}
			set {
				keyboardType = value;
				if (entry != null)
					entry.KeyboardType = value;
			}
		}

		/// <summary>
		/// The type of Return Key that is displayed on the
		/// keyboard, you can change this to use this for
		/// Done, Return, Save, etc. keys on the keyboard
		/// </summary>
		public UIReturnKeyType? ReturnKeyType {
			get {
				return returnKeyType;
			}
			set {
				returnKeyType = value;
				if (entry != null && returnKeyType.HasValue)
					entry.ReturnKeyType = returnKeyType.Value;
			}
		}

		public UITextAutocapitalizationType AutocapitalizationType {
			get {
				return autocapitalizationType;	
			}
			set { 
				autocapitalizationType = value;
				if (entry != null)
					entry.AutocapitalizationType = value;
			}
		}

		public UITextAutocorrectionType AutocorrectionType { 
			get { 
				return autocorrectionType;
			}
			set { 
				autocorrectionType = value;
				if (entry != null)
					this.autocorrectionType = value;
			}
		}

//		public UITextFieldViewMode ClearButtonMode { 
//			get { 
//				return clearButtonMode;
//			}
//			set { 
//				clearButtonMode = value;
//				if (entry != null)
//					entry.ClearButtonMode = value;
//			}
//		}

		public UITextAlignment TextAlignment {
			get {
				return textalignment;
			}
			set{
				textalignment = value;
				if (entry != null) {
					entry.TextAlignment = textalignment;
				}
			}
		}
		UITextAlignment textalignment = UITextAlignment.Left;
		UIKeyboardType keyboardType = UIKeyboardType.Default;
		UIReturnKeyType? returnKeyType = null;
		UITextAutocapitalizationType autocapitalizationType = UITextAutocapitalizationType.Sentences;
		UITextAutocorrectionType autocorrectionType = UITextAutocorrectionType.Default;
		UITextFieldViewMode clearButtonMode = UITextFieldViewMode.Never;
		bool isPassword, becomeResponder;
		MobileUtilities.Forms.UIPlaceholderTextView entry;
		string placeholder;
		static UIFont font = UIFont.BoldSystemFontOfSize (17);

		public UIFont CaptionFont = null;
		public UIFont EntryFont = null;

		public event EventHandler Changed;
		public event Func<bool> ShouldReturn;
		public EventHandler EntryStarted {get;set;}
		public EventHandler EntryEnded {get;set;}
		/// <summary>
		/// Constructs an EntryElement with the given caption, placeholder and initial value.
		/// </summary>
		/// <param name="caption">
		/// The caption to use
		/// </param>
		/// <param name="placeholder">
		/// Placeholder to display when no value is set.
		/// </param>
		/// <param name="value">
		/// Initial value.
		/// </param>
		public MultilineEntryElement (string caption, string placeholder, string value) : base (caption)
		{ 
			Value = value;
			this.placeholder = placeholder;
		}

		/// <summary>
		/// Constructs an EntryElement for password entry with the given caption, placeholder and initial value.
		/// </summary>
		/// <param name="caption">
		/// The caption to use.
		/// </param>
		/// <param name="placeholder">
		/// Placeholder to display when no value is set.
		/// </param>
		/// <param name="value">
		/// Initial value.
		/// </param>
		/// <param name="isPassword">
		/// True if this should be used to enter a password.
		/// </param>
		public MultilineEntryElement (string caption, string placeholder, string value, bool isPassword) : base (caption)
		{
			Value = value;
			this.isPassword = isPassword;
			this.placeholder = placeholder;
		}

		public override string Summary ()
		{
			return Value;
		}

		// 
		// Computes the X position for the entry by aligning all the entries in the Section
		//
		SizeF ComputeEntryPosition (UITableView tv, UITableViewCell cell)
		{
			Section s = Parent as Section;
			if (s.EntryAlignment.Width != 0)
				return s.EntryAlignment;

			// If all EntryElements have a null Caption, align UITextView with the Caption
			// offset of normal cells (at 10px).

			UIFont f = EntryFont != null ? EntryFont : font;

			SizeF max = new SizeF (-15, tv.StringSize ("M", f).Height * Rows);
			foreach (var e in s.Elements){
				var ee = e as MultilineEntryElement;
				if (ee == null)
					continue;

				if (ee.Caption != null) {
					var size = tv.StringSize (ee.Caption, f);
					if (size.Width > max.Width)
						max = size;
				}
			}
			s.EntryAlignment = new SizeF (EditControlWidth, max.Height);
			return s.EntryAlignment;
		}

		public string PlaceHolder = "";

		protected virtual MobileUtilities.Forms.UIPlaceholderTextView CreateTextField (RectangleF frame)
		{
			var result = new MobileUtilities.Forms.UIPlaceholderTextView (frame, Caption) {
				//AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleLeftMargin,
				//Placeholder = placeholder ?? "",
				SecureTextEntry = isPassword,
				Text = Value ?? "",
				Tag = 1,
				TextAlignment = textalignment
				//ClearButtonMode = ClearButtonMode,
				//BorderStyle = UITextBorderStyle.RoundedRect,
			};

			result.MaxChars = this._maxChars;

			if (EntryFont != null)
				result.Font = EntryFont;

			result.Layer.BorderColor = UIColor.LightGray.CGColor;
			result.Layer.BorderWidth = 1.0f;
			result.Layer.CornerRadius = 5f;
			result.Editable = !ReadOnly;

			return result;
		}

		static readonly NSString passwordKey = new NSString ("MultilineEntryElement+Password");
		static readonly NSString cellkey = new NSString ("MultilineEntryElement");

		protected override NSString CellKey {
			get {
				return isPassword ? passwordKey : cellkey;
			}
		}

		void ResizeCaptionToFit (UITableView tv, UITableViewCell cell)
		{
			var size = tv.StringSize (Caption, CaptionFont != null ? CaptionFont : font, 
			                          CaptionMaxWidth, UILineBreakMode.WordWrap);
			cell.TextLabel.Frame = new RectangleF (cell.TextLabel.Frame.Location.X, 
				cell.TextLabel.Frame.Location.Y, CaptionMaxWidth, size.Height); //ize.Height);

			cell.Layer.Frame = new RectangleF (cell.TextLabel.Frame.Location.X, 
			                                   cell.TextLabel.Frame.Location.Y, CaptionMaxWidth, size.Height);
			cell.TextLabel.SizeToFit ();

			cell.SetNeedsLayout ();
		}

		UITableViewCell cell;
		public override UITableViewCell GetCell (UITableView tv)
		{
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Default, CellKey);
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.TextLabel.Frame = new RectangleF(8, 2, CaptionMaxWidth, 0);

				cell.TextLabel.Font = CaptionFont != null ? CaptionFont : font;
				cell.TextLabel.Text = Caption;
				cell.TextLabel.Lines = 0;

				ResizeCaptionToFit (tv, cell);
			} 

			var offset = (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone) ? 20 : 90;

			cell.Frame = new RectangleF(cell.Frame.X, cell.Frame.Y, tv.Frame.Width-offset, cell.Frame.Height);

			SizeF size = ComputeEntryPosition (tv, cell);
			float yOffset = (cell.ContentView.Bounds.Height - size.Height) / 2 - 1;
			float width = cell.ContentView.Bounds.Width - size.Width;

			if (textalignment == UITextAlignment.Right) {
				// Add padding if right aligned
				width -= 10;
			}

			width = tv.Frame.Width - CaptionMaxWidth - 16f;

			//var entryFrame = new RectangleF (CaptionMaxWidth + 4, yOffset, EditControlWidth, size.Height * Rows + 8);

			var height = GetHeight (this.GetContainerTableView (), this.IndexPath);

			var entryFrame = new RectangleF (16, 2, EditControlWidth - 32 , height - 4);

			if (entry == null) {
				entry = CreateTextField (entryFrame);
//				entry.ValueChanged += delegate {
//					FetchValue ();
//				};
				entry.Ended += delegate {					
					FetchValue ();
					if (EntryEnded != null) {
						EntryEnded (this, null);
					}
					entry.ResignFirstResponder();
				};
				entry.ShouldEndEditing += delegate {
				//entry.ShouldReturn += delegate {

					if (ShouldReturn != null)
						return ShouldReturn ();

					RootElement root = GetImmediateRootElement ();
					MultilineEntryElement focus = null;

					if (root == null)
						return true;

					foreach (var s in root) {
						foreach (var e in s) {
							if (e == this) {
								focus = this;
							} else if (focus != null && e is MultilineEntryElement) {
								focus = e as MultilineEntryElement;
								break;
							}
						}

						if (focus != null && focus != this)
							break;
					}

					//if (focus != this)
					//	focus.BecomeFirstResponder (true);
					//else 
						focus.ResignFirstResponder (true);

					return true;
				};
				entry.Started += delegate {
					MultilineEntryElement self = null;

					if (EntryStarted != null) {
						EntryStarted (this, null);
					}

					if (!returnKeyType.HasValue) {
						var returnType = UIReturnKeyType.Default;

						foreach (var e in (Parent as Section).Elements) {
							if (e == this)
								self = this;
							else if (self != null && e is MultilineEntryElement)
								returnType = UIReturnKeyType.Next;
						}
						entry.ReturnKeyType = returnType;
					} else
						entry.ReturnKeyType = returnKeyType.Value;

					tv.ScrollToRow (IndexPath, UITableViewScrollPosition.Middle, true);
				};
				cell.ContentView.AddSubview (entry);
			} else
				entry.Frame = entryFrame;

			if (becomeResponder){
				entry.BecomeFirstResponder ();
				becomeResponder = false;
			}
			entry.KeyboardType = KeyboardType;

			entry.AutocapitalizationType = AutocapitalizationType;
			entry.AutocorrectionType = AutocorrectionType;

			ResizeCaptionToFit (tv, cell);

			return cell;
		}

		/// <summary>
		///  Copies the value from the UITextView in the EntryElement to the
		//   Value property and raises the Changed event if necessary.
		/// </summary>
		public void FetchValue ()
		{
			if (entry == null)
				return;

			var newValue = entry.Text;
			if (newValue == Value)
				return;

			Value = newValue;

			if (Changed != null)
				Changed (this, EventArgs.Empty);
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing){
				if (entry != null){
					entry.Dispose ();
					entry = null;
				}
			}
		}

		public override void Selected (DialogViewController dvc, UITableView tableView, NSIndexPath indexPath)
		{
			BecomeFirstResponder(true);
			tableView.DeselectRow (indexPath, true);
		}

		public override bool Matches (string text)
		{
			return (Value != null ? Value.IndexOf (text, StringComparison.CurrentCultureIgnoreCase) != -1: false) || base.Matches (text);
		}

		/// <summary>
		/// Makes this cell the first responder (get the focus)
		/// </summary>
		/// <param name="animated">
		/// Whether scrolling to the location of this cell should be animated
		/// </param>
		public virtual void BecomeFirstResponder (bool animated)
		{
			becomeResponder = true;
			var tv = GetContainerTableView ();
			if (tv == null)
				return;
			tv.ScrollToRow (IndexPath, UITableViewScrollPosition.Middle, animated);
			if (entry != null){
				entry.BecomeFirstResponder ();
				becomeResponder = false;
			}
		}

		public virtual void ResignFirstResponder (bool animated)
		{
			becomeResponder = false;
			var tv = GetContainerTableView ();
			if (tv == null)
				return;
			tv.ScrollToRow (IndexPath, UITableViewScrollPosition.Middle, animated);
			if (entry != null)
				entry.ResignFirstResponder ();
		}

		#region IElementSizing implementation

		public float GetHeight (UITableView tableView, NSIndexPath indexPath)
		{
			// Get the height of the edit control

			// Get the height of the entry

			float editHeight = Rows * (tableView.StringSize ("Aq", EntryFont != null ? EntryFont : font, CaptionMaxWidth, UILineBreakMode.WordWrap).Height + 8f) + 8f;

			float captionHeight = tableView.StringSize (this.Caption, CaptionFont != null ? CaptionFont : font, 
			                                           tableView.Frame.Width - CaptionMaxWidth - 16f, UILineBreakMode.WordWrap
			                      ).Height + 8f;

			return editHeight;
			//return Math.Max (editHeight, captionHeight);
		}

		#endregion
	}

	public class SpacingElement : Element, IElementSizing
	{
		float _height = 8f;

		public SpacingElement (float height) : base ("")
		{
			this._height = height;
		}

		static NSString cellKey = new NSString ("SpacingElementOrchex");

		protected override NSString CellKey {
			get {
				return cellKey;
			}
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			var cell = base.GetCell (tv);
			cell.BackgroundColor = UIColor.Clear;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;
		}

		#region IElementSizing implementation

		public float GetHeight (UITableView tableView, NSIndexPath indexPath)
		{
			return _height;
		}

		#endregion
	}

	public class NonSelectedStyledStringElement : StyledStringElement
	{
		public override UITableViewCell GetCell (UITableView tv)
		{
			var result = base.GetCell (tv);
			result.SelectionStyle = UITableViewCellSelectionStyle.None;
			return result;
		}

		public NonSelectedStyledStringElement(string title) : base(title)
		{

		}
	}
}

