using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace Orchard.MTDUtils
{
	public class OrchardDialogViewController : DialogViewController 
	{
		public OrchardDialogViewController (RootElement root, bool pushing) : base (root, pushing)
		{
			Initialize ();
		}

		public OrchardDialogViewController (IntPtr handle) : base (handle)
		{
			Initialize ();
		}

		public OrchardDialogViewController (UITableViewStyle style, RootElement root) : base (style, root)
		{
			Initialize ();
		}

		public OrchardDialogViewController (UITableViewStyle style, RootElement root, bool pushing) : base (style, root, pushing)
		{
			Initialize ();
		}

		public OrchardDialogViewController (RootElement root) : base (root)
		{
			Initialize ();
		}

		protected void Initialize()
		{
			//Root.UnevenRows = true;

		}

		protected void PerformiOS7Check()
		{
			foreach (var sect in Root)
				foreach (var elem in sect) {
					if (elem is DateTimeElement)
						((DateTimeElement)elem).BackgroundColor = UIColor.Clear;
					else if (elem is DateElement)
						((DateElement)elem).BackgroundColor = UIColor.Clear; 
					else if (elem is TimeElement)
						((TimeElement)elem).BackgroundColor = UIColor.Clear;
				} 
		}

		public void HideSeperator()
		{
			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}
	}

	public class ItemForm<T> : OrchardDialogViewController where T : class
	{
		public ItemForm (T item, string title, UITableViewStyle style = UITableViewStyle.Plain) : base (style, null, true)
		{
			// Set the title
			Title = title;
			_item = item;

			Root = SetupForm ();
			Root.UnevenRows = true;
		}

		public void AddModalClose (string text)
		{
			NavigationItem.LeftBarButtonItem = new UIBarButtonItem (text, UIBarButtonItemStyle.Done, 
			                                                        delegate(object sender, EventArgs e)
			                                                        {
				this.DismissModalViewControllerAnimated (true);
			});
		}

		public void AddEditButton ()
		{
			this.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Edit, 
			                                                              StartTheEditMode);
		}

		private void StartTheEditMode (object sender, EventArgs e)
		{
			this.SetEditing (true, true);
			TableView.SetEditing (true, true);

			NavigationItem.SetRightBarButtonItem (
				new UIBarButtonItem (UIBarButtonSystemItem.Done, new EventHandler (FinishEditing)), true);

			DoStartEditMode ();
		}

		protected virtual void DoAfterDelete ()
		{
			// Subclasses to use	
		}

		protected virtual void DoStartEditMode ()
		{
			// Subclasses to use
		}

		private void FinishEditing (object sender, EventArgs e)
		{
			SetEditing (false, true);
			TableView.SetEditing (false, true);
			NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Edit,
			                                                         new EventHandler (StartTheEditMode));
			DoEndEditMode ();
		}

		protected void DoEndEditMode ()
		{
			// Subclasses to use
		}

		public virtual RootElement SetupForm ()
		{
			return new RootElement (Title);
		}

		public virtual bool ShowFormToolbar ()
		{
			return false;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (NavigationController != null)
				NavigationController.SetToolbarHidden (!ShowFormToolbar (), animated);
		}

		public bool CheckOnBackButton = false;
		public bool IsShowingDialog = false;

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (IsShowingDialog)
				return;

			// Save the changes if the item is not null
			if (_item != null && !CheckOnBackButton) {
				SaveData ();
			} else {
				bool hasController = false;
				if (NavigationController == null) {
					return;
				}

				foreach (UIViewController c in NavigationController.ViewControllers) {
					if (c == this) {
						hasController = true;
						break;
					}
				}

				if (!hasController)
					DoBackButton ();
			}
		}

		public virtual void DoBackButton()
		{

		}

		public event EventHandler OnCancel = null;

		protected void DoCancel()
		{
			if (OnCancel != null)
				OnCancel(this, EventArgs.Empty);
		}

		protected void AddCancelButton()
		{
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(UIBarButtonSystemItem.Cancel, 
			                                                        new EventHandler(CancelButtonClick));
		}

		protected void ProcessBackButton(object sender, EventArgs e)
		{
			SaveData();
		}

		public virtual void SaveData()
		{

		}

		protected T _item = null;

		public virtual void LoadData(T item)
		{
			_item = item;
		}

		protected void ClearBackButtonEvent()
		{
			NavigationItem.BackBarButtonItem.Clicked -= new EventHandler(ProcessBackButton);	
		}

		public void CancelButtonClick(object sender, EventArgs e)
		{
			DoCancel();
			NavigationController.PopViewControllerAnimated(true);	
		}

		//public DataObjectObservers NotifyList;

		//		public virtual void DataObjectNotify (object sender, DataObjectUpdateArgs args)
		//		{
		//			// In form based items, subclasses need to handle this
		//		}
	}
}

