using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace Orchard.MTDUtils
{
	public class ItemRequest
	{
		public SeachFormMode Mode;
		public bool IsFirstLoad;
		public int PageSize;
		public int PageNumber;
		public bool HasMoreData;
		public string SearchText;
	}
}
