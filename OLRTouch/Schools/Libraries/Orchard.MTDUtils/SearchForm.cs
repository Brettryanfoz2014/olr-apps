using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Threading.Tasks;

namespace Orchard.MTDUtils
{
	public enum SeachFormMode
	{
		Loading,
		Data,
		SearchData,
		MoreData,
		MoreSearchData
	}

	public abstract class SearchForm<T> : OrchardDialogViewController where T : class
	{
		public SearchForm (string title, UITableViewStyle style = UITableViewStyle.Plain, bool pullToRefresh = true) : base (style, null, true)
		{
//			if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
//				this.TableView.ContentInset = new UIEdgeInsets (this.TopLayoutGuide.Length, 0, 0, 0);

			// Set the title
			Title = title;

			Root = GetLoadingMessageElements (SeachFormMode.Loading);
			//NotifyList = new DataObjectObservers (this);

			// Setup the pull to refresh
			if (pullToRefresh)
				RefreshRequested += AddPullToRefresh;

			// Setup the Searchabaility
			EnableSearch = true;
		}

		private async void AddPullToRefresh(object sender, EventArgs e)
		{
			isReloading = true;

			await RefreshData();

			if (isReloading) {
				isReloading = false;
				ReloadComplete ();
			}
		}

		protected void RemovePullToRefresh()
		{
			RefreshRequested -= AddPullToRefresh;
		}

		#region Virtual Methods that need to be overriden

		ActivityElement _activity;

		protected async virtual Task<T[]> GetItems(ItemRequest details)
		{
			return new T[] { };
		}

		protected virtual RootElement GetLoadingMessageElements(SeachFormMode mode, string searchText = null)
		{
			_activity = new ActivityElement ();
			_activity.Caption = "";

			return new RootElement ("") {
				new Section() {
					_activity
				}
			};
		}

		public virtual RootElement GetNoFoundSearchResultsElements()
		{
			return null;
		}

		public virtual bool ShowFormToolbar()
		{
			return false;
		}

		public abstract Element GetElementForItem (T item);

		#endregion

		protected void DisplayElements(RootElement elements)
		{

		}

		public int CurrentPageNumber = 0;
		public int PageSize = 10;

		public string SearchTerm = "";

		public async Task InternalLoadMore ()
		{
			bool populateData;

			CurrentPageNumber++;

			T[] items;
			ItemRequest details = new ItemRequest () {
				HasMoreData = false,
				IsFirstLoad = false,
				Mode = CurrentMode, 
				PageNumber = CurrentPageNumber,
				PageSize = this.PageSize,
				SearchText = CurrentMode == SeachFormMode.Data ? "" : SearchTerm
			};

			try
			{
				items = await GetItems (details);
			
				if (CurrentMode == SeachFormMode.Data)
					CanLoadMoreData = details.HasMoreData;
				else if (CurrentMode == SeachFormMode.SearchData)
					CanLoadMoreSearch = details.HasMoreData;

				// Update the display with the list
				this.InvokeOnMainThread (delegate {
					if (CurrentMode == SeachFormMode.Data)
						Items.AddRange (items);
					else
						SearchItems.AddRange (items);

					// Create the items
					IsLoadingMore = false;
					DisplayItems (CurrentItems);

					TableView.ReloadData ();
				});
			} catch {
				// Display the message that something went wrong with the load
				DisplayProblemLoading ();
			}
		}

		public async void RunLoadMore ()
		{
			IsLoadingMore = true;
			//TableView.ReloadData();

			// We need to load more items
			await InternalLoadMore();
		}

		public async Task RefreshData ()
		{
			if (_currentMode == SeachFormMode.Data)
				await StartAsyncDataRetrieve ();
			else
				await SearchFor (SearchText);
		}

		void HandleOnRefreshDataLoad (object sender, EventArgs e)
		{
			RefreshData ();

//			InvokeOnMainThread (() => { 
//
//			});
		}

		public void AddModalClose (string text)
		{
			NavigationItem.LeftBarButtonItem = new UIBarButtonItem (text, UIBarButtonItemStyle.Done, 
			                                                        delegate(object sender, EventArgs e)
			                                                        {
				this.DismissModalViewControllerAnimated (true);
			});
		}

		public bool EditButtonOnRight = true;

		private void PlaceEditButton(UIBarButtonItem button)
		{
			if (EditButtonOnRight)
				NavigationItem.SetRightBarButtonItem(button, true);
			else
				NavigationItem.SetLeftBarButtonItem(button, true);
		}

		public void AddEditButton ()
		{
			PlaceEditButton(new UIBarButtonItem (UIBarButtonSystemItem.Edit, 
			                                     StartTheEditMode));
		}

		private void StartTheEditMode (object sender, EventArgs e)
		{
			SetEditing (true, true);

			PlaceEditButton (
				new UIBarButtonItem (UIBarButtonSystemItem.Done, new EventHandler (FinishEditing)));
		}

		public virtual bool CanEditItem(T item)
		{
			return true;
		}

		private void FinishEditing (object sender, EventArgs e)
		{
			SetEditing (false, true);

			PlaceEditButton(new UIBarButtonItem (UIBarButtonSystemItem.Edit,
			                                     new EventHandler (StartTheEditMode)));
		}

		public bool AutoLoadMore = false;

		public List<T> Items = new List<T>();
		public List<T> SearchItems = new List<T>();

		public List<T> CurrentItems {
			get {
				if (CurrentMode == SeachFormMode.Data)
					return Items;
				else
					return SearchItems;
			}
		}

		StartLoadingMoreElement _startLoadingNextPart;

		public virtual Section GetDefaultSection()
		{
			return new Section ();
		}

		public virtual void DisplayItems(List<T> itemsToDisplay)
		{
			List<Element> allElements = new List<Element> ();
			foreach (var i in itemsToDisplay)
				allElements.Add (GetElementForItem (i));

			Section s = GetDefaultSection ();
			s.Elements.Clear ();
			s.Elements.AddRange (allElements);

			// See if we have more elements to load
			if (CanLoadMore)
			{
				// Add the loading more element
				_startLoadingNextPart = new StartLoadingMoreElement ();
				_startLoadingNextPart.OnStartNextDownload += () => {
					// Initiate the load more
					RunLoadMore ();
				};
				s.Elements.Add (_startLoadingNextPart);
			}

			RootElement re = new RootElement (Title) {
				s
			};
			this.Root = re;

			// Display the Elements 
			this.Root.Reload (s, UITableViewRowAnimation.Automatic);
		}

		public bool CanLoadMoreData { get; set; }
		public bool CanLoadMoreSearch { get; set; }

		public bool CanLoadMore { 
			get
			{
				return CurrentMode == SeachFormMode.Data ? CanLoadMoreData : CanLoadMoreSearch;
			}
			set
			{
				if (CurrentMode == SeachFormMode.Data)
				{
					CanLoadMoreData = value;
				} else {
					CanLoadMoreSearch = value;
				}
			}
		}

		void DoLoadMoreItems ()
		{
			IsLoadingMore = true;


		}

		public string LoadMoreText = "Load More...";
		public string IsLoadingMoreText = "Loading...";

		internal bool IsLoadingMore = false;

		public virtual T[] LoadMoreItems()
		{
			return null;
		}

		public virtual T[] LoadMoreSearchItems()
		{
			return null;
		}

		public bool IsShowingDialog = false;
		public bool CheckOnBackButton = false;

		public virtual void SaveData()
		{

		}

		public virtual void DoBackButton()
		{

		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (IsShowingDialog)
				return;

			// Save the changes if the item is not null
			if (!CheckOnBackButton) {
				SaveData ();
			} else {
				bool hasController = false;
				if (NavigationController == null) {
					return;
				}

				foreach (UIViewController c in NavigationController.ViewControllers) {
					if (c == this) {
						hasController = true;
						break;
					}
				}

				if (!hasController)
					DoBackButton ();
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			StartAsyncDataRetrieve ();
		}

		public bool UseNetworkIndicator = false;

		public async Task StartAsyncDataRetrieve()
		{
			CurrentMode = SeachFormMode.Loading;

			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

			try
			{
				await InternalDataRetrieve();
			} catch (Exception e) {
				HandleLoadException (e);
			}
		}

		public void Reload()
		{
			Root = GetLoadingMessageElements (SeachFormMode.Loading);

			StartAsyncDataRetrieve ();
		}

		private SeachFormMode _currentMode = SeachFormMode.Data;
		public event EventHandler OnModeChange = null;

		public SeachFormMode CurrentMode
		{
			get
			{
				return _currentMode;
			}
			set
			{
				if (_currentMode != value)
				{
					// Change the mode
					_currentMode = value;

					if (OnModeChange != null)
						OnModeChange(this, EventArgs.Empty);
				}
			}
		}

		bool IsLoading;

		public async Task InternalDataRetrieve ()
		{
			if (IsLoading)
				return;

			IsLoading = true;

			CurrentPageNumber = 1;

			try {
				CurrentMode = SeachFormMode.Data;

				ItemRequest request = new ItemRequest()
				{
					Mode = CurrentMode,
					HasMoreData = false,
					IsFirstLoad = true,
					PageNumber = 1,
					PageSize = 25
				};

				Items.Clear();

				var itemsToAdd = await GetItems(request);

				Items.AddRange(itemsToAdd);

				CanLoadMore = request.HasMoreData;

				// Process the response here
				this.InvokeOnMainThread (delegate {
					UpdateUI();
				});

			} catch (Exception e) {
				HandleLoadException (e);
			}
		}

		bool isReloading = false;

		public void UpdateUI()
		{
			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

			DisplayItems (Items);

			if (AutoLoadMore && CanLoadMore)
				RunLoadMore ();

			if (isReloading) {
				isReloading = false;
				ReloadComplete ();
			}

			IsLoading = false;
		}

		protected void LoadNewSearchItems ()
		{
			if (AutoLoadMore && CanLoadMore) {
				// TODO: Start the process of loading more
				//(this.tvc.TableView.Delegate as SearchableTableViewController<T>.TableDelegate).RunLoadMore ();
			}
		}

		public string SearchText = "";

		UIView _disableViewOverlay;

		public override void SearchButtonClicked (string text)
		{
			// Show the black overlay on the items

			if (_disableViewOverlay == null) {
				_disableViewOverlay = new UIView (this.TableView.Bounds);
				_disableViewOverlay.BackgroundColor = UIColor.Black;
			}
			_disableViewOverlay.Alpha = 0.5f;

			SearchFor (text);
		}

		public async Task SearchFor(string text)
		{
			// Set the Search Loading Message 
			DisplayElements (GetLoadingMessageElements (SeachFormMode.SearchData, text));

			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

			this.SearchText = text;

			await InternalSearchDataRetrieve();
		}

		public async Task InternalSearchDataRetrieve ()
		{
			CurrentMode = SeachFormMode.SearchData;

			ItemRequest request = new ItemRequest()
			{
				Mode = CurrentMode,
				HasMoreData = false,
				IsFirstLoad = true,
				PageNumber = 1,
				PageSize = 25,
				SearchText = this.SearchText
			};

			SearchItems.Clear ();
			var items = await GetItems (request);
			SearchItems.AddRange (items);

			this.InvokeOnMainThread (delegate { 
				UpdateUIFromSearch(); 
			});
		}

		public void UpdateUIFromSearch ()
		{
			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

			if (SearchItems.Count > 0)
				DisplayItems (SearchItems);
			else
				GetNoFoundSearchResultsElements ();

			if (_disableViewOverlay != null) {
				_disableViewOverlay.Alpha = 0;
				_disableViewOverlay = null;
			}
			_disableViewOverlay = null;

			if (isReloading) {
				isReloading = false;
				ReloadComplete ();
			}
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (NavigationController != null)
				NavigationController.SetToolbarHidden (!ShowFormToolbar (), animated);
		}

		Exception _CurrentException = null;

		protected virtual void HandleLoadException(Exception e)
		{
			_CurrentException = e;
			// Need to display an error message
			this.InvokeOnMainThread(delegate { DisplayProblemLoading(); });  
		}

		public virtual void DisplayProblemLoading()
		{
			// TODO: Handle the error here
			StyledStringElement ss = new StyledStringElement ("There was a problem loading the data");
			Root = new RootElement ("") {
				new Section () {
					ss
				}
			};
		}

		public UIFont RefreshTextFont = null;

		public class SearchFormRefreshTableHeaderView : RefreshTableHeaderView
		{
			UIFont _customFont;

			public SearchFormRefreshTableHeaderView(RectangleF rect, UIFont font) : base(rect)
			{
				_customFont = font;
			}

			public override void CreateViews ()
			{
				base.CreateViews ();

				BackgroundColor = UIColor.White;

				if (System.IO.File.Exists("Images/RefreshArrow.png"))
					ArrowView.Image = UIImage.FromFile ("Images/RefreshArrow.png");

				UpdateFonts ();

				LastUpdateLabel.BackgroundColor = UIColor.Clear;
				StatusLabel.BackgroundColor = UIColor.Clear;
				LastUpdateLabel.BackgroundColor = UIColor.Clear;
				ArrowView.BackgroundColor = UIColor.Clear;
				Activity.BackgroundColor = UIColor.Clear;
			}

			public override void LayoutSubviews ()
			{
				UpdateFonts ();

				base.LayoutSubviews ();
			}

			void UpdateFonts ()
			{
				if (_customFont != null) {
					LastUpdateLabel.Font = _customFont;
					StatusLabel.Font = _customFont;
					LastUpdateLabel.Font = _customFont;
				}
			}

			public override void SetStatus (RefreshViewStatus status)
			{
				base.SetStatus (status);

				UpdateFonts ();
			} 
		}

		public override RefreshTableHeaderView MakeRefreshTableHeaderView (RectangleF rect)
		{
			return new SearchFormRefreshTableHeaderView (rect, RefreshTextFont);
		}
	}
}

