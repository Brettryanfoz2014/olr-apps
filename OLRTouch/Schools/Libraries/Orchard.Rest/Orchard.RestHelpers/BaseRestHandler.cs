using System;
using System.Collections.Generic;
using System.Web;
using System.Reflection;
using System.Text;

namespace Orchard.Utils.REST
{
    public enum RESTRouteType
    {
        ViaDelegate,
        ViaMethod,
        ViaWrappedMethod
    }

    public class RESTRoute
    {
        public string method;
        public string path;
        public RestPath restPath;
        public HandleRESTRequest handler;
        public string MethodName;
        public RESTRouteType RouteType = RESTRouteType.ViaDelegate;
    }
   
    public delegate void HandleRESTRequest(RESTRequest request, RESTResponse response);

    public enum RestPathPartType
    {
        Path,
        Query
    }

    public class RestPathPart
    {
        public RestPathPartType partType = RestPathPartType.Path;
        public bool IsVariable;
        public string Name;
        public string Value;
    }

    public class RestPath
    {
        public List<RestPathPart> parts = new List<RestPathPart>();
        
        private string getParamName(string value)
        {
            return value.Substring(1, value.Length - 2);
        }

        private bool isParameter(string value)
        {
            return value.Length > 2 && 
                value.Substring(0, 1).Equals("{") && 
                value.Substring(value.Length - 1, 1).Equals("}");
        }

        public List<string> GetQueryNames()
        {
            List<string> result = new List<string>();
            foreach (var p in parts)
                if (p.partType == RestPathPartType.Query)
                    result.Add(p.Name);
            return result;
        }

        /// <summary>
        /// path is the bit after the handler
        /// if the url is http://www.something.com/Blah.ashx/working/details?name=John
        /// the path would be /working/details?name=John
        /// </summary>
        /// <param name="path"></param>
        public RestPath(string path)
        {
            string[] pathParts = path.Split(new char[] { '?' });
            if (pathParts.Length > 0)
            {
                foreach (string p in pathParts[0].Split(new char[] { '/' }))
                {
                    RestPathPart newPart = new RestPathPart();
                    newPart.partType = RestPathPartType.Path;
                    newPart.IsVariable = isParameter(p);
                    if (newPart.IsVariable)
                        newPart.Name = getParamName(p).ToLower();
                    else
                        newPart.Value = p.ToLower();
                    parts.Add(newPart);
                }
            }

            if (pathParts.Length > 1)
            {
                // Read in the Querystring
                foreach (string p in pathParts[1].Split(new char[] { '&' }))
                {
                    string[] queryParams = p.Split(new char[] { '=' });

                    RestPathPart newPart = new RestPathPart();
                    newPart.partType = RestPathPartType.Query;
                    newPart.Name = queryParams[0].ToLower();
                    if (queryParams.Length == 1)
                    {
                        newPart.IsVariable = false;
                    }
                    else
                    {
                        newPart.Name = queryParams[0];
                        newPart.IsVariable = isParameter(queryParams[1]);
                        if (newPart.IsVariable)
                            newPart.Value = getParamName(queryParams[1]);
                        else
                            newPart.Value = queryParams[1];
                    }
                    parts.Add(newPart);
                }
            }
        }

        public int GetPathCount()
        {
            int result = 0;
            foreach (var p in parts)
                if (p.partType == RestPathPartType.Path)
                    result++;

            return result;
        }

        public bool DoesMatch(string p, Dictionary<string, string> parameters)
        {
            RestPath rp = new RestPath(p);
            if (rp.GetPathCount() != this.GetPathCount())
                return false;
            else
            {
                for (int i = 0; i < parts.Count; i++)
                {
                    RestPathPart part = parts[i];

                    if (part.partType == RestPathPartType.Path)
                    {
                        // Check that the parts match
                        if (!part.IsVariable)
                        {
                            // The sections must be the same
                            if (!part.Value.ToLower().Equals(rp.parts[i].Value.ToLower()))
                                return false;
                        }
                        else
                        {
                            // Store the value from the variable 
                            parameters[part.Name] = rp.parts[i].Value;
                        }
                    }
                }

                // Check that the query string is valid
                List<string> thisQueryNames = this.GetQueryNames();
                List<string> otherNames = rp.GetQueryNames();
                foreach (string name in otherNames)
                {
                    if (thisQueryNames.Contains(name))
                    {
                        // Store the parameter value if required
                        RestPathPart pp = GetQueryName(name);
                        if (pp.IsVariable)
                        {
                            RestPathPart opp = GetQueryName(name);
                            parameters[name] = HttpContext.Current.Server.UrlDecode(opp.Value);
                        } 
                    }
                    else
                    {
                        // We are missing the query parameter that is required in the path
                        return false;
                    }
                }

                return true;
            }
        }

        private RestPathPart GetQueryName(string name)
        {
            foreach (var item in parts)
            {
                if (item.partType == RestPathPartType.Query && item.Name.Equals(name))
                    return item;
            }
            return null;
        }
    }

    public class RestHandlerAttribute : Attribute
    {
        public string Method;
        public string Path;

        public RestHandlerAttribute(string method, string path)
        {
            this.Method = method;
            this.Path = path;
        }

        public RestHandlerAttribute(string method)
        {
            this.Method = method;
            this.Path = "";
        }
    }

    public class SSLRequiredException : ApplicationException
    {
        public SSLRequiredException()
            : base()
        {
        }
    }
    public abstract class BaseRestHandler : IHttpHandler
    {
        public void HandleNotFoundRequest(HttpContext context)
        {
            context.Response.TrySkipIisCustomErrors = true;
            context.Response.StatusCode = 404;
            context.Response.StatusDescription = "No matching Rest request could be found that matched the request sent.";
        }

        protected void RequireSSL()
        {
            var ctx = _restRequest.context;

            if (!ctx.Request.IsSecureConnection)
            {
                throw new SSLRequiredException();
            }
        }

        RESTRoute restRoute;
        public List<RESTRoute> _routes = new List<RESTRoute>();
        
        // Register the routes on the system
        protected virtual void RegisterRoute(string method, string path, HandleRESTRequest handler)
        {
            RESTRoute route = new RESTRoute()
            {
                method = method,
                path = path,
                restPath  = new RestPath(path),
                handler = handler
            };
            _routes.Add(route);
        }

        protected virtual void RegisterRoute(string method, string path, string methodName, RESTRouteType routeType = RESTRouteType.ViaMethod)
        {
            RESTRoute route = new RESTRoute()
            {
                method = method,
                path = path,
                restPath = new RestPath(path),
                RouteType = routeType,
                MethodName = methodName
            };
            _routes.Add(route);
        }

        protected void ReadInPathAttributes()
        {
            MethodInfo[] methods = this.GetType().GetMethods();
            
            List<KeyValuePair<String, MethodInfo>> items = new List<KeyValuePair<string, MethodInfo>>();

            RestHandlerAttribute attr = null;
            foreach (MethodInfo method in methods)
            {
                attr = Attribute.GetCustomAttribute(method,
                    typeof(RestHandlerAttribute), false) as RestHandlerAttribute;
                if (attr == null)
                    continue;

                // Check to see if they are string methods. If they are then we can register them as a wrapped method
                var isAllStrings = true;
                foreach (var p in method.GetParameters())
                {
                    if (!p.ParameterType.Equals(typeof(string)))
                        isAllStrings = false;
                }

                // Get the route and register as a delegate                
                if (!isAllStrings)
                    RegisterRoute(attr.Method, attr.Path, method.Name);
                else
                    RegisterRoute(attr.Method, attr.Path, method.Name, RESTRouteType.ViaWrappedMethod);
            }            
        }

        public virtual void RegisterRoutes()
        {
            // Subclasses will implement this
        }

        private string getActualRestPath(HttpContext context)
        {
            string pq = context.Request.Url.PathAndQuery;
            int i = pq.IndexOf(".ashx");
            string result = pq.Substring(i + 5);

            // jQuery adds a timestamp to $.ajax calls where cached is set to false in GET requests
            // This compensates for that
            if (context.Request.HttpMethod == "GET")
            {
                int j = result.IndexOf("_=");
                if (j > 0)
                    result = result.Substring(0, j - 1);
            }
            return result;
        }

        /// <summary>
        /// Find the Route (delegate) that will be called based on the route. Search through _routes
        /// </summary>
        public RESTRoute FindRegisteredRoute(HttpContext context, Dictionary<string, string> parameters)
        {
            // Get the URL path after the .ashx for the real path
            string actualPath = getActualRestPath(context);

            // Loop through the routes and find the route that matches the path details of the client
            // If you can't find one then return null
            foreach (var r in _routes)
            {
                if (context.Request.HttpMethod == r.method)
                {
                    if (r.restPath.DoesMatch(actualPath, parameters))
                    {
                        // Update all the Query Parameters
                        foreach (var item in r.restPath.parts)
                        {
                            if (item.partType == RestPathPartType.Query)
                            {
                                parameters[item.Value] = context.Request.QueryString[item.Value];
                            }
                        }

                        return r;
                    }
                }
            }

            return null;
        }

        public bool IsReusable
        {
            get { return false; }
        }

        protected RESTRequest _restRequest = null;
        protected RESTResponse _restResponse = null;

        public void ProcessRequest(HttpContext context)
        {
            ReadInPathAttributes();

            RegisterRoutes();

            RegisterDescriptionRoute();

            // Extract the path information and find the right route
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            RESTRoute foundRoute = FindRegisteredRoute(context, parameters);

            if (foundRoute != null)
            {
                // Convert the HttpContext details to the RESTRequest/RESTReqsponse format
                _restRequest = new RESTRequest();
                _restResponse = new RESTResponse();

                _restRequest.context = context;
                _restRequest.Method = foundRoute.method;
                _restRequest.parameters = parameters;
                _restRequest.UpdateParameters();

                try
                {
                    if (foundRoute.RouteType == RESTRouteType.ViaDelegate) 
                        foundRoute.handler(_restRequest, _restResponse);
                    else if (foundRoute.RouteType == RESTRouteType.ViaMethod)
                    {
                        this.GetType().InvokeMember(foundRoute.MethodName, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance, null,
                            this, new object[] { _restRequest, _restResponse });
                    }
                    else
                    {
                        // Create the strings that will be called and capture the output
                        var thisType = this.GetType();
                        var method = thisType.GetMethod(foundRoute.MethodName);

                        // Get the order of the parameters
                        List<object> parametersPassed = new List<object>();
                        foreach (var parameter in method.GetParameters())
                        {
                            parametersPassed.Add(_restRequest.parameters[parameter.Name]);
                        }

                        var result = this.GetType().InvokeMember(foundRoute.MethodName, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance, null,
                            this, parametersPassed.ToArray());

                        if (result != null)
                            _restResponse.OKDirect(result);
                        else
                            _restResponse.OK<object>(null);
                    }

                    // Process the Request and return the resulting RESTResponse to the context.Request
                    if (_restResponse.wasSuccessful)
                    {
                        context.Response.StatusCode = 200;
                        context.Response.ContentType = _restResponse.ContentType;
                        context.Response.Write(_restResponse.ResponseContent);
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                        context.Response.ContentType = _restResponse.ContentType;
                        context.Response.Write(_restResponse.ResponseContent);
                    }
                }
                catch (SSLRequiredException sslExc)
                {
                    context.Response.TrySkipIisCustomErrors = true;
                    context.Response.StatusCode = 403;
                    context.Response.SubStatusCode = 4;
                    context.Response.StatusDescription = "SSL Required";
                }
                catch (Exception e)
                {
                    context.Response.StatusCode = 500;
                    context.Response.StatusDescription = String.Format("There was a problem executing the rest request: {0} ({1})", foundRoute.path, foundRoute.method);
                }
            }
            else
            {
                HandleNotFoundRequest(context);
            }
        }

        protected virtual bool ShowRoutes()
        {
            return true;
        }

        private void RegisterDescriptionRoute()
        {
            if (ShowRoutes())
			{
                RegisterRoute("GET", "/_routes", ShowListOfRegisteredRoutes);
				RegisterRoute("GET", "/_clientCode", ShowClientCode);
			}
        }

		private void ShowClientCode(RESTRequest request, RESTResponse response)
		{
			// We're creating the C# client code for the operations here
			StringBuilder b = new StringBuilder();

			b.AppendLine("using System;");
			b.AppendLine("using System.Collections.Generic;");
			b.AppendLine("");
			b.AppendLine("// This is a client stub example of the code to call the Service. It requires the ");
			b.AppendLine("// Mobile.Network library to use the JSON Utility functions and requires that you ");
			b.AppendLine("// implement a static Read(JsonValue value) on the loaded types. ");
			b.AppendLine("namespace " + this.GetType().Namespace);
			b.AppendLine("{");
			b.AppendLine("    public class " + this.GetType().Name + "Client");
			b.AppendLine("    {");

			string serverFullUrl = "";
			if (request.context.Request.IsSecureConnection)
				serverFullUrl += "https://";
			else
				serverFullUrl += "http://";

			serverFullUrl += request.context.Request.Url.Host;

			string linkUrl = request.context.Request.Url.LocalPath;
			int i = linkUrl.IndexOf(".ashx");
			linkUrl = linkUrl.Substring(0, i + 5);

			// Add the Server Prefix
			b.AppendLine("    \tpublic string ServerPrefix = \"" + serverFullUrl + linkUrl + "\";");
			b.AppendLine();

			foreach (RESTRoute r in this._routes)
			{
				if (String.IsNullOrEmpty(r.MethodName))
					continue;

				string returnType = "void";

				// Find the Method
				var m = this.GetType().GetMethod(r.MethodName);
				var rt = m.ReturnType;
				var isArray = rt.IsArray;
				returnType = rt.Name;
				var singular = rt.Name;
				if (rt.IsArray)
					singular = singular.Substring(0, singular.Length - 2);

				string fullPath = r.path;

				// Add the Parameters to the call
				string parameterList = "";
				foreach (var p in r.restPath.parts)
				{
					if (p.IsVariable)
					{
						if (parameterList.Length > 0)
							parameterList += ", ";

						parameterList += "string " + p.Name;
					}
				}

				// Write the code to generate the reference
				if (r.method != "GET")
				{
					b.AppendLine("\tpublic " + returnType + " " + r.MethodName + "(" + parameterList + ")");
					b.AppendLine("\t{");
					b.AppendLine("\t\tstring url = ServerPrefix + \"" + fullPath + "\";");
					b.AppendLine("\t}");

					//b.Append(fullPath);
				}
				else
				{
					b.AppendLine("\tpublic " + returnType + " " + r.MethodName + "(" + parameterList + ")");
					b.AppendLine("\t{");

					// String set the new path
					foreach (var p in r.restPath.parts)
					{
						if (p.IsVariable)
						{
							if (parameterList.Length > 0)
							{
								fullPath = fullPath.Replace("{" + p.Name + "}", "\" + " + p.Name + " + \"");
							}
						}
					}

					b.AppendLine("\t\tstring url = ServerPrefix + \"" + fullPath + "\";");

					if (!isArray)
					{
						b.AppendLine("\t\treturn MobileUtilities.Network.JsonUtilities.LoadItemFromWebLink<" + singular + ">(url, (item) => { ");
						b.AppendLine("\t\t\t// Need to return the item based on the JSONObject item");
						b.AppendLine("\t\t\treturn " + singular + ".Read(item); ");
						b.AppendLine("\t\t});");
					} else {
						b.AppendLine("\t\treturn MobileUtilities.Network.JsonUtilities.LoadCollectionFromWebLink<" + singular + ">(url, (item) => { ");
						b.AppendLine("\t\t\t// Need to return the item based on the JSONObject item");
						b.AppendLine("\t\t\treturn " + singular + ".Read(item); ");
						b.AppendLine("\t\t}).ToArray();");
					}
					b.AppendLine("\t}");
					b.AppendLine();
				}
			}

			b.AppendLine("    }");
			b.AppendLine("}");

			response.ContentType = "text/text";
			response.ResponseContent = b.ToString();
			response.wasSuccessful = true;
		}

        private void ShowListOfRegisteredRoutes(RESTRequest request, RESTResponse response)
        {
            StringBuilder b = new StringBuilder();
            b.AppendFormat("<html><body><h1>Routes for {0}</h1><table border='1'><tr><td>Route</td><td>Method</td></tr>", 
                this.GetType().FullName);
            foreach (RESTRoute r in this._routes)
            {
                b.Append("<tr><td>");

                string linkUrl = request.context.Request.Url.LocalPath;
                int i = linkUrl.IndexOf(".ashx");
                linkUrl = linkUrl.Substring(0, i + 5);
 
                string fullPath = linkUrl + r.path;

                if (r.method != "GET")
                    b.Append(fullPath);
                else
                    b.Append(String.Format("<a href='{0}' target='_blank'>{0}</a>", fullPath));

                b.Append("</td><td>");
                b.Append(r.method);
                b.Append("</td></tr>");
            }
            b.Append("</table></body></html>");

            response.ContentType = "text/html";
            response.ResponseContent = b.ToString();
            response.wasSuccessful = true;
        }
    }
}
