﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;

namespace Orchard.Utils.REST
{
    public class JsonHelper
    {
        /// <summary>
        /// JSON Serialization
        /// </summary>
        public static string JsonSerializer<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }

        public static string JsonSerializerByObject(object o)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(o.GetType());
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, o);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public static string PostDataToURL(string url, string method, object item)
        {
            using (var client = new WebClient())
            {
                var dataToPost = Encoding.Default.GetBytes(JsonSerializerByObject(item));
                var result = client.UploadData(url, method, dataToPost);
                return result.ToString();
            }
        }
    }
}
