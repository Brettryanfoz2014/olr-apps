﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orchard.Utils.REST
{
    public class RESTResponse
    {
        public bool wasSuccessful = true;
        public string ContentType = "application/json";
        public string ResponseContent;

        public void OK<T>(T item)
        {
            ResponseContent = JsonHelper.JsonSerializer<T>(item);
        }

        public void OKDirect(object o)
        {
            ResponseContent = JsonHelper.JsonSerializerByObject(o);
        }

        public void Fail<T>(T item)
        {
            ResponseContent = JsonHelper.JsonSerializer<T>(item);
            wasSuccessful = false;
        }
    }
}
