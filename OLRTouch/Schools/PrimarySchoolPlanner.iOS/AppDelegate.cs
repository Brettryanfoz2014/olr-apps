using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Schools;

namespace Schools
{
	/// <summary>
	/// App Delegate for Primary Parent Planner
	/// </summary>

	[Register ("AppDelegate")]
	public partial class AppDelegate : EduAppsBaseAppDelegate
	{
		public override void SetupTheme()
		{
			UIImage navBarImage = UIImage.FromFile("finalicons/menubar.png");
			UINavigationBar.Appearance.SetBackgroundImage(navBarImage, UIBarMetrics.Default);
			
			UITextAttributes att = new UITextAttributes();
			att.Font = UIFont.FromName("STHeitiTC-Medium", 18f);
			UINavigationBar.Appearance.SetTitleTextAttributes(att);

			// Setup the font for the UITableViewCell
			var app = UILabel.AppearanceWhenContainedIn(new Type[] { typeof(UITableViewCell) });
			app.Font = UIFont.FromName("STHeitiTC-Light", 12f);

			if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () < 7.0) {
				UIToolbar.Appearance.SetBackgroundImage (navBarImage, UIToolbarPosition.Any, UIBarMetrics.Default);
				UIImage barButton = UIImage.FromFile ("finalicons/menu-bar-button.png").CreateResizableImage (new UIEdgeInsets (0f, 6f, 0f, 6f));
			
				UIBarButtonItem.Appearance.SetBackgroundImage (barButton, UIControlState.Normal, UIBarMetrics.Default);
			
				UIImage backButton = UIImage.FromFile ("finalicons/back.png").CreateResizableImage (new UIEdgeInsets (0, 15, 0, 8));
				UIBarButtonItem.Appearance.SetBackButtonBackgroundImage (backButton, UIControlState.Normal, UIBarMetrics.Default);
			}

			UITextAttributes attButtonBar = new UITextAttributes();
			attButtonBar.Font = UIFont.FromName("STHeitiTC-Light", 14f);
			UIBarButtonItem.Appearance.SetTitleTextAttributes(attButtonBar, UIControlState.Normal);
			UIBarButtonItem.Appearance.TintColor = UIColor.White;

			UISearchBar.Appearance.BackgroundImage = UIImage.FromFile("finalicons/searchbarbg.png");
			UITabBar.Appearance.BackgroundImage = UIImage.FromFile("finalicons/tabbar-background.png");
			//UIToolbar.Appearance.SetBackgroundImage (navBarImage, UIToolbarPosition.Any, UIBarMetrics.Default);

			// Setup the Mobile Forms theming
			MobileUtilities.Forms.Theme.TextFontName = "STHeitiTC-Light";
			MobileUtilities.Forms.Theme.TextFontSize = 16f;
			MobileUtilities.Forms.Theme.SubTextFontName = "STHeitiTC-Light";
			MobileUtilities.Forms.Theme.SubTextFontSize = 12.5f;
			MobileUtilities.Forms.Theme.ApplyTheme = true;

			UITableViewHeaderFooterView.Appearance.TintColor = UIColor.FromRGB(11, 152, 225);

			UILabel.AppearanceWhenContainedIn(new Type[] { typeof(UITableViewHeaderFooterView) }).Font = 
				UIFont.FromName("STHeitiTC-Medium", 14f);

			UITextAttributes attributes = new UITextAttributes ();
			attributes.TextColor = UIColor.White;
			var ba = UIBarButtonItem.AppearanceWhenContainedIn (new Type[] { typeof(UINavigationBar) });
			ba.SetTitleTextAttributes (attributes, UIControlState.Normal);


			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes {
				TextColor = UIColor.White				
			});

			UIToolbar.Appearance.TintColor = UIColor.FromRGB(11, 152, 225);

		}


		public override void WillEnterForeground (UIApplication application)
		{
			base.WillEnterForeground (application);

			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}

		public override void OnActivated (UIApplication application)
		{
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}


	}

}

