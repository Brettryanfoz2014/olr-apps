using System;

namespace Schools
{
	// This is the version for Primary School Planner - Requires modification
	public class ImageConstants
	{
		public static string Checkmark = "finalicons/checkmark.png";
		
		public static string School = "finalicons/school.png";
		public static string University = "finalicons/university.png";
		public static string CourseGroup = "";
		public static string Course = "";
		public static string Assessment = "";
		public static string Calendar = "finalicons/calendar.png";
		public static string Student = "finalicons/user.png";
		public static string Notices = "finalicons/bullhorn.png";
		
		public static string Assignment = "finalicons/homework.png";
		public static string Exam = "finalicons/exam.png";
		
		public static string TabAssessments = "AppIcon29x29.png";
		public static string TabStudents = "finalicons/friends.png";
		public static string TabCalendar = "finalicons/bar-icon-cal-white.png";

		public const string IconMenu = "finalicons/menu_button_normal.png";
		public const string IconCalendar = "finalicons/menu_calendar.png";
		public const string IconCanteen = "finalicons/menu_canteen.png";
		public const string IconContactDeveloper = "finalicons/menu_contact_developer.png";
		public const string IconContactUs = "finalicons/menu_contact_us.png";
		public const string IconEvents = "finalicons/menu_events.png";
		public const string IconForms = "finalicons/menu_forms.png";
		public const string IconNewsLetters = "finalicons/menu_newsletters.png";
		public const string IconNotices = "finalicons/menu_notices.png";
		public const string IconReportAbsence = "finalicons/menu_report_absence.png";
		public const string IconSchoolPolicies = "finalicons/menu_school_policies.png";
		public const string IconStudents = "finalicons/menu_students.png";
	}
}