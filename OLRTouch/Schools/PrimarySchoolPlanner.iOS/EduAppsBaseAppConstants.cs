using System;
using System.Collections.Generic;
using System.Linq;

namespace Schools
{
	/// <summary>
	/// App Delegate for Primary Parent Planner
	/// </summary>

	public class EduAppsBaseAppConstants
	{
		public const string ApplicationName = "OLR";
		public const string EntriesTitle = "Events";
		public const string EntriesPlural = "Events";
		public const string EntriesSingular = "Event";
		public const string EntriesSearchText = "Search for an event";
		public const string NoticesPlural = "Messages";
		public const string NoticesSearchForText = "Search for a message";

		public const string SchoolTitle = "School";
		public const string SchoolSearchForText = "Search for a School";
		public const string PreviousSchoolSearchForText = "Choose Previous School";

		public const string CalendarEntryPrefix = "[O L R] ";
		public const string CoursesTitle = "Classes";
		public const string CoursesSingular = "Class";
		public const bool ShowCourseGroups = false;

		public const bool FixAssessmentTitle = true;
		public const string FixedAssessmentTitle = "Event";
		public const string SubjectTitle = "Class";
		public const bool FixAssessmentDateTitle = true;
		public const string FixedAssessmentDateTitle = "Date";
		
		public const string HostName = "ppprest.assessmentalert.com";
		public const string HostPrefix = "http://ppprest.assessmentalert.com";
		public const string HostSecurePrefix = "https://ppprest.assessmentalert.com";

		public const string DeveloperEmail = "info@primaryparentplanner.com";

		//public const string HostPrefix = "http://192.168.1.2/AssessmentAlertRest";
		//public const string HostSecurePrefix = "http://192.168.1.2/AssessmentAlertRest";

		public const bool RequireSubscription = true; 

		public const string MenuStudents = "Students";
		public const string MenuEvents = "Events";
		public const string MenuMessages = "Messages";
		public const string MenuNewsletters = "Newsletters";
		public const string MenuNotes = "Notes";
		public const string MenuContactUs = "Contact Us";
		public const string MenuCanteen = "Canteen";
		public const string MenuCalendar = "Calendar";
		public const string MenuReportAbsence = "Report Absence";
		public const string MenuSchoolPolicies = "School Policies";
		public const string MenuContactDeveloper = "Contact Developer";


		public const string DocumentTypeForms = "1";
		public const string DocumentTypeNewsletters = "2";
		public const string DocumentTypeSchoolPolicies = "3";

		public const string MessageNoInfo = "Your school has not uploaded information in this section yet.";
		public const string MessageNoSchool = "No school is selected.";
		public const string MessageTitle = "Please Note";

		public const long 	OLRSchoolId = 14912;
		public const string AddClassesForText = "Add Classes";
	}

}
