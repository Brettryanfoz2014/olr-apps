using System;
using MobileUtilities.Forms;
using Schools;
using MessageUI;
using UIKit;

namespace Schools
{
	public class DiagnosticsViewController : ViewEditFormController<object>
	{
		public DiagnosticsViewController ()
		{
			Title = "Diagnose";

			TabBarItem.Image = UIImage. FromFile(ImageConstants.Exam);
		}

		MFMailComposeViewController _mailController;

		Entry memoContent;

		public override void SetupForm ()
		{
			base.SetupForm ();

			string content = "";

			GeneralSettings settings = null;
			using (var db = DataConnection.GetConnection())
			{
				settings = GeneralSettings.GetSettings(db);
				content += "Push Token: " + settings.PushDeviceToken + Environment.NewLine;

				var students = db.Query<Student>("select * from Student");
				foreach (var s in students)
				{
					content += s.Name + Environment.NewLine;
				}
			}

			var details = new Section("Info");
			memoContent = details.AddMemo ("Device", "", "Device Information", false);
			memoContent.Value = content;

			var button = details.AddButton ("Send");
			button.OnClick += (sender, e) => {
				// Send the message
				_mailController = new MFMailComposeViewController ();
				_mailController.SetToRecipients (new string[]{"glenn@orchardebusiness.com"});
				_mailController.SetSubject ("EduApps Diagnostics for " + EduAppsBaseAppConstants.ApplicationName);
				_mailController.SetMessageBody (memoContent.Value, false);

				_mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
					Console.WriteLine (args.Result.ToString ());
					args.Controller.DismissViewController (true, null);
				};

				this.PresentViewController (_mailController, true, null);
			};
			sections.Add(details);
		}
	}
}

