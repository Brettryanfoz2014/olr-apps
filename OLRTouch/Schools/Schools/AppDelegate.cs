using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MobileUtilities.Forms;
using MobileUtilities.Mapping;
using System.Threading;
using System.Net;
using MobileUtilities.Network;
using Mindscape.Raygun4Net;

namespace Schools
{
	/// <summary>
	/// This is the AppDelegate for Assessment Alert
	/// </summary>

	[Register ("AppDelegate")]
	public partial class AppDelegate : EduAppsBaseAppDelegate
	{
		public override void SetupTheme()
		{
			System.Threading.Tasks.Task.Run (() => {
				CloudSubscriptionStorage.DoSync ();
			});

			UIImage navBarImage = UIImage.FromFile("finalicons/menubar.png");
			UINavigationBar.Appearance.SetBackgroundImage(navBarImage, UIBarMetrics.Default);

			//UITextAttributes att = new UITextAttributes();
			//att.Font = UIFont.FromName("Verdana", 12);
			//UINavigationBar.Appearance.SetTitleTextAttributes(att);

			UIToolbar.Appearance.SetBackgroundImage(navBarImage, UIToolbarPosition.Any, UIBarMetrics.Default);
			UISearchBar.Appearance.BackgroundImage = navBarImage;

			if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () < 7.0) {
				UIImage barButton = UIImage.FromFile ("finalicons/menu-bar-button.png").CreateResizableImage (new UIEdgeInsets (0f, 6f, 0f, 6f));
				UIBarButtonItem.Appearance.SetBackgroundImage (barButton, UIControlState.Normal, UIBarMetrics.Default);
				UIImage backButton = UIImage.FromFile ("finalicons/back.png").CreateResizableImage (new UIEdgeInsets (0, 15, 0, 8));
				UIBarButtonItem.Appearance.SetBackButtonBackgroundImage (backButton, UIControlState.Normal, UIBarMetrics.Default);

				UITextAttributes att = new UITextAttributes();
				att.Font = UIFont.BoldSystemFontOfSize (UIFont.SystemFontSize);
				att.TextColor = UIColor.White;
				UINavigationBar.Appearance.SetTitleTextAttributes(att);
			}

			//UIImage tabBarBackground = UIImage.FromFile("finalicons/tabbar-background.png");
			
			//UITabBar.Appearance.BackgroundImage = tabBarBackground;
			//UITabBar.Appearance.SelectionIndicatorImage = UIImage.FromFile("finalicons/tabbar-selected.png");
			


			//UILabel.AppearanceWhenContainedIn(new Type[] { typeof(UITableViewHeaderFooterView) })
			//	.Font = UIFont.FromName(Theme.TextFontName, 8f);

			MobileUtilities.Forms.Theme.ApplyTheme = false;
			//MobileUtilities.Forms.Theme.ApplyNavigationTitleColor = true;
			//MobileUtilities.Forms.Theme.NavigationTitleColor = UIColor.White;

			UITextAttributes attributes = new UITextAttributes ();
			attributes.TextColor = UIColor.White;
			var ba = UIBarButtonItem.AppearanceWhenContainedIn (new Type[] { typeof(UINavigationBar) });
			ba.SetTitleTextAttributes (attributes, UIControlState.Normal);

			if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
				UIApplication.SharedApplication.SetStatusBarStyle (UIStatusBarStyle.LightContent, true);
		}

		protected override void BeforeSetup()
		{
			RaygunClient.Attach("JwK8UAavL7RYcoboOHyQLA==");
		}

		public override void WillEnterForeground (UIApplication application)
		{
			base.WillEnterForeground (application);

			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}

		public override void OnActivated (UIApplication application)
		{
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}

		/*protected override UIViewController[] GetTabViewControllers ()
		{
			return new UIViewController[] { 
				studentNav, assessmentsNav, ctrlNotices, ctrlCalendar 
			};
		}*/
	}
}

