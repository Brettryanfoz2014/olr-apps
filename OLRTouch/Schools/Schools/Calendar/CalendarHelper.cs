using System;
using EventKit;
using System.Collections.Generic;
using Foundation;

namespace Schools
{
	public abstract class CalendarStore
	{
		public abstract CalendarWrapper GetCalendarByName(string name);
		public abstract Dictionary<string, string> GetCalendars();
		public abstract CalendarWrapper GetDefaultCalendar ();
		public abstract string[] GetCalendarNames();
	}

	public class iOSCalendarStore : CalendarStore
	{
		EKEventStore _eventStore;

		public iOSCalendarStore (EKEventStore eventStore)
		{
			_eventStore = eventStore;
		}

		public override CalendarWrapper GetCalendarByName(string name)
		{
			foreach (var cal in _eventStore.Calendars)
				if (cal.Title == name)
					return new iOSCalendarWrapper(cal);

			return null;
		}

		public override Dictionary<string, string> GetCalendars()
		{
			Dictionary<string, string> result = new Dictionary<string, string>();
			foreach (var cal in _eventStore.Calendars)
				result[cal.Title] = cal.CalendarIdentifier; 

			return result;
		}

		public override CalendarWrapper GetDefaultCalendar ()
		{
			return new iOSCalendarWrapper(_eventStore.DefaultCalendarForNewEvents);
//			var all = GetCalendars();
//			foreach (string calName in all.Keys)
//				if (calName.ToLower().Equals("default") || calName.ToLower().Equals("calendar"))
//					return GetCalendarByName(calName);
//
//			return null;
		}

		public override string[] GetCalendarNames()
		{
			List<string> result = new List<string>();
			foreach (var cal in _eventStore.Calendars)
				result.Add (cal.Title); 

			result.Sort();
			
			return result.ToArray();
		}
	}
}

