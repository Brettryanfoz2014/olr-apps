using System;
using SQLite;

namespace Schools
{
	// aka. Year Group. K through to 13 as the year group
	public class CourseGroup
	{
		//Like the School year
		[PrimaryKey, AutoIncrement]
		public int CoreId { get; set; }

		[Indexed]
		public long Id { get; set; }

		public string CourseGroupName { get; set; }
		public long SchoolId { get; set; }
	}
}

