using System;
using System.Json;
using System.Collections.Generic;

namespace Schools
{
	// A class that handles sending the details of the registered devices to the server for processing
	public class DeviceStudentUpdate
	{
		public string deviceID { get; set; }

		public StudentCourseInfo[] students;

		public bool ThreeWeekAlert;
		public bool TwoWeekAlert;
		public bool OneWeekAlert;
		public bool ThreeDayAlert;
		public bool OneDayAlert;

		public string GetJson()
		{
			JsonObject o = new JsonObject();
			o.Add("deviceID", deviceID);
			o.Add("platformType", AssessmentAlertDevice.PlatformType);

			List<JsonValue> studentsList = new List<JsonValue>();
			foreach (StudentCourseInfo sci in students)
			{
				studentsList.Add(sci.GetEntry());
			}

			o.Add("students", new JsonArray(studentsList));
			o.Add("ThreeWeekAlert", new JsonPrimitive(ThreeWeekAlert));
			o.Add("TwoWeekAlert", new JsonPrimitive(TwoWeekAlert));
			o.Add("OneWeekAlert", new JsonPrimitive(OneWeekAlert));
			o.Add("ThreeDayAlert", new JsonPrimitive(ThreeDayAlert));
			o.Add("OneDayAlert", new JsonPrimitive(OneDayAlert));

			return o.ToString();
		}

		public void ReadFromDB()
		{
			using (var db = DataConnection.GetConnection())
			{
				var settings = GeneralSettings.GetSettings(db);

				ThreeWeekAlert = settings.ThreeWeekNotify;
				TwoWeekAlert = settings.TwoWeekNotify;
				OneWeekAlert = settings.OneWeekNotify;
				ThreeDayAlert = settings.ThreeDayNotify;
				OneDayAlert = settings.OneDayNotify;

				deviceID = settings.PushDeviceToken;

				// Read in the students
				var dbStudents = db.Query<Student>("select * from Student");
				var listOfStudents = new List<StudentCourseInfo>();
				foreach (var s in dbStudents)
				{
					StudentCourseInfo entry = new StudentCourseInfo();
					entry.SchoolId = Convert.ToInt32(s.SchoolId);
					entry.StudentName = s.Name;

					List<int> allCourses = new List<int>();
					var studentCourses = db.Query<StudentCourse>("select * from StudentCourse where StudentId = ?", s.Id);
					foreach (var c in studentCourses)
						allCourses.Add(Convert.ToInt32(c.CourseId));
					entry.Courses = allCourses.ToArray();
					listOfStudents.Add(entry);
				}
				this.students = listOfStudents.ToArray();
			} 
		}

		static DeviceStudentUpdate _updateOfInfo;
		static SendDeviceUpdate PushNotificationsUpdate;

		public static void PushChanges()
		{
			_updateOfInfo = new DeviceStudentUpdate();
			_updateOfInfo.ReadFromDB();
			
			PushNotificationsUpdate = new SendDeviceUpdate();
			PushNotificationsUpdate.SendDetails(_updateOfInfo);
		}
	}

	public class StudentCourseInfo
	{
		public string StudentName;
		public int SchoolId;
		public int[] Courses;

		public JsonObject GetEntry()
		{
			JsonObject o = new JsonObject();
			o.Add("name", StudentName);
			o.Add("schoolId", SchoolId);

			List<JsonValue> courses = new List<JsonValue>();
			foreach (int c in Courses)
				courses.Add(new JsonPrimitive(c));
			o.Add("courses", new JsonArray(courses));

			return o;
		}
	}
}

