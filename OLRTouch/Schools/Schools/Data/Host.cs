using System;
using System.Json;
using System.Collections.Generic;
using System.Net;
using System.IO;

namespace Schools
{
	public class Host
	{
		//public static string Name_Test = "192.168.1.8";
		//public static string Prefix_Test = "http://192.168.1.8/AssessmentAlertRest";
		//public static string SecurePrefix_Test = "http://192.168.1.8/AssessmentAlertRest";

		public static string Name 
		{
			get {
				return EduAppsBaseAppConstants.HostName;
			}
		}

		public static string Prefix
		{
			get
			{
				return EduAppsBaseAppConstants.HostPrefix;
			}
		}

		public static string SecurePrefix 
		{
			get
			{
				return EduAppsBaseAppConstants.HostSecurePrefix;
			}
		}

		public static string Combine (string path)
		{
			return Prefix + path;
		}

		public static string CombineSecure (string path)
		{
			return SecurePrefix + path;
		}

		#region Methods for creating the URLs for the App

		public static string GetSchoolsByPageURL(int Records_Per_Search, int Page_Number, float latitude, float longitude)
		{
			return Combine("/rest/Schools.ashx/SchoolsByPage/" + Records_Per_Search + "/" + Page_Number + "/" + latitude + "," + longitude);
		}

		public static string GetSchoolsByPageURL(int Records_Per_Search, int Page_Number)
		{
			return Combine("/rest/Schools.ashx/SchoolsByPage/" + Records_Per_Search + "/" + Page_Number + "/na");
		}

		public static string GetSchoolsByPageWithSearchURL(int Records_Per_Search, int Page_Number, float latitude, float longitude, string searchTerm)
		{
			return Combine("/rest/Schools.ashx/SearchSchoolsByPage/" + Records_Per_Search + "/" + Page_Number + "/" + latitude + "," + longitude + "?term=" + searchTerm);
		}

		public static string GetSchoolsByPageWithSearchURL(int Records_Per_Search, int Page_Number, string searchTerm)
		{
			return Combine("/rest/Schools.ashx/SearchSchoolsByPage/" + Records_Per_Search + "/" + Page_Number + "/na?term=" + searchTerm);
		}

		public static string GetSchoolDetailsPage (long id)
		{
			return Combine ("/rest/Schools.ashx/GetSchool/" + id);
		}

		public static string GetCourseGroupsByPageURL(int Records_Per_Search, int Page_Number, int schoolId)
		{
			return Combine(String.Format("/rest/CourseGroups.ashx/SchoolCourseGroups/{0}/{1}/{2}", schoolId, Records_Per_Search, Page_Number));
		}

		public static string GetCourseGroupsByPageWithSearchURL(int Records_Per_Search, int Page_Number, int schoolId, string searchTerm)
		{
			return Combine(String.Format ("/rest/CourseGroups.ashx/SearchSchoolCourseGroups/{0}/{1}/{2}?term={3}", 
				schoolId, Records_Per_Search, Page_Number, searchTerm));
		}

		public static string GetCoursesByPageURL(int Records_Per_Search, int Page_Number, int schoolId, int courseGroupId)
		{
			return Combine(String.Format ("/rest/Courses.ashx/GetCourses/{0}/{1}/{2}/{3}", schoolId, courseGroupId, Records_Per_Search, Page_Number));
		}

		public static string GetCoursesByPageWithSearchURL(int Records_Per_Search, int Page_Number, int schoolId, int courseGroupId, string searchTerm)
		{
			return Combine(String.Format ("/rest/Courses.ashx/SearchCourses/{0}/{1}/{2}/{3}?term={4}", schoolId, courseGroupId, Records_Per_Search, Page_Number, searchTerm));
		}

		public static string GetAssessmentsURL(int[] coursesId)
		{
			var allCoursesList = String.Join (",", coursesId);
			return Combine(String.Format ("/rest/Assessments.ashx/AssessmentsForCourses/{0}", allCoursesList));
		}

		public static string GetNoticesURL (long[] courses)
		{
			var allCoursesList = String.Join (",", courses);
			return Combine(String.Format ("/rest/Notifications.ashx/NotificationsForCourses/{0}", allCoursesList));
		}

		public static string GetNoticesURL ()
		{
			return Combine(String.Format ("/rest/Notifications.ashx/NotificationsForCourses/"));
		}

		public static string GetSchoolDocumentsURL(int documentType)
		{
			// Returns the prefix for larger loading
			return Combine (String.Format ("/rest/Documents.ashx/SchoolDocuments/{0}/", documentType));
		}

		public static string GetSchoolCrestURL(long schoolId) {
			return Combine (String.Format ("/rest/Schools.ashx/GetSchool/{0}", schoolId));
		}

		public static string GetSchoolAbsenceURL() {
			return Combine ("/rest/Schools.ashx/NewAbsence");
		}
		#endregion
	}
}
