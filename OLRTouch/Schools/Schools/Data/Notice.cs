using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	public class DateUtils
	{
		public static DateTime FromString(string value)
		{
			int i = value.IndexOf('(');
			int j = value.IndexOf(')');
			string datePart = value.Substring(i+1, j - i - 1);

			int k = datePart.IndexOf('+');
			string dp = datePart.Substring(0, k);
			string tzp = datePart.Substring(k+1);

			DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToInt64(dp));
			DateTime runtimeKnowsThisisUtc = DateTime.SpecifyKind (dt, DateTimeKind.Utc);
			DateTime localVersion = runtimeKnowsThisisUtc.ToLocalTime ();
			return localVersion;
		}
	}

	public class Notice
	{
		[AutoIncrement, PrimaryKey]
		public long NotificationId;

		public string NoticeMessage;

		public string DateOfNotice;

		public DateTime GetLocalMessageDate()
		{
			return DateUtils.FromString (DateOfNotice);
		}

		public bool IsAWebLink()
		{
			Uri test = null;
			return Uri.TryCreate (NoticeMessage, UriKind.Absolute, out test) && 
				(test.Scheme == "http" || test.Scheme == "https");
		}
	}

}
