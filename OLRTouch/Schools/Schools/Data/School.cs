using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	public enum SchoolType : int
	{
		School = 1,
		University = 2,
		PrimarySchool = 3,
		MiddleSchool = 4,
		HighSchool = 5
	}

	public class School
	{
		[PrimaryKey, AutoIncrement]
		public int CoreId { get; set; }

		[Indexed]
		public long Id
		{
			get; set;
		}

		public SchoolType SchoolType
		{
			get; set;
		}

		[Indexed]
		public string SchoolName
		{
			get; set;
		}

		public string Address
		{
			get; set;
		}

		public bool HasLocation
		{
			get; set;
		}

		public float Latitude 
		{
			get; set;
		}

		public float Longtitude
		{
			get; set;
		}

		public string PhoneNumber
		{
			get; set;
		}

		public string FaxNumber 
		{
			get; set;
		}

		public string Email
		{
			get; set;
		}

		public string WebSite
		{
			get; set;
		}

		public bool RequiresSubscription {
			get;
			set;
		}

		public string CanteenUrl
		{
			get;
			set;
		}

		public string CrestUrl
		{
			get;
			set;
		}

		public static List<School> getMySchools() {
			SQLiteConnection db = DataConnection.GetConnection ();
			List<School> mySchools = new List<School> ();
			try {
				mySchools = db.Query<School>("select * from School where Id in (select SchoolId from Course where Id in (select CourseId from StudentCourse) )");
			} finally {
				DataConnection.ReleaseConnection ();
			}
			return mySchools;
		}

		public static School getMyCurrentSchool() {
			var schools = getMySchools ();

			SQLiteConnection db = DataConnection.GetConnection ();
			School mySchoolInfo = null;

			try {
				GeneralSettings settings = GeneralSettings.GetSettings (db);
				for ( int i = 0 ; i < schools.Count ; i++ ) {
					if ( schools[i].Id == settings.MySchoolId ) {
						mySchoolInfo = schools[i];
						break;
					}
				}
				if ( mySchoolInfo == null && schools.Count > 0 ) {
					mySchoolInfo = schools[0];
					settings.MySchoolId = mySchoolInfo.Id;
					db.Update(settings);
				}
			} finally {
				DataConnection.ReleaseConnection ();
			}

			return mySchoolInfo;
		}

		public static void setMyCurrentSchool(long schoolId) {
			SQLiteConnection db = DataConnection.GetConnection ();
			try {
				GeneralSettings settings = GeneralSettings.GetSettings (db);
				settings.MySchoolId = schoolId;
				db.Update(settings);
			} finally {
				DataConnection.ReleaseConnection ();
			}

		}
	}
}

