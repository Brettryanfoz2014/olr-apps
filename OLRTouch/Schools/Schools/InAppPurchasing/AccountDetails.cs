using System;
using System.Text;
using System.Net;
using System.IO;
using MobileUtilities.Data;
using System.Json;
using System.Collections.Generic;

namespace Schools {

	public class AccountDetails : BaseEncryptedData
	{
		public string Username;
		public string PwdHash;

		public override string ToString ()
		{
			JsonObject o = new JsonObject(new KeyValuePair<string, JsonValue>[] {
				new KeyValuePair<string, JsonValue>("Username", new JsonPrimitive(Username)),
				new KeyValuePair<string, JsonValue>("PwdHash", new JsonPrimitive(PwdHash))
			});

			// Need to create the value that is returned
			return Salt(12) + o.ToString() + Salt(7);
		}

		public static AccountDetails FromString (string value, bool isEncrypted)
		{
			if (isEncrypted)
				value = EncryptionHelper.DecryptString(value, "FdwtRyk!vtYma2d");

			// Get rid of the first 6
			value = value.Substring(12);
			value = value.Substring(0, value.Length - 7);

			// We've got the JSON Details left
			var o = JsonObject.Parse(value);

			AccountDetails details = new AccountDetails();
			details.Username = o["Username"];
			details.PwdHash = o["PwdHash"];
			
			return details;
		}
	}
}