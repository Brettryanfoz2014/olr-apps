using System;
using System.Text;
using System.Net;
using System.IO;
using System.Json;
using MobileUtilities.Data;

namespace Schools {

	public class BaseEncryptedData
	{
		protected string okChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_";
		
		protected char GetLetter()
		{
			int num = _random.Next(0, okChars.Length); 
			return okChars[num];
		}
		
		protected string Salt(int size)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i <= size; i++)
				sb.Append(GetLetter());
			return sb.ToString();
		}

		public string Encrypt ()
		{
			return EncryptionHelper.EncryptString(ToString(), "FdwtRyk!vtYma2d");
		}

		Random _random = new Random();
	}
	
}