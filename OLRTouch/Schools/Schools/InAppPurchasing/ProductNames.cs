using System;
using MonoTouch.Foundation;

namespace Schools
{
	// This is the file for Assessment Alert. Other variants such as PPP and WMG need to use their own
	public class ProductNames
	{
		public static NSString AppName = new NSString("com.assessmentalert.app");
		public static NSString YearSubscription = new NSString("com.assessmentalert.oneyear");
	}
}

