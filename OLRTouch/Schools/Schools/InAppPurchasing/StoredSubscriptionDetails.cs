using System;
using System.Text;
using System.Net;
using System.IO;
using MobileUtilities.Data;
using System.Json;
using System.Collections.Generic;

namespace Schools {

	public class StoredSubscriptionDetails : BaseEncryptedData
	{
		public string transactionId;
		public DateTime expiryDate;
		public string platformType;
		
		string PlatformAsInt (string platformType)
		{
			switch (platformType.ToLower())
			{
			case "ios":
				return "1";
			case "droid":
				return "2";
			case "win":
				return "3";
			default:
				return "0";
			}
		}
		
		public override string ToString ()
		{
			// Need to create the value that is returned
			return Salt(6) + PlatformAsInt(platformType) + this.transactionId + expiryDate.ToString("yyyyMMdd") + Salt(5);
		}

		public static StoredSubscriptionDetails FromString(string value, bool isEncrypted)
		{
			if (isEncrypted)
				value = EncryptionHelper.DecryptString(value, "FdwtRyk!vtYma2d");
			
			// Get rid of the first 6
			value = value.Substring(6);
			value = value.Substring(0, value.Length - 5);
			
			string platform = "unknown";
			var p = value.Substring(0, 1);
			switch (p)
			{
			case "1":
				platform = "iOS";
				break;
			case "2":
				platform = "Droid";
				break;
			case "3":
				platform = "Windows";
				break;
			}
			
			var transId = value.Substring(1, value.Length - 9);
			var expiryDate = value.Substring(value.Length - 8);
			
			int y = Convert.ToInt32(expiryDate.Substring(0, 4));
			int m = Convert.ToInt32(expiryDate.Substring(4, 2));
			int d = Convert.ToInt32(expiryDate.Substring(6, 2));
			
			DateTime actualExpiry = new DateTime(y, m, d);
			
			return new StoredSubscriptionDetails() {
				expiryDate = actualExpiry,
				transactionId = transId,
				platformType = platform
			};
		}
	}
}