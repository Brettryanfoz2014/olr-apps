using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using System.Json;
using MobileUtilities.Data;
using CoreGraphics;
using CoreGraphics;
using MonoTouch.Dialog;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

using GoogleAnalytics.iOS;

namespace Schools
{
	public class DocumentsTypeSearcher : SearchViewController<SchoolDocument>
	{
		int _documentType = 1;
		UIImage _mainImage;

		public DocumentsTypeSearcher (String documentType, string title, string imageLocation, string searchText) : base(title)
		{
			_documentType = Int32.Parse(documentType);
			Title = title;

			_mainImage = UIImage.FromFile (imageLocation);

			TabBarItem.Image = _mainImage;
			SearchBarPlaceholder = searchText;
			IsGrouped = false;
			ShowGroupIndex = false;
		}

		public override float GetCellHeight ()
		{
			return 78f;
		}

		static NSString cellIdent = new NSString("SchoolDocumentIdent");

		public override UITableViewCell DrawCell (SchoolDocument item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent) as MessageSummaryCell;

			if (cell == null) {
				cell = new MessageSummaryCell ();
			}

			string schoolName = "";

			// Get the School
			using (var db = DataConnection.GetConnection ())
			{
				var school = LocalCache.LoadSchoolFromCache(db, item.SchoolId);
				schoolName = school.SchoolName;
			}

			cell.Update (schoolName, item.Description, item.Title, item.GetAddedTime (), false, 0);
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

			return cell;
		}

		WebNavigateSection _webNav;

		public override void DoRowSelected (SchoolDocument item)
		{
			base.DoRowSelected (item);

			_webNav = new WebNavigateSection (item.DocumentLink, item.Title);
			NavigationController.PushViewController (_webNav, true);
		}

		public override NSString GetCellIdentifier ()
		{
			return AssessmentCell.ID;
		}

		public override bool CanEditItem (SchoolDocument item)
		{
			return false;
		}

		public override void ApplyThemeToCell (SchoolDocument item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}

		public bool HasLoadedEver = false;

		protected List<string> GetAllCourseIds ()
		{
			List<string> result = new List<string> ();

			using (var conn = DataConnection.GetConnection ())
			{
				List<StudentCourse> courses = conn.Query<StudentCourse> ("select * from StudentCourse");

				foreach (StudentCourse sc in courses) {
					if (!result.Contains(sc.CourseId.ToString()))
						result.Add(sc.CourseId.ToString());
				}
			}

			return result;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Refresh, DoReload);

			AssessmentReloader.Instance.OnReload += (sender, e) => {
				BeginInvokeOnMainThread(delegate {
					Reload ();
				});
			};
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "DocumentsTypeSearcher");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());

			StartAsyncDataRetrieve ();
		}

		SchoolDocumentDetails _schoolDocuments;

		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();

			#if DEBUG
			string id = "TEST_AUTO_SYSTEM";
			#else
			string id = "inapp_assessments"; //subscriptionDetails.transactionId;
			#endif

			var headers = new Dictionary<string, string>();
			headers ["transid"] = id;

			var list = GetAllCourseIds ();

			Items.Clear ();

			_schoolDocuments = new SchoolDocumentDetails ();

			if (list.Count > 0) {
				JsonLoaders.LoadCollectionFromWebLinkWithIds<SchoolDocument> (
					Host.GetSchoolDocumentsURL (this._documentType), 
					list, 21, 
					headers, 
					(o) => {
						var documentDetails = JsonLoaders.LoadSchoolDocumentDetails(o);

						_schoolDocuments.SchoolsCount += documentDetails.SchoolsCount;
						_schoolDocuments.SchoolsSubscribedCount += documentDetails.SchoolsSubscribedCount;

						Items.AddRange(documentDetails.documents);

						return null;
					}
				);
			} else {
				Items = new List<SchoolDocument>();
			}

			if (Items.Count == 0) {
				BeginInvokeOnMainThread (delegate {
					DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoInfo);
				});				
			}
		}

		void SetupChangeMonitor ()
		{
			AssessmentReloader.Instance.OnReload += DoReload;
		}

		public override void DoSearchDataRetrieve ()
		{
			base.DoSearchDataRetrieve ();

			SearchItems.Clear ();

			var lowerText = SearchText.ToLower();

			// See if we have any Students that match that name
			foreach (var item in Items)
				if (item.Title.ToLower().Contains(lowerText) || item.Description.ToLower().Contains(lowerText))
					SearchItems.Add(item);
		}

		private void DoReload(object sender, EventArgs e)
		{
			BeginInvokeOnMainThread (delegate {
				StartAsyncDataRetrieve ("Reloading " + EduAppsBaseAppConstants.NoticesPlural, "");
			});
		}

		PopupNotifierController NoNetworkPopup;
		bool ShowNoNetworkOnFirstAppear = false;
		bool HasShown = false;

		void DoShowNoNetwork()
		{
			BeginInvokeOnMainThread(delegate {
				if (NoNetworkPopup == null)
					NoNetworkPopup = new PopupNotifierController (this.View, "No network connectivity", "Showing known " + EduAppsBaseAppConstants.EntriesPlural) { 
					AutomaticTiming = false,
					Location = PopupNotifierLocation.Bottom,
					BackgroundColor = UIColor.Blue
				};
				NoNetworkPopup.AnimStart += (sender, e) => {
					tvc.TableView.Frame = new CGRect(tvc.TableView.Frame.Left, tvc.TableView.Frame.Top, 
						tvc.TableView.Frame.Width, tvc.TableView.Frame.Height - NoNetworkPopup.View.Frame.Height);
				};
				NoNetworkPopup.AnimStop += (sender, e) => {
					tvc.TableView.Frame = new CGRect(tvc.TableView.Frame.Left, tvc.TableView.Frame.Top, 
						tvc.TableView.Frame.Width, tvc.TableView.Frame.Height + NoNetworkPopup.View.Frame.Height);
				};

				NoNetworkPopup.Popup ();
			});
		}

		void ShowNoNetwork ()
		{
			if (HasShown) {
				DoShowNoNetwork();
			} else {
				ShowNoNetworkOnFirstAppear = true;
			}
		}

		void HideNoNetwork()
		{
			if (NoNetworkPopup != null)
				NoNetworkPopup.Hide();
		}
	}

	public class MessageSummaryCell : UITableViewCell {

		static UIFont SenderFont = UIFont.FromName(MobileUtilities.Forms.Theme.TextFontName, 19);
		static UIFont SubjectFont = UIFont.FromName (MobileUtilities.Forms.Theme.TextFontName, 14);
		static UIFont TextFont = UIFont.FromName (MobileUtilities.Forms.Theme.TextFontName, 13);
		static UIFont CountFont = UIFont.FromName (MobileUtilities.Forms.Theme.TextFontName, 13);
		public string Sender { get; private set; }
		public string Body { get; private set; }
		public string Subject { get; private set; }
		public DateTime Date { get; private set; }
		public bool NewFlag  { get; private set; }
		public int MessageCount  { get; private set; }

		static CGGradient gradient;

		static MessageSummaryCell ()
		{
			using (var colorspace = CGColorSpace.CreateDeviceRGB ()){
				gradient = new CGGradient (colorspace, new nfloat [] { /*				 first */ .52f, .69f, .96f, 1, /*				 second */ .12f, .31f, .67f, 1 }, null); //new float [] { 0, 1 });
			}
		}

		public MessageSummaryCell ()
		{
			BackgroundColor = UIColor.Clear;
		}

		public void Update (string sender, string body, string subject, DateTime date, bool newFlag, int messageCount)
		{
			Sender = sender;
			Body = body;
			Subject = subject;
			Date = date;
			NewFlag = newFlag;
			MessageCount = messageCount;
			SetNeedsDisplay ();
		}

		public override void Draw (CGRect rect)
		{
			const int padright = 21;
			var ctx = UIGraphics.GetCurrentContext ();
			nfloat boxWidth;
			CGSize ssize;

			if (MessageCount > 0){
				var ms = MessageCount.ToString ();

				ssize = UIKit.UIStringDrawing.StringSize (ms, CountFont);;
				boxWidth = (float)Math.Min (22 + ssize.Width, 18);
				var crect = new CGRect (Bounds.Width-20-boxWidth, 32, boxWidth, 16);

				UIColor.Gray.SetFill ();
				GraphicsUtil.FillRoundedRect (ctx, crect, 3);
				UIColor.White.SetColor ();
				crect.X += 5;

				ms.DrawString (crect, CountFont);
				//DrawString (ms, crect, CountFont);

				boxWidth += padright;
			} else
				boxWidth = 0;

			UIColor.FromRGB (36, 112, 216).SetColor ();
			var diff = DateTime.Now - Date;
			var now = DateTime.Now;
			string label;
			if (now.Day == Date.Day && now.Month == Date.Month && now.Year == Date.Year)
				label = Date.ToShortTimeString ();
			else if (diff <= TimeSpan.FromHours (24))
				label = "Yesterday";
			else if (diff < TimeSpan.FromDays (6))
				label = Date.ToString ("dddd");
			else
				label = Date.ToShortDateString ();
			
			ssize = UIKit.UIStringDrawing.StringSize (label, SubjectFont);
			nfloat dateSize = ssize.Width + padright + 5;
			label.DrawString(new CGRect (Bounds.Width-dateSize, 6, dateSize, 14), SubjectFont, UILineBreakMode.Clip, UITextAlignment.Left);


			const int offset = 33;
			nfloat bw = Bounds.Width-offset;

			UIColor.Black.SetColor ();
			Sender.DrawString(new CGPoint (offset, 2), bw-dateSize, SenderFont, UILineBreakMode.TailTruncation);
			Subject.DrawString(new CGPoint (offset, 23), bw-offset-boxWidth, SubjectFont, UILineBreakMode.TailTruncation);

			//DrawString (Sender, new CGPoint (offset, 2), bw-dateSize, SenderFont, UILineBreakMode.TailTruncation);
			//DrawString (Subject, new CGPoint (offset, 23), bw-offset-boxWidth, SubjectFont, UILineBreakMode.TailTruncation);

			//UIColor.Black.SetFill ();
			//ctx.FillRect (new RectangleF (offset, 40, bw-boxWidth, 34));
			UIColor.Gray.SetColor ();
			Body.DrawString(new CGRect (offset, 40, bw-boxWidth, 34), TextFont, UILineBreakMode.TailTruncation, UITextAlignment.Left);
			//DrawString (Body, new CGRect (offset, 40, bw-boxWidth, 34), TextFont, UILineBreakMode.TailTruncation, UITextAlignment.Left);

			if (NewFlag){
				ctx.SaveState ();
				ctx.AddEllipseInRect (new CGRect (10, 32, 12, 12));
				ctx.Clip ();
				ctx.DrawLinearGradient (gradient, new CGPoint (10, 32), new CGPoint (22, 44), CGGradientDrawingOptions.DrawsAfterEndLocation);
				ctx.RestoreState ();
			}
		}
	}
}
