using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using System.Json;
using MobileUtilities.Data;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class NoticesSearcher : SearchViewController<Notice>
	{
		public NoticesSearcher () : base(EduAppsBaseAppConstants.NoticesPlural)
		{
			Title = EduAppsBaseAppConstants.MenuMessages;
			TabBarItem.Image = UIImage.FromFile (ImageConstants.IconNotices);
			SearchBarPlaceholder = EduAppsBaseAppConstants.NoticesSearchForText;
			IsGrouped = false;
			ShowGroupIndex = false;
		}

		public override void DataObjectNotify (object sender, DataObjectUpdateArgs args)
		{
			base.DataObjectNotify (sender, args);
		}

		static NSString cellIdent = new NSString("NoticesIdent");

		public override UITableViewCell DrawCell (Notice item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);

			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
			}

			cell.TextLabel.Text = item.GetLocalMessageDate().ToShortDateString() + " " + item.GetLocalMessageDate().ToShortTimeString();
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

			cell.DetailTextLabel.Text = item.NoticeMessage;
			cell.ImageView.Image = UIImage.FromFile (ImageConstants.Notices);

			return cell;
		}

		NoticeDetailForm form;
		WebNavigateSection _webNav;

		public override void DoRowSelected (Notice item)
		{
			base.DoRowSelected (item);

			if (!item.IsAWebLink ()) {
				form = new NoticeDetailForm ();
				form.LoadAndSetup (item);
				NavigationController.PushViewController (form, true);
			} else {
				_webNav = new WebNavigateSection (item.NoticeMessage, "Message");
				NavigationController.PushViewController (_webNav, true);
			}
		}

		public override NSString GetCellIdentifier ()
		{
			return AssessmentCell.ID;
		}

		public override bool CanEditItem (Notice item)
		{
			return false;
		}

		public override void ApplyThemeToCell (Notice item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}

		public bool HasLoadedEver = false;

		protected List<string> GetAllCourseIds ()
		{
			List<string> result = new List<string> ();

			using (var conn = DataConnection.GetConnection ())
			{
				List<StudentCourse> courses = conn.Query<StudentCourse> ("select * from StudentCourse");

				foreach (StudentCourse sc in courses) {
					if (!result.Contains(sc.CourseId.ToString()))
						result.Add(sc.CourseId.ToString());
				}
			}

			return result;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Refresh, (s, e) => { Reload(); });

			AssessmentReloader.Instance.OnReload += (sender, e) => { 
				BeginInvokeOnMainThread(delegate {
					Reload ();
				});
			};

		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "NoticesSearcher");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());

			StartAsyncDataRetrieve ();
		}

		/*
		public override void UpdateUI ()
		{
			resetBadge ();
			base.UpdateUI ();
		}

		private void resetBadge(){
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
			this.TabBarItem.BadgeValue = null;
		}
*/
		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();

			var list = GetAllCourseIds ();

			if (list.Count > 0) {
				Items = JsonLoaders.LoadCollectionFromWebLinkWithIds<Notice> (
					Host.GetNoticesURL (), 
					list, 25, 
					new Dictionary<string, string>(), 
					(o) => {
						return JsonLoaders.LoadNotice (o); 
					}
				);

			} else {
				Items = new List<Notice>();
			}

			if (Items.Count == 0) {
//				BeginInvokeOnMainThread (delegate {
//					DialogHelpers.ShowMessage ("Error", EduAppsBaseAppConstants.MessageNoInfo);
//				});				
			}

			//resetBadge ();
		}

		public override void DoSearchDataRetrieve ()
		{
			base.DoSearchDataRetrieve ();

			SearchItems.Clear ();

			var lowerText = SearchText.ToLower();

			// See if we have any Students that match that name
			foreach (var item in Items)
				if (item.NoticeMessage.ToLower().Contains(lowerText))
					SearchItems.Add(item);
		}

		PopupNotifierController NoNetworkPopup;
		bool ShowNoNetworkOnFirstAppear = false;
		bool HasShown = false;

		void DoShowNoNetwork()
		{
			BeginInvokeOnMainThread(delegate {
				if (NoNetworkPopup == null)
					NoNetworkPopup = new PopupNotifierController (this.View, "No network connectivity", "Showing known " + EduAppsBaseAppConstants.EntriesPlural) { 
					AutomaticTiming = false,
					Location = PopupNotifierLocation.Bottom,
					BackgroundColor = UIColor.Blue
				};
				NoNetworkPopup.AnimStart += (sender, e) => {
					tvc.TableView.Frame = new CoreGraphics.CGRect(tvc.TableView.Frame.Left, tvc.TableView.Frame.Top, 
					                                                    tvc.TableView.Frame.Width, tvc.TableView.Frame.Height - NoNetworkPopup.View.Frame.Height);
				};
				NoNetworkPopup.AnimStop += (sender, e) => {
					tvc.TableView.Frame = new CoreGraphics.CGRect(tvc.TableView.Frame.Left, tvc.TableView.Frame.Top, 
					                                                    tvc.TableView.Frame.Width, tvc.TableView.Frame.Height + NoNetworkPopup.View.Frame.Height);
				};

				NoNetworkPopup.Popup ();
			});
		}

		void ShowNoNetwork ()
		{
			if (HasShown) {
				DoShowNoNetwork();
			} else {
				ShowNoNetworkOnFirstAppear = true;
			}
		}

		void HideNoNetwork()
		{
			if (NoNetworkPopup != null)
				NoNetworkPopup.Hide();
		}
	}

}

