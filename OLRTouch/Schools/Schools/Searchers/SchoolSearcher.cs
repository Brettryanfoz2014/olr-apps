using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using System.Collections.Generic;
using MBProgressHUD;
using RestSharp;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class BaseSchoolSearcher : SearchViewController<School>
	{
		public BaseSchoolSearcher () : base(EduAppsBaseAppConstants.SchoolTitle)
		{
			SearchBarPlaceholder = EduAppsBaseAppConstants.SchoolSearchForText;
		}

		public static NSString cellIdent = new NSString("SchoolsFlyweight");

		public override NSString GetCellIdentifier ()
		{
			return cellIdent;
		}

		public override UITableViewCell DrawCell (School item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);

			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
			}

			cell.TextLabel.Text = item.SchoolName;
			cell.DetailTextLabel.Text = item.Address;

			if (item.RequiresSubscription)
				cell.DetailTextLabel.Text = "(Requires subscription): " + cell.DetailTextLabel.Text;

			cell.DetailTextLabel.Font = UIFont.FromName (cell.DetailTextLabel.Font.Name, 10f);

			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.ImageView.Image = null;

//			if (item.SchoolType == SchoolType.School || item.SchoolType == SchoolType.PrimarySchool || item.SchoolType == SchoolType.MiddleSchool 
//				|| item.SchoolType == SchoolType.HighSchool)
//			{
//				cell.ImageView.Image = UIImage.FromFile (ImageConstants.School);
//			} else {
//				cell.ImageView.Image = UIImage.FromFile (ImageConstants.University);
//			}

			return cell;
		}

		public override void ApplyThemeToCell (School item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}
	}

	public class EnrolledSchoolSearcher : BaseSchoolSearcher
	{
		public EnrolledSchoolSearcher () : base()
		{
			Title = "School";
			SearchBarPlaceholder = EduAppsBaseAppConstants.SchoolSearchForText;
			TabBarItem.Image = UIImage.FromFile ("finalicons/298.png");
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			StartAsyncDataRetrieve ();

			AssessmentReloader.Instance.OnReload += (sender, e) => { 
				BeginInvokeOnMainThread(delegate {
					Reload ();
				});
			};
		}

		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();
			Items.Clear ();
			Items.AddRange (School.getMySchools ());

//			List<long> schoolIds = new List<long>();
//
//			using (var db = DataConnection.GetConnection())
//			{
//				Items.Clear ();
//
//				var coursesEnrolled = db.Query<StudentCourse>("select * from StudentCourse");
//				foreach (StudentCourse c in coursesEnrolled)
//				{
//					var localCourse = LocalCache.LoadCourseFromCache(db, c.CourseId);
//
//					if (!schoolIds.Contains(localCourse.SchoolId))
//					{
//						schoolIds.Add(localCourse.SchoolId);
//						Items.Add(LocalCache.LoadSchoolFromCache(db, localCourse.SchoolId));
//					}
//				}
//			}
		}

		public override void DoSearchDataRetrieve ()
		{
			base.DoSearchDataRetrieve ();

			SearchItems.Clear ();
			foreach (var item in Items)
				if (item.SchoolName.ToLower ().Contains (this.SearchText.ToLower ()) ||
				    item.Address.ToLower ().Contains (this.SearchText.ToLower ()))
					SearchItems.Add (item);
		}

		DisplaySchoolController _schoolDisplay;

		public override void DoRowSelected (School item)
		{
			School.setMyCurrentSchool (item.Id);
			NavigationController.PopViewController (true);
			/*
			var hud = new MTMBProgressHUD (View) {
				LabelText = "Loading School Details...",
				RemoveFromSuperViewOnHide = true
			};

			View.AddSubview (hud);

			hud.Show (animated: true);

			// Load the School from the network if we can
			var client = new RestClient (Host.GetSchoolDetailsPage(item.Id));
			var request = new RestRequest (Method.GET);
			client.ExecuteAsync (request, response => {
				// Parse the Json for the school
				try
				{
					var o = System.Json.JsonObject.Parse(response.Content);

					// Get the School Details
					var actualSchool = JsonLoaders.LoadSchool(o as System.Json.JsonObject);
					using (var db = DataConnection.GetConnection())
					{
						LocalCache.StoreSchoolToCache(db, actualSchool);
					}

					item.Address = actualSchool.Address;
					item.CanteenUrl = actualSchool.CanteenUrl;
					item.CoreId = actualSchool.CoreId;
					item.Email = actualSchool.Email;
					item.FaxNumber = actualSchool.FaxNumber;
					item.HasLocation = actualSchool.HasLocation;
					item.Id = actualSchool.Id;
					item.Latitude = actualSchool.Latitude;
					item.Longtitude = actualSchool.Longtitude;
					item.PhoneNumber = actualSchool.PhoneNumber;
					item.RequiresSubscription = actualSchool.RequiresSubscription;
					item.SchoolName = actualSchool.SchoolName;
					item.SchoolType = actualSchool.SchoolType;
					item.WebSite = actualSchool.WebSite;
				} catch {
					// We have network issues or something
				}

				BeginInvokeOnMainThread(delegate {
					hud.Hide(true, 0);

					var _schoolDisplay = new DisplaySchoolNiceController (item);
					NavigationController.PushViewController (_schoolDisplay, true);
				});
			});*/
		}
	}

	public class SchoolSearcher : BaseSchoolSearcher
	{
		public SchoolSearcher () : base()
		{
			SearchBarPlaceholder = EduAppsBaseAppConstants.SchoolSearchForText;
		}

		public StudentDetails StudentDetails = null;

		int Data_PageNumber = 1;
		int Search_PageNumber = 1; 
		int Records_Per_Search = 30;

		#region No Network Support

		PopupNotifierController NoNetworkPopup;
		bool ShowNoNetworkOnFirstAppear = false;
		bool HasShown = false;

		public override void ViewDidAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			HasShown = true;

			if (ShowNoNetworkOnFirstAppear)
			{
				DoShowNoNetwork();
				ShowNoNetworkOnFirstAppear = false;
			}

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "SchoolSearcher");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

		void DoShowNoNetwork()
		{
			BeginInvokeOnMainThread(delegate {
				if (NoNetworkPopup == null)
					NoNetworkPopup = new PopupNotifierController (this.View, "No network connectivity", "Displaying known schools") { 
						AutomaticTiming = false,
						Location = PopupNotifierLocation.Bottom,
						BackgroundColor = UIColor.Blue
					};

				NoNetworkPopup.Popup ();
			});
		}

		void ShowNoNetwork ()
		{
			if (HasShown) {
				DoShowNoNetwork();
			} else {
				ShowNoNetworkOnFirstAppear = true;
			}
		}

		void HideNoNetwork()
		{
			if (NoNetworkPopup != null)
				NoNetworkPopup.Hide();
		}

		#endregion

		public class GetSearchPageDetails
		{
			public string pagesize;
			public string pagenumber;
			public string location;
		}

		public string GetSchoolSearchData(string pagesize, string pagenumber, string location)
		{
			GetSearchPageDetails result = new GetSearchPageDetails ();
			result.pagesize = pagesize;
			result.pagenumber = pagenumber;
			result.location = location;

			return MobileUtilities.Network.JsonUtilities.Serialize<GetSearchPageDetails> (result);
		}

		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();

			Items.Clear ();

			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					Data_PageNumber = 1;

					// Get the list of schools from the web site
					string url = Host.Prefix + "/rest/Schools.ashx/SchoolsByPageDetail"; 

					var list = JsonLoaders.LoadCollectionFromWebLinkWithPOST<School> (
						url, GetSchoolSearchData(Records_Per_Search.ToString(), "1", CurrentLocation.AsQueryString),  (o) => {
						return JsonLoaders.LoadSchool (o);	
					}, "inapplocation");

					LocalCache.StoreSchools(list);

					Items.AddRange (list);

					// Determine if we can load more records
					Data_PageNumber++;
					CanLoadMoreData = list.Count == Records_Per_Search;
					HideNoNetwork();

					couldConnect = true;
				} 
			}
			catch (Exception failedToLoad) {
				couldConnect = false;
			}

			if (!couldConnect)
			{
				using (var db = DataConnection.GetConnection())
				{
					var schools = db.Query<School>("select * from School");
					Items.AddRange(schools);
				}
				
				ShowNoNetwork();
			}
		}

		public override School[] LoadMoreItems ()
		{
			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {

					string url = Host.Prefix + "/rest/Schools.ashx/SchoolsByPageDetail"; 

					var list = JsonLoaders.LoadCollectionFromWebLinkWithPOST<School> (
						url, GetSchoolSearchData(Records_Per_Search.ToString(), Data_PageNumber.ToString(), CurrentLocation.AsQueryString),  (o) => {
						return JsonLoaders.LoadSchool (o);	
					}, "inapplocation");
				
					LocalCache.StoreSchools(list);

					// Determine if we can load more records
					Data_PageNumber++;
					CanLoadMoreData = list.Count == Records_Per_Search;

					HideNoNetwork();

					return list.ToArray ();
				}
			} catch {
				couldConnect = false;
			}

			// Return the results from the cache
			if (couldConnect == false)
				ShowNoNetwork();

			return new School[] { };
		}


		public class GetSearchPageWithTermDetails
		{
			public string pagesize;
			public string pagenumber;
			public string location;
			public string term;
		}

		public string GetSchoolSearchWithTermData(string pagesize, string pagenumber, string location, string term)
		{
			GetSearchPageWithTermDetails result = new GetSearchPageWithTermDetails ();
			result.pagesize = pagesize;
			result.pagenumber = pagenumber;
			result.location = location;
			result.term = term;

			return MobileUtilities.Network.JsonUtilities.Serialize<GetSearchPageWithTermDetails> (result);
		}

		public override void DoSearchDataRetrieve ()
		{
			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					SearchItems.Clear ();

					Search_PageNumber = 1;

					string url = Host.Prefix + "/rest/Schools.ashx/SearchSchoolsByPageDetail";

					var list = JsonLoaders.LoadCollectionFromWebLinkWithPOST<School> (
						url, GetSchoolSearchWithTermData(Records_Per_Search.ToString(), "1", CurrentLocation.AsQueryString, SearchText),
						(o) => {
						return JsonLoaders.LoadSchool (o);	}, "inappsearch");

					// Store the list to the Cache
					LocalCache.StoreSchools(list);

					SearchItems.AddRange (list);

					Search_PageNumber++;
					CanLoadMoreSearch = list.Count == Records_Per_Search;

					HideNoNetwork();

					couldConnect = true;
				} 
			} catch {
				couldConnect = false;
			}

			if (!couldConnect)
			{
				// Load the items from the cache
				using (var db = DataConnection.GetConnection())
				{
					var schools = db.Query<School>("select * from School where SchoolName like ?", '%' + SearchText + '%');
					SearchItems.AddRange(schools);
				}

				ShowNoNetwork();
			}
		}

		public override School[] LoadMoreSearchItems ()
		{
			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {

					string url = Host.Prefix + "/rest/Schools.ashx/SearchSchoolsByPageDetail";

					var list = JsonLoaders.LoadCollectionFromWebLinkWithPOST<School> (
						url, GetSchoolSearchWithTermData(Records_Per_Search.ToString(), Search_PageNumber.ToString(), CurrentLocation.AsQueryString, SearchText),
						(o) => {
						return JsonLoaders.LoadSchool (o);	}, "inappsearch");

					// Store the list to the Cache
					LocalCache.StoreSchools(list);

					Search_PageNumber++;
					CanLoadMoreSearch = list.Count == Records_Per_Search;

					HideNoNetwork();

					couldConnect = true;
					return list.ToArray ();
				}
			} catch {
				couldConnect = false;
			}

			ShowNoNetwork();
			CanLoadMoreSearch = false;
			return new School[] { };
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			StartAsyncDataRetrieve("Loading Schools...", "");

			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Done, (sender, e) => {
				StudentDetails.SaveDetails();
				DismissViewController (true, null);
			});

			SetupPullToRefresh();
		}

		private CourseGroupSearcher _cgSearcher = null;
		private CourseSearcher _cSearcher = null;

		public override void DoRowSelected (School item)
		{
			base.DoRowSelected (item);

			if (EduAppsBaseAppConstants.ShowCourseGroups)
			{
				// Need to display the Course Groups for the selected school
				_cgSearcher = new CourseGroupSearcher();
				_cgSearcher.school = item;
				_cgSearcher.StudentDetails = StudentDetails;
				NavigationController.PushViewController(_cgSearcher, true);
			} else {
				// Show the Courses for the selected school
				_cSearcher = new CourseSearcher();
				_cSearcher.school = item;
				_cSearcher.coursegroup = new CourseGroup();
				_cSearcher.coursegroup.CourseGroupName = "Main";
				_cSearcher.coursegroup.Id = -1;
				_cSearcher.coursegroup.SchoolId = item.Id;
				_cSearcher.StudentDetails = StudentDetails;

				NavigationController.PushViewController(_cSearcher, true);
			}
		}
	}
}

