using System;
using System.Collections.Generic;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using MBProgressHUD;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class StudentSearcher: SearchViewController<Student>
	{
		UIBarButtonItem btnEdit, btnDone, btnAdd, spacer;
		public StudentSearcher () : base("Students")
		{
			Title = EduAppsBaseAppConstants.MenuStudents;
			TabBarItem.Image = UIImage.FromFile (ImageConstants.IconStudents);
			SearchBarPlaceholder = "Search for a Student";
		}

		public static NSString cellIdent = new NSString("StudentsFlyweight");
		
		public override NSString GetCellIdentifier ()
		{
			return cellIdent;
		}

		public override UITableViewCell DrawCell (Student item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);
			
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
			}

			if (item.Name.Length > 0) {
				cell.TextLabel.Text = item.Name;
				cell.TextLabel.Alpha = 1.0f;
			} else {
				cell.TextLabel.Text = "No Name";
				cell.TextLabel.Alpha = 0.5f;
			}

			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.ImageView.Image = UIImage.FromFile (ImageConstants.Student);
			
			return cell;
		}

		public override void ApplyThemeToCell (Student item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}

		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();

			Items.Clear ();
			using (var conn = DataConnection.GetConnection ())
			{
				var list = conn.Query<Student> ("select * from Student");
				Items.AddRange (list);
			} 
		}

		public override void DoSearchDataRetrieve ()
		{
			base.DoSearchDataRetrieve ();

			SearchItems.Clear ();
			var likeText = "%" + SearchText + "%";
			using (var conn = DataConnection.GetConnection ())
			{
				var list = conn.Query<Student> ("select * from Student where Name like ?", likeText);
				SearchItems.AddRange (list);
			} 
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			StartAsyncDataRetrieve("Loading Students...");

			btnAdd = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Add, DoAddStudent);
			btnEdit = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Edit, StartStudentEdit);
			btnDone = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Done, FinishStudentEdit);
			spacer = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace) { Width = 50 };


			UITextAttributes att = new UITextAttributes();
			att.Font = UIFont.FromName("STHeitiTC-Medium", 20f);
			att.TextColor = UIColor.FromRGB(11, 152, 225);

			/*UITextAttributes att = new UITextAttributes();
			att.Font = UIFont.BoldSystemFontOfSize (UIFont.SystemFontSize);
			att.TextColor = UIColor.FromRGB(11, 152, 225);*/

			btnAdd.SetTitleTextAttributes (att, UIControlState.Normal);
			btnEdit.SetTitleTextAttributes (att, UIControlState.Normal);
			btnDone.SetTitleTextAttributes (att, UIControlState.Normal);


			if (!DataConnection.hasStudents())
			{
				// Show the Setup Wizard from the first screen
				DoAddStudent(this, EventArgs.Empty);
			}

			SetupPullToRefresh();

			FinishStudentEdit (this, EventArgs.Empty);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (HomeController.isFirstRun) {
				NavigationController.PopToRootViewController (true);
			} else {
				NavigationController.ToolbarHidden = false;
			}

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "StudentSearcher");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());

		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated); 
		}

		public void GoToHome(object sender, EventArgs e) {
			NavigationController.PopViewController (false);
			NavigationController.PushViewController (new HomeController (), false);
		}

		public void StartStudentEdit (object sender, EventArgs e)
		{
			tvc.SetEditing (true, true);
			SetToolbarItems (new UIBarButtonItem[]{ spacer, btnDone,spacer, btnAdd , spacer}, false);
		}

		public void FinishStudentEdit (object sender, EventArgs e)
		{
			tvc.SetEditing (false, true);
			SetToolbarItems (new UIBarButtonItem[]{ spacer, btnEdit, spacer, btnAdd , spacer }, false);
		}


		StudentDetails _details = null;

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			NavigationController.ToolbarHidden = true;
		}

		private void DoAddStudent(object sender, EventArgs e)
		{
			// Add the Student Controller
			newStudent = new Student();
			_details = new StudentDetails();
			_details.student = newStudent;
			_details.AfterSave += (Student s) => {
				Items.Add(s);
				this.tvc.RefreshTableDisplay();
				//ShowFinishedAddingStudents();
				//App.Current.RequiresDataReload = true;
				AssessmentReloader.Instance.Reload ();
			};

			long schoolId = -1;

			if (Items != null && Items.Count > 0) {
				var db = DataConnection.GetConnection();
				try
				{
					List<Student> students = db.Query<Student>("select * from Student where Id = ?", Items[0].Id);

					if (students.Count >= 1)
					{
						// Get the School Names
						var coursesEnrolled = db.Query<StudentCourse>("select * from StudentCourse where StudentId = ?", Items[0].Id);
						foreach (StudentCourse c in coursesEnrolled)
						{
							var localCourse = LocalCache.LoadCourseFromCache(db, c.CourseId);

							if (localCourse.SchoolId != 0){
								schoolId = localCourse.SchoolId;
								break;
							}
						}
					}
				} finally {
					DataConnection.ReleaseConnection();
				}
			}

			MainNavController nav = new MainNavController ();

			// Add the Student screen
			//studentCtrl = new NewStudentController();
			//studentCtrl.LoadAndSetup(_details);
			//navMan.PushViewController(studentCtrl, true);

			// Add the splash screen
			if (schoolId != -1) {
				studentCtrl = new NewStudentController ();
				studentCtrl.alreadyRegisteredSchoolId = schoolId;
				studentCtrl.LoadAndSetup (_details);
				nav.PushViewController(studentCtrl, false);
			} else {
				_iab = new IntroSplashViewController();
				_iab.details = _details;
				nav.PushViewController (_iab, false);
			}

			NavigationController.PresentViewController (nav, true, null);

		}

		NewStudentController studentCtrl;
		IntroSplashViewController _iab;

		public override void DoRowSelected (Student item)
		{
			base.DoRowSelected (item);

			// Show the summary for the student
			StudentInfo info = new StudentInfo();
			info.LoadAndSetup(item);
			info.OriginalName = item.Name;
			NavigationController.PushViewController(info, true);
		}

		public override void RemoveItem (Student item)
		{
			using (var db = DataConnection.GetConnection())
			{
				db.Execute("delete from StudentCourse where StudentId = " + item.Id);
				db.Delete(item);
			} 

			DeviceStudentUpdate.PushChanges();
			AssessmentReload.Current.RequiresDataReload = true;
			AssessmentReloader.Instance.Reload ();
		}

		UINavigationController navMan;
		Student newStudent;

		PopupNotifierController _finishedAdding;

		private void ShowFinishedAddingStudents ()
		{
			var hud = new MTMBProgressHUD (View) {
				LabelText = "Student Added",
				DetailsLabelText = "Click below to see the student(s) " + EduAppsBaseAppConstants.EntriesPlural,
				RemoveFromSuperViewOnHide = true,
				Mode = MBProgressHUDMode.Text
			};

			View.AddSubview (hud);

			hud.Show (animated: true);
			hud.Hide (animated: true, delay: 3);
		}
	}
}

