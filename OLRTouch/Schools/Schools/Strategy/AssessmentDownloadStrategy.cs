using System;
using System.Collections.Generic;
using System.Linq;

namespace Schools
{
	public class AssessmentDownloadStrategy
	{
		public List<Assessment> Assessments = new List<Assessment>();
		public List<string> allCourses;

		public AssessmentDownloadStrategy(List<string> CourseIds)
		{
			this.allCourses = CourseIds;
		}

		protected virtual void DoPopulate()
		{

		}

		protected virtual void PreProcess()
		{

		}

		protected virtual void PostProcess()
		{
			Assessments = Assessments.OrderBy (r => r.GetLocalDueDate ()).ToList ();
			LocalCache.StoreAssessments(Assessments);
		}

		public void Populate()
		{
			PreProcess ();
			DoPopulate ();
			PostProcess ();
		}
	}

	public class DoNothingStrategy : AssessmentDownloadStrategy
	{
		public DoNothingStrategy(List<string> CourseIds) : base(CourseIds)
		{
		}
	}
}

