using System;
using System.Collections.Generic;
using System.Linq;

namespace Schools
{

	public class NonSubscribedAssessmentDownloadStrategy : AssessmentDownloadStrategy
	{
		public int SchoolsCount = 0;
		public int SchoolsSubscribedCount = 0;

		public NonSubscribedAssessmentDownloadStrategy(List<string> CourseIds) : base(CourseIds)
		{
		}

		protected override void DoPopulate()
		{
			while (allCourses.Count > 0)
			{
				List<string> currentBatch = new List<string>();
				int numberToRemove;

				if (allCourses.Count > 25)
				{
					currentBatch.AddRange(allCourses.GetRange(0, 25));
					numberToRemove = 25;
				} else
				{	
					currentBatch.AddRange(allCourses);
					numberToRemove = allCourses.Count;
				}

				string batchCourseList = String.Join (",", currentBatch.ToArray ());
				string url = String.Format (
					Host.Prefix + "/rest/Assessments.ashx/SchoolLinkAssessment/{0}", batchCourseList);

#if DEBUG
				string id = "TEST_AUTO_SYSTEM";
#else
				string id = "droid_inapp"; //subscriptionDetails.transactionId;
#endif

				var batchlist = JsonLoaders.LoadCollectionFromWebLink<AssessmentDetails> (
					url, (o) => { 
					var details = JsonLoaders.LoadAssessmentDetails(o);

						this.SchoolsCount += details.SchoolsCount;
						this.SchoolsSubscribedCount += details.SchoolsSubscribedCount;

					foreach (var a in details.assessments)
					if (a.GetLocalDueDate () >= DateTime.Now.Date)
							this.Assessments.Add(a);

					return null;
				}, id);

				allCourses.RemoveRange(0, numberToRemove);
			}
		}
	}
}
