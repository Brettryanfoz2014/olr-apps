using System;
using System.Collections.Generic;
using System.Linq;

namespace Schools
{
	public class SubscribedAssessmentDownloadStrategy : AssessmentDownloadStrategy
	{
		public SubscribedAssessmentDownloadStrategy(List<string> CourseIds) : base(CourseIds)
		{
		}

		protected override void DoPopulate()
		{
			while (allCourses.Count > 0)
			{
				List<string> currentBatch = new List<string>();
				int numberToRemove;

				if (allCourses.Count > 25)
				{
					currentBatch.AddRange(allCourses.GetRange(0, 25));
					numberToRemove = 25;
				} else
				{	
					currentBatch.AddRange(allCourses);
					numberToRemove = allCourses.Count;
				}

				string batchCourseList = String.Join (",", currentBatch.ToArray ());
				string url = String.Format (
						Host.Prefix + "/rest/Assessments.ashx/AssessmentsForCourses/{0}", batchCourseList);

#if DEBUG
				string id = "TEST_AUTO_SYSTEM";
#else
				string id = "inapp_assessments"; //subscriptionDetails.transactionId;
#endif
				var batchlist = JsonLoaders.LoadCollectionFromWebLink<Assessment> (
					url, (o) => { 
					var assessment = JsonLoaders.LoadAssessment (o);
					if (assessment.GetLocalDueDate () >= DateTime.Now.Date)
						return assessment;
					else
						return null;
				}, id);

				// Remove the items
				Assessments.AddRange(batchlist);

				allCourses.RemoveRange(0, numberToRemove);
			}
		}
	}

}
