using System;

namespace Schools
{

	public class AssessmentReload
	{
		public static AssessmentReload Current {
			get { return current; }
		}
		private static AssessmentReload current;

		static AssessmentReload ()
		{
			current = new AssessmentReload();
		}

		bool _RequiresDataReload = false;
		public bool RequiresDataReload 
		{
			get
			{
				return _RequiresDataReload;
			} 
			set
			{
				_RequiresDataReload = value;
			}
		}
	}
}
