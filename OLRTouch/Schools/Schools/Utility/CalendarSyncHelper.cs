using System;
using System.Collections.Generic;

namespace Schools
{
	public delegate void CalendarSyncStatus(string message);
	public delegate void CalendarEntryCallback(CalendarEntry entry);

	public abstract class CalendarWrapper
	{
		public int DaysBefore = -365;
		public int DaysAfter = 365;

		public virtual string GetName()
		{
			return "Calendar";
		}

		public void SyncCalendar(List<Assessment> currentAssessments, CalendarSyncStatus onStatus)
		{
			DateTime startDate = DateTime.Now.AddDays(DaysBefore);
			DateTime endDate = DateTime.Now.AddDays(DaysAfter);

			onStatus("Analysing existing " + EduAppsBaseAppConstants.EntriesPlural.ToLower());

			List<CalendarEntry> localCalendarAppointments = GetAssessmentAlertsInCalendar();

			// Get the MD5s of all the current Assessments
			Dictionary<Assessment, string> assessmentHashes = new Dictionary<Assessment, string>();
			Dictionary<CalendarEntry, string> calendarHashes = new Dictionary<CalendarEntry, string>();  

			using (var db = DataConnection.GetConnection())
			{
				foreach (Assessment a in currentAssessments)
				{
					assessmentHashes[a] = MD5Utility.HashAssessment(db, a);
				}
			}
		
			foreach (CalendarEntry e in localCalendarAppointments)
			{
				calendarHashes[e] = MD5Utility.HashCalendarEntry(e);
			}

			// Make a copy of the lists
			List<Assessment> remainingAssessments = new List<Assessment>(currentAssessments);
			List<CalendarEntry> remainingCalendarEntries = new List<CalendarEntry>(localCalendarAppointments);

			// Go through the list and see that the Assessments are in the Calendar
			foreach (var ass in currentAssessments)
			{
				var assessmentHash = assessmentHashes[ass];

				if (calendarHashes.ContainsValue(assessmentHash))
				{
					// It exists in the calendar. We don't need to process it
					remainingAssessments.Remove(ass);

					// Find the entry where it matches
					foreach (CalendarEntry e in localCalendarAppointments)
						if (calendarHashes[e] == assessmentHash)
							remainingCalendarEntries.Remove(e);
				} else {
					// We have an assessment that needs to be inserted. We can leave it in as one that needs to be removed
				}
			}

			int total = remainingAssessments.Count + remainingCalendarEntries.Count;
			int currentPosition = 1;

			StartBatch();

			// Go through the calendar entries and the remaining ones are the ones that should be deleted
			foreach (CalendarEntry ce in remainingCalendarEntries)
			{
				DeleteEntry(ce);

				onStatus(String.Format("Processing {0} of {1}", currentPosition, total));
				currentPosition++;
			}

			// Go through the appointments and insert them
			foreach (var assessment in remainingAssessments)
			{
				InsertAssessment(assessment);

				onStatus(String.Format("Processing {0} of {1}", currentPosition, total));
				currentPosition++;
			}

			EndBatch(onStatus);
		}

		public void LowMemorySync(List<Assessment> currentAssessments, CalendarSyncStatus onStatus)
		{
			DateTime startDate = DateTime.Now.AddDays(DaysBefore);
			DateTime endDate = DateTime.Now.AddDays(DaysAfter);
			
			onStatus("Analysing existing " + EduAppsBaseAppConstants.EntriesPlural.ToLower());

			// Get the MD5s of all the current Assessments
			Dictionary<string, Assessment> assessmentHashes = new Dictionary<string, Assessment>();

			int assessmentCount = currentAssessments.Count;
			int assessmentIndex = 0;

			using (var db = DataConnection.GetConnection())
			{
				foreach (Assessment a in currentAssessments)
				{
					var hash = MD5Utility.HashAssessment(db, a);
					assessmentHashes[hash] = a;

					assessmentIndex++;
					onStatus("Analysing existing " + EduAppsBaseAppConstants.EntriesPlural.ToLower() + " (" + assessmentIndex + " of " + assessmentCount + ")");
				}
			}

			// Go through the items and remove them if needed
			GetAssessmentAlertsInCalendar((e) => {
				var calHash = MD5Utility.HashCalendarEntry(e);

				// See if it is in the list
				if (assessmentHashes.ContainsKey(calHash))
				{
					// We are right to leave it, but remove it from the local collection
					var foundItem = assessmentHashes[calHash];
					assessmentHashes.Remove(calHash);
				} else {
					// Remove the item
					DeleteEntry(e);
				}
			});

			// Insert the remaining assessments into the calendar in reasonable batch sizes
			foreach (string hashKey in assessmentHashes.Keys)
			{
				var assessment = assessmentHashes[hashKey];
				InsertAssessment(assessment);
			}

			EndBatch(onStatus);
		}

		public abstract List<CalendarEntry> GetAssessmentAlertsInCalendar(CalendarEntryCallback callback = null);
		public abstract CalendarEntry GetById(string id);
		public abstract void DeleteEntry (CalendarEntry ce);
		public abstract void InsertAssessment (Assessment assessment);

		public virtual void StartBatch()
		{
			// Subclasses to override if they need to
		}

		public virtual void EndBatch(CalendarSyncStatus onStatus)
		{
			// Subclasses to override if they need to
		}
	}
	
	public class CalendarEntry
	{
		public string Name;
		public string Description;
		public string Location;

		public DateTime startDate;
		public DateTime endDate;
		public bool AllDay;
	}


}

