﻿using System;
using Foundation;

namespace Schools
{
	public class CloudSubscriptionStorage
	{
		public static string subscriptionExpiresDateKey = "subscriptionExpiresDate";

		public static void WriteSubscriptionValidUntil(DateTime dt)
		{
			string fullDateString = dt.ToString ("yyyy-M-d");

			var store = NSUbiquitousKeyValueStore.DefaultStore;

			store.SetString (subscriptionExpiresDateKey, fullDateString); 
			store.Synchronize();
		}

		public static void DoSync()
		{
			NSUbiquitousKeyValueStore.DefaultStore.Synchronize ();
		}

		public static bool IsSubscribed()
		{
			bool foundValue = false;
			var store = NSUbiquitousKeyValueStore.DefaultStore;
			try
			{
				var date = store.GetString(subscriptionExpiresDateKey);
				if (String.IsNullOrWhiteSpace(date))
				{
					foundValue = false;
				} else 
				{
					foundValue = true;

					var parts = date.Split('-');
					var year = Convert.ToInt32(parts[0]);
					var month = Convert.ToInt32(parts[1]);
					var day = Convert.ToInt32(parts[2]);

					var expiryDate = new DateTime(year, month, day);
					return expiryDate.AddDays(1).AddSeconds(-1) > DateTime.Now;
				}
			} catch {
				foundValue = false;
			}

			return false;
		}

		public static DateTime GetExpiryDate ()
		{
			var store = NSUbiquitousKeyValueStore.DefaultStore;
			try
			{
				var date = store.GetString(subscriptionExpiresDateKey);
				if (!String.IsNullOrWhiteSpace(date))
				{
					var parts = date.Split('-');
					var year = Convert.ToInt32(parts[0]);
					var month = Convert.ToInt32(parts[1]);
					var day = Convert.ToInt32(parts[2]);

					var expiryDate = new DateTime(year, month, day);
					return expiryDate.AddDays(1).AddSeconds(-1);
				}
			} catch {
				// We'll fall through
			}

			return DateTime.MinValue;
		}
	}
}

