using System;
using System.Collections.Generic;

namespace Schools
{
	public class GrammarString
	{
		public static string Join(string[] list, string andText = "and")
		{
			if (list.Length == 0)
				return "";
			else if (list.Length == 1)
				return list[0];
			else if (list.Length == 2)
				return list[0] + " " + andText + " " + list[1];
			else {
				List<string> all = new List<string>(list);
				string last = all[all.Count - 1];
				all.RemoveAt(all.Count - 1);
				return String.Join(", ", all.ToArray()) + " " + andText + " " + last;
			}
		}
	}
}

