using System;
using System.Net;
using System.Text;
using System.Threading;

namespace Schools
{
	public partial class SendDeviceUpdate
	{
		DeviceStudentUpdate _update;

		public void SendDetails(DeviceStudentUpdate update)
		{
			_update = update;
			ThreadStart job = new ThreadStart(InternalSending);
			Thread thread = new Thread(job);
			thread.Start();
		}

		public void InternalSending ()
		{
			if (_update.deviceID != null) {
				if (_update.deviceID.Length > 0) {
					var content = _update.GetJson ();
					PostDataToURL (Host.CombineSecure ("/rest/Devices.ashx/ConfigureDevice"), "POST", content);
				}
			}
		}

		public string PostDataToURL(string url, string method, string details)
		{
			try
			{
			using (var client = new WebClient())
			{
				BeforeSend();

				var dataToPost = Encoding.Default.GetBytes(details);
				var result = client.UploadData(url, method, dataToPost);

				AfterSend(true);

				return result.ToString();
				}
			} catch {
				AfterSend(false);

				return "";
			}
		}
	}
}

