using System;
using System.Collections.Generic;
using EventKit;

namespace Schools
{
	public class iOSCalendarEntry : CalendarEntry
	{
		public EKEvent iOSEvent;

		public iOSCalendarEntry(EKEvent calendarEvent)
		{
			iOSEvent = calendarEvent;

			this.AllDay = iOSEvent.AllDay;
			this.Name = iOSEvent.Title;
			this.Description = iOSEvent.Description;
			this.startDate = MobileUtilities.Forms.DateUtil.NSDateToDateTime(iOSEvent.StartDate);
			this.endDate = MobileUtilities.Forms.DateUtil.NSDateToDateTime(iOSEvent.EndDate);
			this.Location = iOSEvent.Location;
		}
	}
}
