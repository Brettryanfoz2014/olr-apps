using System;
using System.Threading;
using MobileUtilities.Forms;
using UIKit;
using Foundation;
using System.Collections.Generic;
using EventKit;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class AllCalendarsListController : ViewEditFormController<Entry>
	{
		public AllCalendarsListController () : base()
		{
			Title = "Calendars";
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			Section s = new Section("Calendars");

			foreach (var c in App.Current.Wrapper.GetCalendarNames())
			{
				var navCalendar = s.AddCheckNavigate(c);
				navCalendar.HideNavigation = true;
				navCalendar.NavigateImage = ImageConstants.Calendar;
				if (c == _item.Value)
					navCalendar.SetChecked();
				else
					navCalendar.ClearChecked();

				navCalendar.OnClick += (object sender, EventArgs e) => 
				{
					Entry selectedCalendar = sender as Entry;
					_item.Value = selectedCalendar.Label;

					if (OnReloadSettingsView != null)
						OnReloadSettingsView(this, EventArgs.Empty);

					NavigationController.PopViewController(true);
				};
			}

			sections.Add (s);
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "AllCalendarsListController");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		public event EventHandler OnReloadSettingsView = null;
	}
}

