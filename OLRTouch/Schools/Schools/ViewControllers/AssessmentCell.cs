using System;
using UIKit;
using CoreGraphics;
using CoreGraphics;
using Foundation;

namespace Schools
{
	public class AssessmentCell : UITableViewCell
	{
		Assessment _assessment;

		public AssessmentCell () : base(UITableViewCellStyle.Subtitle, ID)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
		}

		public void SetAssessment(Assessment assessment)
		{
			_assessment = assessment;
		}

		public static NSString ID = new NSString("AssessmentCellID");

		public override void Draw (CGRect rect)
		{
			//base.Draw (rect);

			this.BackgroundColor = UIColor.Clear;

			// Need to draw the Rectangle on the left hand side first
			//// General Declarations
			var colorSpace = CGColorSpace.CreateDeviceRGB();
			var context = UIGraphics.GetCurrentContext();
			
			//// Color Declarations
			UIColor dateRed = UIColor.FromRGBA(0.800f, 0.109f, 0.000f, 1.000f);
			UIColor dateBlue = UIColor.FromRGBA(0.0f, 0.0f, 1.0f, 1.0f);

			//// Gradient Declarations
			#if Gradient
			var greyGradientColors = new CGColor [] {UIColor.White.CGColor, UIColor.FromRGBA(0.572f, 0.572f, 0.572f, 1.000f).CGColor, UIColor.Black.CGColor};
			var greyGradientLocations = new float [] {0.65f, 0.75f, 0.75f};
			var greyGradient = new CGGradient(colorSpace, greyGradientColors, greyGradientLocations);
			#endif
			//UIColor dropShadowColor = UIColor.FromRGBA(0.476f, 0.476f, 0.476f, 0.380f);

			//// Shadow Declarations
//			var dropShadow = dropShadowColor.CGColor;
//			var dropShadowOffset = new SizeF(2.1f, 2.1f);
//			var dropShadowBlurRadius = 1;
//
			var textContent = _assessment.GetLocalDueDate().Day.ToString();
			var text2Content = _assessment.GetLocalDueDate().ToString("MMM").ToUpper();
			
			//// Rounded Rectangle Drawing
			var roundedRectanglePath = UIBezierPath.FromRoundedRect(new CGRect(1.5f, 1.5f, 38, 38), 4);
			context.SaveState();
//			context.SetShadowWithColor(dropShadowOffset, dropShadowBlurRadius, dropShadow);
			context.BeginTransparencyLayer(null);
			roundedRectanglePath.AddClip();
			//context.DrawLinearGradient(greyGradient, new PointF(20.5f, 1.5f), new PointF(20.5f, 39.5f), 0);
			context.EndTransparencyLayer();
			context.RestoreState();
			
			UIColor.DarkGray.SetStroke();
			roundedRectanglePath.LineWidth = 1;
			roundedRectanglePath.Stroke();
			
			//// Rounded Rectangle 2 Drawing
			var roundedRectangle2Path = UIBezierPath.FromRoundedRect(new CGRect(2, 28, 37, 11), UIRectCorner.BottomLeft | UIRectCorner.BottomRight, new CGSize(4, 4));

			if (_assessment.GetLocalDueDate() <= DateTime.Now.Date.AddDays(30))
				dateRed.SetFill();
			else
				dateBlue.SetFill();

			roundedRectangle2Path.Fill();
			
			//// Text Drawing
			var textRect = new CGRect(2, 0, 37, 28);
			UIColor.Black.SetFill();
			new NSString(textContent).DrawString(textRect, UIFont.FromName("Helvetica-Bold", 24), UILineBreakMode.WordWrap, UITextAlignment.Center);
			
			//// Text 2 Drawing
			var text2Rect = new CGRect(2, 27, 37, 15);
			UIColor.White.SetFill();
			new NSString(text2Content).DrawString(text2Rect, UIFont.FromName("HelveticaNeue-Bold", 9), UILineBreakMode.WordWrap, UITextAlignment.Center);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			float buffer = 0f;
			if (MobileUtilities.Multimedia.DeviceHardware.IsOnIPad ())
				buffer = 1024 - 480;

			TextLabel.Frame = new CGRect (50f, TextLabel.Frame.Top, 210f + buffer, TextLabel.Frame.Height);
			DetailTextLabel.Frame = new CGRect (50f, DetailTextLabel.Frame.Top, 210f + buffer, DetailTextLabel.Frame.Height);
		}
	}
}

