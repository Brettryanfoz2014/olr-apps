using System;
using UIKit;
using CoreGraphics;
using MobileUtilities.Forms;

namespace Schools
{
	public class AssessmentsCalendarViewController : CalendarMonthViewController
	{
		AssessmentsList _assessments = new AssessmentsList();

		public AssessmentsCalendarViewController () : base()
		{
			SetupCalendar();
		}

		public void SetupCalendar()
		{
			TabBarItem.Image = UIImage.FromFile (ImageConstants.Checkmark);
			this.Title = EduAppsBaseAppConstants.EntriesPlural;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.BackgroundColor = UIColor.White;

			//_assessments = new AssessmentsList();
			//_assessments.ShowSearchOption = false;
			//this.View.Add(_assessments.View);
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			//_assessments.View.Frame = new RectangleF(0, 400, 320, View.Frame.Height - 400);
		}
	}
}

