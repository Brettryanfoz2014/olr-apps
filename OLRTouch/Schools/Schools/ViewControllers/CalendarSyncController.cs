using System;
using System.Threading;
using MobileUtilities.Forms;
using UIKit;
using Foundation;
using System.Collections.Generic;
using EventKit;
using ObjCRuntime;
using MBProgressHUD;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class CalendarSyncController : ViewEditFormController<object>
	{
		public CalendarSyncController () : base()
		{
			Title = EduAppsBaseAppConstants.MenuCalendar;
			TabBarItem.Image = UIImage.FromFile(ImageConstants.IconCalendar);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			App.Current.RequestAccess(this);
		}

		MTMBProgressHUD ShowSyncingMessage;
		
		void ShowSyncing ()
		{
			ShowSyncingMessage = new MTMBProgressHUD (View);
			ShowSyncingMessage.LabelText = "";
			ShowSyncingMessage.DetailsLabelText = "";

			View.AddSubview (ShowSyncingMessage);

			ShowSyncingMessage.Show (animated: true);
		}
		
		void HideSyncing()
		{
			// Lets delay a couple of seconds

			BeginInvokeOnMainThread(delegate {
				if (ShowSyncingMessage != null)
					ShowSyncingMessage.Hide (animated: true, delay: 1);

				// Re-enable the button
				_btnSyncCalendar.EnableButton();
			});
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "CalendarSyncController");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());

			SetupForm ();
			CheckButtonState ();
			TableView.ReloadData();
		}

		Entry _calendar;
		Entry _btnSyncCalendar;

		void CheckButtonState ()
		{
			GeneralSettings settings = null;
			using (var db = DataConnection.GetConnection())
			{
				settings = GeneralSettings.GetSettings(db);
			} 

			if (_btnSyncCalendar == null)
				return;

//			if (EduAppsBaseAppConstants.RequireSubscription == false)
//			{
				_btnSyncCalendar.SetButtonTitle("Synchronise Now");
				_btnSyncCalendar.EnableButton();
//			} else {
//				if (!settings.IsSubscriptionValid())
//				{
//					_btnSyncCalendar.SetButtonTitle("Subscription Required");
//					_btnSyncCalendar.DisableButton();
//				} else {
//					_btnSyncCalendar.SetButtonTitle("Synchronise Now");
//					_btnSyncCalendar.EnableButton();
//				}
//			}
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			sections.Clear();

			GeneralSettings settings = null;
			using (var db = DataConnection.GetConnection())
			{
				settings = GeneralSettings.GetSettings(db);
			} 

			var details = new Section("Calendar Sync");
			details.AddInfoText("", "This allows you to add the items to your devices calendar. All previous " 
			                    + EduAppsBaseAppConstants.ApplicationName + " entries that are no longer valid will be removed from the selected calendar.");

			_calendar = details.AddNavigate("Calendar");
			_calendar.OnClick += (object sender, EventArgs e) => {
				AllCalendarsListController allCal = new AllCalendarsListController();
				allCal.LoadAndSetup(sender as Entry);
				allCal.OnReloadSettingsView += (object sender2, EventArgs e2) => {
					this.TableView.ReloadData();
				};
				NavigationController.PushViewController(allCal, true);
			};
			_calendar.AsString = settings.CalendarName;
			_calendar.NavigateImage = ImageConstants.Calendar;

			_btnSyncCalendar = details.AddButton("Synchronise Now");
			_btnSyncCalendar.OnClick += (sender, e) => {
				// Do Something here
				_btnSyncCalendar.DisableButton();

				DoSync();
			};

			sections.Add(details);

			Section push = new Section("Push Notifications");

			Entry _notifyWhen = push.AddNavigate("Notify of " + EduAppsBaseAppConstants.EntriesPlural);
			_notifyWhen.Value = settings.GetPushNotificationSummary();
			_notifyWhen.OnClick += (sender, e) => {
				GeneralSettings settings2 = null;
				using (var db2 = DataConnection.GetConnection())
				{
					settings2 = GeneralSettings.GetSettings(db2);
				} 

				_beforeNotificationsShow = settings2.GetPushNotificationSummary();

				// Load the notification when screen
				PushNotificationWhenDisplay pnWhen = new PushNotificationWhenDisplay();
				pnWhen.LoadAndSetup(settings2);
				pnWhen.AfterSave += (object sender2, EventArgs e2) => {
					GeneralSettings gs = sender2 as GeneralSettings;
					_notifyWhen.Value = gs.GetPushNotificationSummary();

					string newNotifications = gs.GetPushNotificationSummary();

					if (!_beforeNotificationsShow.Equals(newNotifications))
						DeviceStudentUpdate.PushChanges();

					this.TableView.ReloadData();
				};
				NavigationController.PushViewController(pnWhen, true);
			};

			sections.Add(push);

		}

		string _beforeNotificationsShow = "";

		SendDeviceUpdate _pushNotificationsUpdate = new SendDeviceUpdate();

		public override void SaveData ()
		{
			base.SaveData ();

			using (var db = DataConnection.GetConnection())
			{
				var settings = GeneralSettings.GetSettings(db);
				settings.CalendarName = _calendar.AsString;
				db.Update(settings);
			} 
		}

		void DoSync ()
		{
			// Check that we have permissions to Sync
			if (!App.Current.HasBeenGrantedAccess)
			{
				new UIAlertView ( "Calendar Access Denied", 
				                 "The Application has been denied access to the Calendar. Calendar Syncronisation will not be possible until it is enabled in Privacy Settings for " + EduAppsBaseAppConstants.ApplicationName, 
				                 null, "OK", null).Show ();

				return;
			}

			ShowSyncing();

			_calName = _calendar.AsString;


			var thread = new Thread(DoCalendarSync);
			thread.Start();
		}

		string _calName = "";

		
		[Export("DoAfterCompleteFadeOut")]
		void DoAfterCompleteFadeOut()
		{
			BeginInvokeOnMainThread(delegate {
				if (ShowSyncingMessage != null)
					ShowSyncingMessage.Hide (true, 1);
			});
		}

		[Export("DoCalendarSync")]
		void DoCalendarSync()
		{
			using(var pool = new NSAutoreleasePool())
			{
				var c = App.Current.Wrapper.GetCalendarByName(_calName);
				if (c == null)
				{
					c = App.Current.Wrapper.GetDefaultCalendar();

					if (c == null)
					{
						BeginInvokeOnMainThread(delegate {
							DialogHelpers.ShowMessage("No calendar found", "An appropriate calendar could not be found. Please create and " + 
							                          "select a calendar before synchronizing " + EduAppsBaseAppConstants.EntriesPlural.ToLower()
							                          );
						});

						return;
					}
				}

				BeginInvokeOnMainThread(delegate {
					ShowSyncingMessage.LabelText = "Synching " + EduAppsBaseAppConstants.EntriesPlural + " to your calendar";
				});

				// Run code in new thread
				List<string> allCourses = new List<string>();

				using (var conn = DataConnection.GetConnection ())
				{
					var students = conn.Query<Student>("select * from Student");
					
					// Get all the courses for a Query
					var courses = conn.Query<StudentCourse>("select * from StudentCourse");

					foreach (StudentCourse sc in courses)
					{
						allCourses.Add(sc.CourseId.ToString());
					}
				} 

				string allCoursesList = String.Join(",", allCourses.ToArray());

				List<Assessment> list = new List<Assessment>();

				// If we have no courses then we have no assessments
				if (!String.IsNullOrEmpty(allCoursesList))
				{
					bool retrievedFromServer = false;

					try
					{
						if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name))
						{
							BeginInvokeOnMainThread(delegate {
								ShowSyncingMessage.DetailsLabelText = "Downloading Assessments";
							});

							GeneralSettings settings;

							using (var db = DataConnection.GetConnection())
							{
								settings = GeneralSettings.GetSettings(db);	
							} 

							AssessmentDownloadStrategy strategy;
							if (settings.IsSubscriptionValid () || EduAppsBaseAppConstants.RequireSubscription == false) {
								strategy = new SubscribedAssessmentDownloadStrategy(allCourses);
							} else {
								strategy = new NonSubscribedAssessmentDownloadStrategy(allCourses);
							}
							strategy.Populate();

							list = strategy.Assessments;

//							if (strategy is NonSubscribedAssessmentDownloadStrategy)
//							{
//								NonSubscribedAssessmentDownloadStrategy info = strategy as NonSubscribedAssessmentDownloadStrategy;
//								if (info.SchoolsSubscribedCount == 0)
//								{
//									// We have no schools that are subscribed. Show the Purchase screen
//								} else if (info.SchoolsSubscribedCount < info.SchoolsCount)
//								{
//									// We have some schools that are not available. Pop up a message that
//									// there are schools there. 
//									InvokeOnMainThread(delegate {
//										var hud = new MTMBProgressHUD (View) {
//											LabelText = "Not all schools loaded",
//											DetailsLabelText = "Not all of the schools are subscribed to " + 
//											EduAppsBaseAppConstants.ApplicationName + ". Please purchase to get access to the other courses",
//											Mode = MBProgressHUDMode.Text,
//											RemoveFromSuperViewOnHide = true
//										};
//
//										View.AddSubview (hud);
//
//										hud.Show (animated: true);
//										hud.Hide (animated: true, delay: 5);
//									});
//								}
//							}

							retrievedFromServer = true;
						}
					} catch {
						retrievedFromServer = false;
					}

					if (!retrievedFromServer)
					{
						// Load the Assessments from the local information
						using (var conn = DataConnection.GetConnection())
						{
							list = conn.Query<Assessment>("select * from Assessment where CourseId in (" + allCoursesList + ")");
						}
					}
				}

				// Get all the entries in the calendar that match the format
				c.SyncCalendar(list, (status) => {
					BeginInvokeOnMainThread(delegate {
						ShowSyncingMessage.DetailsLabelText = status;
					});
				});

				BeginInvokeOnMainThread(delegate {
					_btnSyncCalendar.EnableButton();
					ShowSyncingMessage.DetailsLabelText = "Complete";

					var userInfo = new NSString("MyUserInfo");
					var timer = NSTimer.CreateScheduledTimer(2, this, new Selector("DoAfterCompleteFadeOut"), userInfo, false);
				});
			}
		}
	}
}

