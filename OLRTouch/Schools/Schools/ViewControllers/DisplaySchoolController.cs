using System;
using MobileUtilities.Forms;
using UIKit;
using MessageUI;
using CoreGraphics;
using CoreAnimation;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class DisplaySchoolNiceController : UIViewController
	{
		School _item;

		public DisplaySchoolNiceController () : base ()
		{
			Title = EduAppsBaseAppConstants.MenuContactUs;
			_item = School.getMyCurrentSchool ();
			TabBarItem.Image = UIImage.FromFile (ImageConstants.IconContactUs);
		}

		UILabel schoolName;
		UILabel address;
		UILabel phone;
		UILabel fax;
		UILabel webSite;
		UILabel canteen;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.BackgroundColor = UIColor.White;

			var actions = new SchoolActions (_item, this);

			UIImageView schoolImage = new UIImageView (new CGRect (25, 34, 53, 53));
			schoolImage.Image = UIImage.FromFile ("finalicons/298.png");
			Add (schoolImage);

			schoolName = new UILabel (new CGRect (98, 32, 195, 56));

			Add (schoolName);

			address = new UILabel (new CGRect (25, 107, 268, 35));

			address.Lines = 0;
			address.LineBreakMode = UILineBreakMode.WordWrap;
			Add (address);

			phone = new UILabel (new CGRect (27, 151, 268, 25));

			Add (phone);

			fax = new UILabel (new CGRect (25, 176, 268, 25));

			Add (fax);

			webSite = new UILabel (new CGRect (25, 211, 268, 35));

			Add (webSite);

			canteen = new UILabel (new CGRect (25, 246, 268, 35));

			Add (canteen);

			// Add the buttons
			UIButton btnCall = App.GetNormalButton ("Call", 280);
			Add (btnCall);
			btnCall.TouchUpInside += (sender, e) => {
				actions.MakeCall ();
			};

			UIButton btnEmail = App.GetNormalButton ("Email", 325);
			Add (btnEmail);
			btnEmail.TouchUpInside += (sender, e) => {
				actions.Email (NavigationController);
			};

			UIButton btnWebSite = App.GetNormalButton ("Web Site", 370);
			Add (btnWebSite);
			btnWebSite.TouchUpInside += (sender, e) => {
				actions.VisitWebSite (NavigationController);
			};

			schoolName.Font = UIFont.FromName (Theme.TextFontName, Theme.TextFontSize);
			foreach (UILabel l in new UILabel[] { address, phone, fax, webSite, canteen })
				l.Font = UIFont.FromName (Theme.SubTextFontName, Theme.SubTextFontSize);
			foreach (UIButton b in new UIButton[] { btnCall, btnEmail, btnWebSite}) {
				b.Font = UIFont.FromName (Theme.TextFontName, Theme.TextFontSize);
			}
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			schoolName.Text = (_item == null) ? "No School" : _item.SchoolName;
			address.Text = (_item == null) ? "" : _item.Address;
			phone.Text = "Phone: " + ((_item == null) ? "" : _item.PhoneNumber);
			fax.Text = "Fax: " + ((_item == null) ? "" : _item.FaxNumber);
			webSite.Text = "Web: " + ((_item == null) ? "" : _item.WebSite);
			canteen.Text = "Canteen: " + ((_item == null) ? "" : _item.CanteenUrl);

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "DisplaySchoolController");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}
	}

	public class SchoolActions
	{
		School _item;
		UIViewController _controller;

		public SchoolActions (School item, UIViewController controller)
		{
			_item = item;
			_controller = controller;
		}

		public void MakeCall ()
		{
			if (_item == null) {
				DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoSchool);
				return;
			}
			if (String.IsNullOrWhiteSpace (_item.PhoneNumber)) {
				DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoInfo);
				return;
			}
			string phoneLink = "tel:";
			foreach (char c in _item.PhoneNumber)
				if (Char.IsNumber (c))
					phoneLink += c;
			UIApplication.SharedApplication.OpenUrl (new Foundation.NSUrl (phoneLink));
		}

		MFMailComposeViewController _mailController;

		public void Email (UIViewController parent)
		{
			if (_item == null) {
				DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoSchool);
				return;
			}
			if (String.IsNullOrWhiteSpace (_item.Email)) {
				DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoInfo);
				return;
			}
			// Open up the email dialog
			_mailController = new MFMailComposeViewController ();
			_mailController.SetToRecipients (new string[]{ _item.Email });
			_mailController.SetSubject ("");
			_mailController.SetMessageBody ("", false);
			_mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
				args.Controller.DismissViewController (true, null);
			};

			UITextAttributes attButtonBar = new UITextAttributes();
			attButtonBar.Font = UIFont.FromName("STHeitiTC-Light", 14f);
//			_mailController.NavigationItem.RightBarButtonItem.SetTitleTextAttributes (attButtonBar, UIControlState.Normal);
//			_mailController.NavigationItem.LeftBarButtonItem.SetTitleTextAttributes (attButtonBar, UIControlState.Normal);

//			parent.PresentViewController (_mailController, true, null);
			_controller.NavigationController.PresentViewController (_mailController, true, null);
		}

		WebNavigateSection _webSite;

		void ShowWebLink (UINavigationController nav, string link, string title)
		{
			_webSite = new WebNavigateSection (link, title);
			nav.PushViewController (_webSite, true);
		}

		public void VisitWebSite (UINavigationController nav)
		{
			if (_item == null) {
				DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoSchool);
				return;
			}
			if (String.IsNullOrWhiteSpace (_item.WebSite)) {
				DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoInfo);
			}
			ShowWebLink (nav, _item.WebSite, _item.SchoolName);
		}

		public void VisitCanteenSite (UINavigationController nav)
		{
			if (_item == null) {
					DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoSchool);
				return;
			}
			if (String.IsNullOrWhiteSpace (_item.CanteenUrl)) {
				DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoInfo);
				return;
			}
			ShowWebLink (nav, _item.CanteenUrl, "Canteen");
		}
	}

	public class DisplaySchoolController : ViewEditFormController<School>
	{
		public DisplaySchoolController () : base ()
		{
			Title = "Contact Us";
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			Title = _item.SchoolName;

			// Add the aspects of the school
			Section _main = new Section (_item.SchoolName);

			_main.AddInfoText ("", _item.Address);
			//var addr = _main.AddNavigate(_item)
			//            AddMap (_item.Address, false);
			//addr.OnRetrieveAddress += (object sender, AddressDetailsArgs args) => {
			//	args.Location = _item.Latitude + "," + _item.Longtitude;
			//	args.addrType = AddressType.GPS;
			//};

			if (!String.IsNullOrEmpty (_item.WebSite)) {
				if (_item.WebSite.StartsWith ("www"))
					_item.WebSite = "http://" + _item.WebSite;

				var webLink = _main.AddWebLink ("", _item.WebSite);
			}

			if (!String.IsNullOrEmpty (_item.PhoneNumber)) {
				_main.AddInfoText ("Phone: " + _item.PhoneNumber, "").OnClick += (sender, e) => {
					string phoneLink = "tel:";
					foreach (char c in _item.PhoneNumber)
						if (Char.IsNumber (c))
							phoneLink += c;
					UIApplication.SharedApplication.OpenUrl (new Foundation.NSUrl (phoneLink));
				};
			}

			if (!String.IsNullOrEmpty (_item.Email)) {
				_main.AddInfoText ("Email: " + _item.Email, "").OnClick += (sender, e) => {

					// Open up the email dialog
					_mailController = new MFMailComposeViewController ();
					_mailController.SetToRecipients (new string[]{ _item.Email });
					_mailController.SetSubject ("");
					_mailController.SetMessageBody ("", false);
					_mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
						Console.WriteLine (args.Result.ToString ());
						args.Controller.DismissViewController (true, null);
					};
					this.PresentViewController (_mailController, true, null);
				};
			}

			if (!String.IsNullOrEmpty (_item.FaxNumber))
				_main.AddInfoText ("Fax: " + _item.FaxNumber, "");
			if (!String.IsNullOrEmpty (_item.CanteenUrl))
				_main.AddWebLink ("Canteen Link", _item.CanteenUrl);

			sections.Add (_main);
		}

		MFMailComposeViewController _mailController;
	}
}

