using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using System.Collections.Generic;
using MBProgressHUD;
using RestSharp;
using MessageUI;
using System.Threading;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class MainAction 
	{
		public ActionMove moveTo;
		public String name;
		public String icon;
		public MainAction(ActionMove moveTo, String name, String icon) {
			this.moveTo = moveTo;
			this.name = name;
			this.icon = icon;
		}
	}

	public enum ActionMove : int
	{
		To_Students = 1,
		To_Events,
		To_Notices,
		To_Newsletters,
		To_Forms,
		To_ContactUs,
		To_Canteen,
		To_Calendar,
		To_ReportAbsence,
		To_SchoolPolicy,
		To_ContactDeveloper
	}

	public class HomeController : UITableViewController
	{
		public static Boolean isFirstRun = true;
		public List<MainAction> actions = new List<MainAction> ();
		public School _mySchool;
		public UIImageView	ivCrest;
		public UILabel lblSchoolName;

		public HomeController () : base (UITableViewStyle.Plain)
		{
			Title = "Home";

			actions.Add (new MainAction (ActionMove.To_Students, EduAppsBaseAppConstants.MenuStudents, ImageConstants.IconStudents));
			actions.Add (new MainAction (ActionMove.To_Events, EduAppsBaseAppConstants.MenuEvents, ImageConstants.IconEvents));
			actions.Add (new MainAction (ActionMove.To_Notices, EduAppsBaseAppConstants.MenuMessages, ImageConstants.IconNotices));
			actions.Add (new MainAction (ActionMove.To_Newsletters, EduAppsBaseAppConstants.MenuNewsletters, ImageConstants.IconNewsLetters));
			actions.Add (new MainAction (ActionMove.To_Forms, EduAppsBaseAppConstants.MenuNotes, ImageConstants.IconForms));
			actions.Add (new MainAction (ActionMove.To_ContactUs, EduAppsBaseAppConstants.MenuContactUs, ImageConstants.IconContactUs));
			actions.Add (new MainAction (ActionMove.To_Canteen, EduAppsBaseAppConstants.MenuCanteen, ImageConstants.IconCanteen));
			actions.Add (new MainAction (ActionMove.To_Calendar, EduAppsBaseAppConstants.MenuCalendar, ImageConstants.IconCalendar));
			actions.Add (new MainAction (ActionMove.To_ReportAbsence, EduAppsBaseAppConstants.MenuReportAbsence, ImageConstants.IconReportAbsence));
			actions.Add (new MainAction (ActionMove.To_SchoolPolicy, EduAppsBaseAppConstants.MenuSchoolPolicies, ImageConstants.IconSchoolPolicies));
			actions.Add (new MainAction (ActionMove.To_ContactDeveloper, EduAppsBaseAppConstants.MenuContactDeveloper, ImageConstants.IconContactDeveloper));

			UIImage menu = UIImage.FromFile (ImageConstants.IconMenu);
			UIButton menuButton = new UIButton(new RectangleF(0,0,30,30));
			menuButton.SetBackgroundImage (menu, UIControlState.Normal);
			menuButton.TouchUpInside += DoSelectSchool; 
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(menuButton);

		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			if (isFirstRun) {
				isFirstRun = false;
				if (DataConnection.hasStudents())
				{
					navigateViewController (new AssessmentsList ());
				} else {
					navigateViewController (new StudentSearcher ());
				}
			}

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "HomeViewController");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());

		}

		public override void ViewWillAppear(bool animated) {
			base.ViewWillAppear(animated);
			_mySchool = School.getMyCurrentSchool ();

			if (_mySchool != null ) {
				var client = new RestClient (Host.GetSchoolDetailsPage(_mySchool.Id));
				var request = new RestRequest (Method.GET);
				client.ExecuteAsync (request, response => {
					try
					{
						var o = System.Json.JsonObject.Parse(response.Content);

						// Get the School Details
						var actualSchool = JsonLoaders.LoadSchool(o as System.Json.JsonObject);
						using (var db = DataConnection.GetConnection())
						{
							LocalCache.StoreSchoolToCache(db, actualSchool);
						}
						Console.WriteLine ("Crest Url = " + actualSchool.CrestUrl);

						using (var imgUrl = new NSUrl (actualSchool.CrestUrl)) {
							using (var data = NSData.FromUrl (imgUrl)) {
								if (data == null) {
									return;
								}
								InvokeOnMainThread (delegate {
									UIImage crest  = UIImage.LoadFromData(data);
									setCrestImage(crest, actualSchool.SchoolName);
								});
							}
						}

					} catch {
						TableView.TableHeaderView = null;
						ivCrest = null;
						lblSchoolName = null;
					}
				});
				if (lblSchoolName != null) {
					lblSchoolName.Text = _mySchool.SchoolName;
					setAlignSchoolName ();
				}
			} else {
				TableView.TableHeaderView = null;
				ivCrest = null;
				lblSchoolName = null;
			}
		}

		public void setCrestImage (UIImage crest, string schoolName) {
			if (ivCrest != null) {
				ivCrest.Image = crest;
				return;
			}
			UIView headerView = new UIView (new RectangleF (0, 0, 320, 80));

			ivCrest = new UIImageView (new RectangleF (20, 10, 60, 60));
			ivCrest.Image = crest;


			lblSchoolName = new UILabel (new RectangleF(90, 20, 210, 60));
			lblSchoolName.Text = schoolName;
			lblSchoolName.LineBreakMode = UILineBreakMode.WordWrap;
			lblSchoolName.Lines = 3;
			lblSchoolName.Font = UIFont.FromName ("Constantia", 20f);


			headerView.Add (ivCrest);
			headerView.Add (lblSchoolName);

			UIView divier = new UIView (new RectangleF (0, 78, 320, 1));
			divier.BackgroundColor = UIColor.FromRGB (200, 200, 200);
			headerView.Add (divier);

			TableView.TableHeaderView = headerView;

			setAlignSchoolName ();
		}

		public void setAlignSchoolName() {
			lblSchoolName.Frame = new RectangleF(90, 20, 210, 60);
			lblSchoolName.SizeToFit ();
			nfloat fWidth = lblSchoolName.Frame.Width;
			nfloat fHeight = lblSchoolName.Frame.Height;

			nfloat fLeft = (250f - fWidth) / 2.0f;

			RectangleF frameCrest = ivCrest.Frame;
			frameCrest.X = fLeft;
			ivCrest.Frame = frameCrest;

			RectangleF frameSchool = lblSchoolName.Frame;
			frameSchool.X = fLeft + 70f;
			frameSchool.Y = (80f - frameSchool.Height) / 2f;
			lblSchoolName.Frame = frameSchool;

			Console.WriteLine ("Width = " + fWidth + " , " + "Height = " + fHeight);
		}


		public void LoadSchoolCrest(){
			using (var imgUrl = new NSUrl (_mySchool.CrestUrl)) {
				using (var data = NSData.FromUrl (imgUrl)) {
					if (data == null) {
						return;
					}
					InvokeOnMainThread (delegate {
						UIImage crest = UIImage.LoadFromData(data);
						UIImageView crestView = new UIImageView(new RectangleF(new PointF(0,0), new SizeF(30, 30)));
						crestView.Image = crest;
						UIBarButtonItem crestImage = new UIBarButtonItem (crestView);
						NavigationItem.LeftBarButtonItem = crestImage;
					});
				}
			}
		}
		void DoSelectSchool(object sender, EventArgs e)
		{
			// Show the purchase subscription screen
			NavigationController.PushViewController (new EnrolledSchoolSearcher(), true);
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			TableView.Source = new HomeControllerSource (this);
		}

		public void navigateViewController(UIViewController controller) {
			Title = "Home";
			NavigationController.PushViewController (controller, true);
		}
	}

	public class HomeControllerSource : UITableViewSource
	{
		HomeController homeController;
		

		public HomeControllerSource (HomeController controller)
		{
			homeController = controller;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return homeController.actions.Count;
		}

		public override string TitleForHeader (UITableView tableView, nint section)
		{
			return "";
		}

		public override string TitleForFooter (UITableView tableView, nint section)
		{
			return "";
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = new UITableViewCell (UITableViewCellStyle.Subtitle, "HomeCell");

			cell.TextLabel.Text = homeController.actions[indexPath.Row].name;
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.ImageView.Image = UIImage.FromFile(homeController.actions [indexPath.Row].icon);

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			tableView.DeselectRow (indexPath, true);

			MainAction action = homeController.actions [indexPath.Row];
			School _school = School.getMyCurrentSchool ();

			UIViewController controller = null; 
			switch (action.moveTo) {
			case ActionMove.To_Students:
				controller = new StudentSearcher ();
				break;
			case ActionMove.To_Events:
				controller = new AssessmentsList ();
				break;
			case ActionMove.To_Notices:
				controller = new NoticesSearcher ();
				break;
			case ActionMove.To_Newsletters:
				controller = new DocumentsTypeSearcher (EduAppsBaseAppConstants.DocumentTypeNewsletters, 
					EduAppsBaseAppConstants.MenuNewsletters,
					ImageConstants.IconNewsLetters, "Search for a newsletter");
				break;
			case ActionMove.To_Forms:
				controller = new DocumentsTypeSearcher (EduAppsBaseAppConstants.DocumentTypeForms, 
					EduAppsBaseAppConstants.MenuNotes, 
					ImageConstants.IconForms, "Search for a note");
				break;
			case ActionMove.To_ContactUs:
				controller = new DisplaySchoolNiceController ();
				break;
			case ActionMove.To_Canteen:
				if (_school == null) {
					DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoSchool);
					return;
				}
				if (String.IsNullOrWhiteSpace (_school.CanteenUrl)) {
					DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoInfo);
					return;
				}
				controller = new WebNavigateSection (_school.CanteenUrl, "Canteen");
				break;
			case ActionMove.To_Calendar:
				controller = new CalendarSyncController ();
				break;
			case ActionMove.To_ReportAbsence:
				if (homeController._mySchool == null) {
					DialogHelpers.ShowMessage (EduAppsBaseAppConstants.MessageTitle, EduAppsBaseAppConstants.MessageNoSchool);
					return;
				}
				controller = new ReportAbsenceController ();
				break;
			case ActionMove.To_SchoolPolicy:
				controller = new DocumentsTypeSearcher (EduAppsBaseAppConstants.DocumentTypeSchoolPolicies,
					EduAppsBaseAppConstants.MenuSchoolPolicies, ImageConstants.IconSchoolPolicies, 
					"Search for a policy");
				break;
			case ActionMove.To_ContactDeveloper:

				GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "MFMailComposeViewController");
				GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());

				MFMailComposeViewController mailCompose = new MFMailComposeViewController ();
				mailCompose.SetToRecipients (new string[]{ EduAppsBaseAppConstants.DeveloperEmail });
				mailCompose.SetSubject ("");
				mailCompose.SetMessageBody ("", false);
				mailCompose.Finished += ( object s, MFComposeResultEventArgs args) => {
					args.Controller.DismissViewController (true, null);
				};
				homeController.ParentViewController.PresentViewController (mailCompose, true, null);
				return;
			}
		
			if ( controller != null )
				homeController.navigateViewController (controller);
		}

	}
}
