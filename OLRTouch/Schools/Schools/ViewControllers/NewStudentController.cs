using System;
using MobileUtilities.Forms;
using UIKit;
using GoogleAnalytics.iOS;
using RestSharp;

namespace Schools
{
	public class NewStudentController : ViewEditFormController<StudentDetails>
	{
		public NewStudentController () : base()
		{
			Title = "Student";
		}

		Entry _name = null;

		public long alreadyRegisteredSchoolId = -1;
		private CourseGroupSearcher _cgSearcher = null;
		private CourseSearcher _cSearcher = null;
		private School _mySchool;
		public override void SaveData ()
		{
			base.SaveData ();

			if (_item != null)
				_item.student.Name = _name.AsString;
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			var main = new Section ("", "");
			_name = main.AddString ("Name", "", "Name", false);
			_name.Value = _item.student.Name;
			sections.Add (main);

			var schoolsSection = new Section ("School");
			if (_item.studentSchool == null) {
				schoolsSection.AddButton ("Select a school").OnClick += (object sender, EventArgs e) => {

				};
			} else {
				// Show the School
				schoolsSection.AddNavigate (_item.studentSchool.SchoolName).Placeholder = _item.studentSchool.Address;
			}

			var courses = new Section ("Courses");

			// Load the courses that the student has enrolled in
			foreach (var c in _item.currentCourses) {
				// Add the list of subscribed courses
				var courseLink = courses.AddNavigate (c.Name);
				courseLink.Placeholder = c.Subject;
				courseLink.OnClick += (object sender, EventArgs e) => {
					// View the details of the course

				};
			}

			var btnCourses = courses.AddButton(EduAppsBaseAppConstants.AddClassesForText);
			sections.Add(courses);
			btnCourses.OnClick += (sender, e) => {
				// Show the Class List

				if (_mySchool == null)
				{
					new UIAlertView ( "Add Classes Failed", 
						"Add Classes needs to be connected to Internet at least for the first time for " + EduAppsBaseAppConstants.ApplicationName, 
						null, "OK", null).Show ();
					
				}else{
					_cSearcher = new CourseSearcher();
					_cSearcher.school = _mySchool;
					_cSearcher.coursegroup = new CourseGroup();
					_cSearcher.coursegroup.CourseGroupName = "Main";
					_cSearcher.coursegroup.Id = -1;
					_cSearcher.coursegroup.SchoolId = _mySchool.Id;
					_cSearcher.StudentDetails = _item; //null

					NavigationController.PushViewController(_cSearcher, true);
				}
			};
		}

		private void GetOLRSchool()
		{			
			_mySchool = School.getMyCurrentSchool ();

			if (_mySchool == null ) {
				var client = new RestClient (Host.GetSchoolDetailsPage(EduAppsBaseAppConstants.OLRSchoolId));
				var request = new RestRequest (Method.GET);
				client.ExecuteAsync (request, response => {
					try
					{
						var o = System.Json.JsonObject.Parse(response.Content);

						// Get the School Details
						_mySchool = JsonLoaders.LoadSchool(o as System.Json.JsonObject);
						using (var db = DataConnection.GetConnection())
						{
							LocalCache.StoreSchoolToCache(db, _mySchool);
						}
						Console.WriteLine ("Crest Url = " + _mySchool.CrestUrl);

					} catch (Exception ex){
						Console.WriteLine ("Exception = " + ex.Message);
					}
				});
			}
		}


		private void DoSaveDetails(object sender, EventArgs e)
		{
			_item.student.Name = _name.AsString;
			_item.SaveDetails();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Done, (sender, e) => {
				DoSaveDetails(this, EventArgs.Empty);
				DismissViewController (true, null);
			});
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "NewStudentController");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
			GetOLRSchool ();
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		SchoolSearcher ss;
	}


}

