using System;
using MobileUtilities.Forms;
using UIKit;
using System.Collections.Generic;
using Foundation;
using StoreKit;
using System.Threading;
using System.Json;

namespace Schools
{
	public class NonSubscribedAssessmentsViewController : ViewEditFormController<object>
	{
		public AssessmentsList AssessmentsViewController;
		List<string> _products;
		InAppPurchaseManager _iap;
		NSObject _priceObserver;

		public NonSubscribedAssessmentsViewController () : base()
		{
			Title = EduAppsBaseAppConstants.EntriesPlural;
			TabBarItem.Image = UIImage.FromFile (ImageConstants.Checkmark);

			// two products for sale on this page
			_products = new List<string>(new string[] { ProductNames.YearSubscription });
			_iap = new InAppPurchaseManager();
			_iap.OnSuccessful += HandleOnSuccessful;
			_iap.OnFailed += HandleOnFailed;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			_priceObserver = NSNotificationCenter.DefaultCenter.AddObserver (
				InAppPurchaseManager.InAppPurchaseManagerProductsFetchedNotification,  
				(notification) => {
						// Handle the details of the product price information
						var info = notification.UserInfo;
						if (info != null)
						{
							if (info.ContainsKey(ProductNames.YearSubscription)) {
								var product = (SKProduct) info.ObjectForKey(ProductNames.YearSubscription);
							
								_infoPricing.Label = product.LocalizedTitle;
								_infoPricing.Value = product.LocalizedDescription;
								_infoPricing.CellSizeSet = false;

								if (_iap.CanMakePayments())
								{
									_btnPurchase.SetButtonTitle("Buy for " + product.LocalizedPrice(), UIControlState.Normal);
									_btnPurchase.EnableButton();
									_btnRestore.SetButtonTitle("Restore Purchase");
									_btnRestore.EnableButton();
								}

								SetPurchaseVisibility();

								TableView.ReloadData();

							} else {
								// There is something wrong with the Store Kit detail that we are trying to download
								PurchasingNotAvailable();
							}	

							UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
						} else {
							// We don't have the product information
							PurchasingNotAvailable();
						}
					}	
			);	

			// Disable the buttons
			_btnPurchase.DisableButton();
			//_btnRestore.DisableButton();
			TableView.ReloadData();

			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

			if (_iap.CanMakePayments())
				_iap.RequestProductData(_products);
			else {
				PurchasingNotAvailable();
			}
		}

		public void PurchasingNotAvailable()
		{
			_btnRestore.SetButtonTitle("Restore unavailable");
			_btnRestore.DisableButton();
			_btnPurchase.SetButtonTitle("Purchasing unavailable");
			_btnPurchase.DisableButton();
			_infoPricing.Label = "";
			_infoPricing.Value = "Product information could not be retrieved";
			_infoPricing.CellSizeSet = false;
			TableView.ReloadData();
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (_priceObserver != null)
				NSNotificationCenter.DefaultCenter.RemoveObserver (_priceObserver);
		}

		Section _main, _purchase, _restore;
		Entry _btnPurchase, _infoPricing, _cannotBuy, _btnRestore, _restoreInfo, _tbEmail, _tbPassword;

		public override void SetupForm ()
		{
			base.SetupForm ();

			//_main = new Section("Details");
			//_main.AddInfoText("One year subscription", 
			//				  "In order to view assessments you need to purchase a subscription.");
			//sections.Add(_main);

			_purchase = new Section("Purchase Subscription");

			_infoPricing = _purchase.AddInfoText("Loading Product Details", "");
			_btnPurchase = _purchase.AddButton("Loading...");
			_btnPurchase.OnClick += DoSubscriptionPurchase;
			sections.Add(_purchase);

			_restore = new Section("Restore");
			_restoreInfo = _restore.AddInfoText("", "Already purchased a subscription? " + 
			                                    "If you have purchased and registered on another device " + 
			                                    "enter your registration details to then restore the subscription.");

			_tbEmail = _restore.AddString("Email", "", "Email", false);
			_tbPassword = _restore.AddPassword("Password", "", "Password", false);

			//_btnRestore = _restore.AddButton("Restore Subscription");
			_btnRestore = _restore.AddButton("Restore Purchase");
			_btnRestore.OnClick += DoPurchaseRestore;

			_cannotBuy = _purchase.AddInfoText("Cannot purchase", 
			                                   "In-App Purchases are disabled on your device. Edit Restrictions in Settings to allow In-App purchasing in order to purchase a subscription");

			SetPurchaseVisibility();
		}

		void SetPurchaseVisibility ()
		{
			_purchase.entries.Clear();

			if (_iap.CanMakePayments())
			{
				_purchase.entries.AddRange(new Entry[] { _infoPricing, _btnPurchase });

				if (!sections.Contains(_restore))
					sections.Add(_restore);
			} else {
				_purchase.entries.Add(_cannotBuy);
				sections.Remove(_restore);
			}

			//sections.Remove(_restore);

			TableView.ReloadData();
		}

		void DoSubscriptionPurchase (object sender, EventArgs e)
		{
			_btnPurchase.DisableButton();
			_btnRestore.DisableButton ();

			// Start the process of purchasing the product
			_iap.PurchaseProduct(ProductNames.YearSubscription);
		}

		void DoPurchaseRestore (object sender, EventArgs e)
		{
			if (_btnPurchase != null)
				_btnPurchase.DisableButton();

			if (_btnRestore != null)
				_btnRestore.DisableButton ();

			//_iap.RestoreApps();

			// Talk to the server and then try and gain access to the system
			_restoringStatus = new ShowProcessingAlertView("Restoring Subscription", "");
			_restoringStatus.Show();

			// Start the thread of restoring the details
			_entryUsername = _tbEmail.Value;
			_entryPassword = _tbPassword.Value;

			var thread = new Thread(DoDeviceRestore);
			thread.Start();
		}

		public class DeviceRestoreRequest
		{
			public string username;
			public string pwdHash;
		}

		void DoDeviceRestore()
		{
			// Create the object
			DeviceRestoreRequest details = new DeviceRestoreRequest();
			details.username = _entryUsername;
			details.pwdHash = _entryPassword;

			var hashedPassword = MD5Utility.Hash(_entryPassword);
			JsonObject o = new JsonObject();
			o.Add("EmailAddress", _entryUsername);
			o.Add("PwdHash", hashedPassword);
			o.Add("TransactionId", "");
			o.Add("DeviceType", 1);

			// Post the details to the server
			string url = Host.CombineSecure("/rest/Accounts.ashx/CheckAccount");
			var responseText = MobileUtilities.Network.Http.HttpsPost2(url, o.ToString(), "", "");

			// Hide the animation window
			BeginInvokeOnMainThread(delegate {
				_restoringStatus.Close();
			});

			var response = JsonObject.Parse(responseText);

			var transId = (string)response["TransactionId"];
			var expiry = (string)response["ExpiryDate"];
			var success = Convert.ToBoolean(response["Success"].ToString());
			var errorMessage = (string)response["ErrorMessage"];

			if (success)
			{
				// Store the account details
				using (var db = DataConnection.GetConnection())
				{
					var settings = GeneralSettings.GetSettings(db);

					// Store the transaction settings
					StoredSubscriptionDetails details2 = new StoredSubscriptionDetails();
					details2.platformType = "iOS";
					details2.transactionId = transId;
					details2.expiryDate = new DateTime(
						Convert.ToInt32(expiry.Substring(0, 4)),
			            Convert.ToInt32(expiry.Substring(4, 2)),
		                Convert.ToInt32(expiry.Substring(6, 2))
						);

					if (details2.expiryDate < DateTime.Now)
					{
						// We have to explain that there should no longer be a working system
						BeginInvokeOnMainThread(delegate {
							_btnRestore.EnableButton();
							DialogHelpers.ShowMessage("An error occurred logging in", "The Expiry Date for that account is no longer valid.");
						});
					} else {
						settings.SubscriptionTransaction = details2.Encrypt();

						// Store the account settings
						settings.StoreUserDetails(_entryUsername, hashedPassword);

						db.Update(settings);

						// Close the main screen and go to the assessments list
						BeginInvokeOnMainThread(delegate {
							HandleOnSuccessful(this, EventArgs.Empty);
						});
					}
				}
			} else {
				BeginInvokeOnMainThread(delegate {
					_btnRestore.EnableButton();
					DialogHelpers.ShowMessage("An error occurred logging in", errorMessage);
				});
			}
		}

		string _entryUsername, _entryPassword;

		ShowProcessingAlertView _restoringStatus = null;

		void HandleOnFailed (object sender, EventArgs e)
		{
			// Re-enable the button
			if (_btnPurchase != null)
				_btnPurchase.EnableButton();	

			if (_btnRestore != null)
				_btnRestore.EnableButton ();
		}
		
		void HandleOnSuccessful (object sender, EventArgs e)
		{
			// Close the application down and revert to the Assessment Screen
			if (NavigationController != null) {
				// Let the reminder go out
				AssessmentReload.Current.RequiresDataReload = true;

				NavigationController.PopViewController (true);
				//NavigationController.ViewControllers = new UIViewController[] {AssessmentsViewController};
			}
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}
	}
}

