﻿using System;
using MobileUtilities.Forms;
using UIKit;
using MessageUI;
using System.Drawing;
using CoreAnimation;
using Foundation;
using Factorymind.Components;
using SharpMobileCode.ModalPicker;
using MobileUtilities.Network;
using System.Threading;
using System.Net;
using System.IO;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;
using GoogleAnalytics.iOS;
using CoreGraphics;

namespace Schools
{
	public class ReportAbsenceController : UIViewController
	{
		School _item;
		public ReportAbsenceController () : base()
		{
			_item = School.getMyCurrentSchool ();
			Title = EduAppsBaseAppConstants.MenuReportAbsence;
			TabBarItem.Image = UIImage.FromFile (ImageConstants.IconReportAbsence);
		}

		UITextField inputStudentFirstName;
		UITextField inputStudentLastName;
		UITextField inputStudentClass;
		UITextField inputParentFirstName;
		UITextField inputParentLastName;
		UITextField inputParentEmail;
		UITextField inputParentMobile;

		UITextView inputReason;

		UIButton btnYes, btnNo, btnFrom, btnTo;

		bool hasCertificate = true;
		NSDate dateFrom, dateTo;

		float topGlobal = 10;
		float topInfo = 10;
		float heightLabel = 22;
		float heightInput = 28; 
		float heightSpacer = 5;
		float heightDatePicker = 200;
		UIScrollView scrollView ;
		UIView infoView;
		NSDateFormatter fullFormatter, dateFormatter;

		UIBarButtonItem btnDone, btnSubmit;

		UIImage imageOn, imageOff;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			fullFormatter = new NSDateFormatter () {
				DateFormat = "MMMM dd, yyyy"
			};

			dateFormatter = new NSDateFormatter () {
				DateFormat = "MM/dd/yyyy"
			};

			View.BackgroundColor = UIColor.White;


			scrollView = new UIScrollView (new RectangleF(0,0,320,View.Frame.Height - NavigationController.NavigationBar.Frame.Height));
			Add (scrollView);
			//scrollView.Frame = scrollView.Superview.Bounds; 

			UILabel title = new UILabel ( new RectangleF (10, topGlobal, 300, 30));
			title.Text = "Absentee Note";
			scrollView.Add (title);

			topGlobal += 30 + heightSpacer; 

			UILabel info1 = new UILabel (new RectangleF (10, topGlobal, 300, 40));
			info1.Text = "Please complete this form if your child has been sick or otherwise absent from school.";
			info1.Lines = 0;
			info1.LineBreakMode = UILineBreakMode.WordWrap;
			scrollView.Add (info1);

			topGlobal +=40;

			UILabel info2 = new UILabel (new RectangleF (10, topGlobal, 300, 30));
			info2.Text = "This school office may contact you to verify details.";
			info2.Lines = 0;
			info2.LineBreakMode = UILineBreakMode.WordWrap;
			info2.TextColor = UIColor.FromRGB (0.9f, 0.2f, 0.2f);
			scrollView.Add (info2);

			topGlobal += 30 + heightSpacer;

			infoView = new UIView (new RectangleF (10, topGlobal, 300, 1000));

			topInfo = 10;

			UILabel lblStudentFirstName = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblStudentFirstName.Text = "Student First Name:";
			infoView.Add (lblStudentFirstName);

			topInfo += heightLabel + heightSpacer;

			inputStudentFirstName = new UITextField (new RectangleF (10, topInfo, 280, heightInput));
			infoView.Add (inputStudentFirstName);

			topInfo += heightInput + heightSpacer;

			UILabel lblStudentLastName = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblStudentLastName.Text = "Student Last Name:";
			infoView.Add (lblStudentLastName);

			topInfo += heightLabel + heightSpacer;

			inputStudentLastName = new UITextField (new RectangleF (10, topInfo, 280, heightInput));
			infoView.Add (inputStudentLastName);

			topInfo += heightInput + heightSpacer;

			UILabel lblStudentClass = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblStudentClass.Text = "Student Class/Year:";
			infoView.Add (lblStudentClass);

			topInfo += heightLabel + heightSpacer;

			inputStudentClass = new UITextField (new RectangleF (10, topInfo, 280, heightInput));
			infoView.Add (inputStudentClass);

			topInfo += heightInput + heightSpacer;

			UILabel lblParentFirstName = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblParentFirstName.Text = "Parent/Carer First Name:";
			infoView.Add (lblParentFirstName);

			topInfo += heightLabel + heightSpacer;

			inputParentFirstName = new UITextField (new RectangleF (10, topInfo, 280, heightInput));
			infoView.Add (inputParentFirstName);

			topInfo += heightInput + heightSpacer;


			UILabel lblParentLastName = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblParentLastName.Text = "Parent/Carer Surname:";
			infoView.Add (lblParentLastName);

			topInfo += heightLabel + heightSpacer;

			inputParentLastName = new UITextField (new RectangleF (10, topInfo, 280, heightInput));
			infoView.Add (inputParentLastName);

			topInfo += heightInput + heightSpacer;

			UILabel lblParentEmail = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblParentEmail.Text = "Parent/Carer Email Address:";
			infoView.Add (lblParentEmail);

			topInfo += heightLabel + heightSpacer;

			inputParentEmail = new UITextField (new RectangleF (10, topInfo, 280, heightInput));
			infoView.Add (inputParentEmail);

			topInfo += heightInput + heightSpacer;


			UILabel lblParentMobile = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblParentMobile.Text = "Parent/Carer Mobile No:";
			infoView.Add (lblParentMobile);

			topInfo += heightLabel + heightSpacer;

			inputParentMobile = new UITextField (new RectangleF (10, topInfo, 280, heightInput));
			infoView.Add (inputParentMobile);

			topInfo += heightInput + heightSpacer;

			UILabel lblDateFrom = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblDateFrom.Text = "Absent or Sick Date From:";
			infoView.Add (lblDateFrom);

			topInfo += heightLabel + heightSpacer;

			//            pickerDateFrom = new UIDatePicker ( new RectangleF(10, topInfo, 280, heightDatePicker));
			//            infoView.Add (pickerDateFrom);
			dateFrom = NSDate.Now;
			btnFrom = new UIButton (UIButtonType.System);
			btnFrom.Frame = new RectangleF (30, topInfo, 220, heightInput);
			btnFrom.SetTitle (fullFormatter.ToString(dateFrom), UIControlState.Normal);
			btnFrom.SetTitleColor (UIColor.Black, UIControlState.Normal);
			btnFrom.TintColor = UIColor.Blue;
			btnFrom.SetImage (imageOn, UIControlState.Normal);
			btnFrom.TouchUpInside += delegate {
				DoSelectDate(true);
			};
			infoView.Add (btnFrom);

			topInfo += heightInput + heightSpacer;


			UILabel lblDateTo = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblDateTo.Text = "Absent or Sick Date To:";
			infoView.Add (lblDateTo);

			topInfo += heightLabel + heightSpacer;


			//            pickerDateTo = new UIDatePicker (new RectangleF (10, topInfo, 280, heightDatePicker));
			//            infoView.Add (pickerDateTo);
			dateTo = NSDate.Now;
			btnTo = new UIButton (UIButtonType.System);
			btnTo.Frame = new RectangleF (30, topInfo, 220, heightInput);
			btnTo.SetTitle (fullFormatter.ToString(dateTo), UIControlState.Normal);
			btnTo.SetTitleColor (UIColor.Black, UIControlState.Normal);
			btnTo.TintColor = UIColor.Blue;
			btnTo.SetImage (imageOn, UIControlState.Normal);
			btnTo.TouchUpInside += delegate {
				DoSelectDate(false);
			};
			infoView.Add (btnTo);

			topInfo += heightInput + heightSpacer;

			UILabel lblReason = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblReason.Text = "Absent or Sick Reason:";
			infoView.Add (lblReason);

			topInfo += heightLabel + heightSpacer;

			inputReason = new UITextView (new RectangleF (10, topInfo, 280, heightDatePicker));

			infoView.Add (inputReason);


			topInfo += heightDatePicker + heightSpacer;

			UILabel lblCertificate = new UILabel (new RectangleF (10, topInfo, 280, heightLabel));
			lblCertificate.Text = "Has Doctor Certificate:";
			infoView.Add (lblCertificate);

			topInfo += heightLabel + heightSpacer;

			UIView radioView = new UIView (new RectangleF (10, topInfo, 280, heightInput));

			imageOn = UIImage.FromFile ("finalicons/radio_on.png");
			imageOff = UIImage.FromFile ("finalicons/radio_off.png");


			btnYes = new UIButton (UIButtonType.Custom);
			btnYes.Frame = new RectangleF (30, 0, 90, heightInput);
			btnYes.SetTitle ("  Yes", UIControlState.Normal);
			btnYes.SetTitleColor (UIColor.Black, UIControlState.Normal);
			btnYes.SetImage (imageOn, UIControlState.Normal);
			btnYes.TouchUpInside += delegate {
				DoSelectCertificate(true);
			};

			btnNo = new UIButton (UIButtonType.Custom);
			btnNo.Frame = new RectangleF (160, 0, 90, heightInput);
			btnNo.SetTitle ("  No", UIControlState.Normal);
			btnNo.SetTitleColor (UIColor.Black, UIControlState.Normal);
			btnNo.SetImage (imageOff, UIControlState.Normal);
			btnNo.TouchUpInside += delegate {
				DoSelectCertificate(false);
			};

			radioView.Add (btnYes);
			radioView.Add (btnNo);

			infoView.Add (radioView);


			topInfo += heightInput;

			infoView.Frame = new RectangleF (10, topGlobal, 300, topInfo + 10);
			infoView.BackgroundColor = UIColor.LightGray;
			scrollView.Add (infoView);

			topGlobal += topInfo + 10;

			// Add the buttons
			/*UIButton btnSubmit = App.GetNormalButton ("Submit", topGlobal);
            btnSubmit.BackgroundColor = UIColor.Clear;
            scrollView.Add (btnSubmit);

            btnSubmit.TouchUpInside += DoSubmitReport;*/

			scrollView.ContentSize = new SizeF (320, topGlobal + 30);

			title.Font = UIFont.FromName (Theme.TextFontName, Theme.TextFontSize);

			foreach (UILabel l in new UILabel[] { info1,info2,lblStudentFirstName,lblStudentLastName,lblStudentClass,
				lblParentFirstName,lblParentLastName, lblParentEmail, lblParentMobile,
				lblDateFrom,lblDateTo,lblReason,lblCertificate })
				l.Font = UIFont.FromName (Theme.SubTextFontName, Theme.SubTextFontSize);

			foreach (UITextField field in new UITextField[] { inputStudentFirstName,inputStudentLastName,
				inputStudentClass,inputParentFirstName,inputParentLastName,
				inputParentEmail,inputParentMobile}) {
				field.Font = UIFont.FromName (Theme.TextFontName, Theme.TextFontSize);
				field.BackgroundColor = UIColor.FromRGB (0.99f, 0.99f, 0.99f);
				field.LeftViewMode = UITextFieldViewMode.Always;
				field.RightViewMode = UITextFieldViewMode.Always;
				field.BorderStyle = UITextBorderStyle.RoundedRect;
				field.ShouldReturn += (textField) => {
					textField.ResignFirstResponder();
					return true;
				};
			}

			inputReason.Font = UIFont.FromName (Theme.TextFontName, Theme.TextFontSize);
			inputReason.BackgroundColor = UIColor.FromRGB (0.99f, 0.99f, 0.99f);
			inputReason.ShouldBeginEditing = (textField) => {
				NavigationItem.RightBarButtonItem = btnDone;
				return true;
			};
			inputReason.ShouldEndEditing = (textField) => {
				NavigationItem.RightBarButtonItem = btnSubmit;
				return true;
			};

			#region add gesture to hide keyboard

			var gesture = new UITapGestureRecognizer(() => {
				inputStudentFirstName.ResignFirstResponder();
				inputStudentLastName.ResignFirstResponder();
				inputStudentClass.ResignFirstResponder();
				inputParentFirstName.ResignFirstResponder();
				inputParentLastName.ResignFirstResponder();
				inputParentEmail.ResignFirstResponder();
				inputParentMobile.ResignFirstResponder();
				inputReason.ResignFirstResponder();
			});
			infoView.AddGestureRecognizer(gesture); //check here 

			#endregion

			btnDone = new UIBarButtonItem ("Done", UIBarButtonItemStyle.Plain, DoFinishEditing);
			btnSubmit = new UIBarButtonItem ("Submit", UIBarButtonItemStyle.Plain, DoSubmitReport);

			NavigationItem.RightBarButtonItem = btnSubmit;
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "ReportAbsenceController");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}


		public void DoSelectDate(bool isFrom) {
			View.EndEditing (true);
			var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Select Date", this){
				HeaderBackgroundColor = UIColor.Blue,
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.DatePicker.Mode = UIDatePickerMode.Date;
			modalPicker.OnModalPickerDismissed += (sender, e) =>  {
				if ( isFrom ) {
					dateFrom = modalPicker.DatePicker.Date;
					btnFrom.SetTitle( fullFormatter.ToString(dateFrom), UIControlState.Normal);
				} else {
					dateTo = modalPicker.DatePicker.Date;
					btnTo.SetTitle( fullFormatter.ToString(dateTo), UIControlState.Normal);
				}
			};

			PresentViewController (modalPicker, true, null);
		}

		public void DoSelectCertificate(bool has) {
			hasCertificate = has;
			btnYes.SetImage (has ? imageOn : imageOff, UIControlState.Normal);
			btnNo.SetImage (has ? imageOff : imageOn, UIControlState.Normal);
		}

		public class AbsenceReportDetail
		{
			public string StudentFirstName;
			public string StudentLastName;
			public string Class ;
			public string ParentFirstName ;
			public string ParentLastName ;
			public string ParentEmail ;
			public string ParentPhone ;
			public string DateFrom ;
			public string DateTo ;
			public string Reason ;
			public Boolean DoctorCertificate ;
			public long SchoolId;
		}

		string jsonReport = "";
		ShowProcessingAlertView alertView = null;

		public void DoSubmitReport (object sender, EventArgs e)
		{
			View.EndEditing (true);

			AbsenceReportDetail detail = new AbsenceReportDetail ();
			detail.StudentFirstName = inputStudentFirstName.Text;
			detail.StudentLastName = inputStudentLastName.Text;
			detail.Class = inputStudentClass.Text;
			detail.ParentFirstName = inputParentFirstName.Text;
			detail.ParentLastName = inputParentLastName.Text;
			detail.ParentEmail = inputParentEmail.Text;
			detail.ParentPhone = inputParentMobile.Text;
			detail.DateFrom = dateFormatter.ToString(dateFrom);
			detail.DateTo = dateFormatter.ToString(dateTo);
			detail.Reason = inputReason.Text;
			detail.DoctorCertificate = hasCertificate;
			detail.SchoolId = _item.Id;

			jsonReport = getPostData(detail);//JsonUtilities.Serialize<AbsenceReportDetail> (detail);

			if ( String.IsNullOrWhiteSpace ( detail.StudentFirstName) ||
				String.IsNullOrWhiteSpace ( detail.StudentLastName) ||
				String.IsNullOrWhiteSpace ( detail.Class) ||
				String.IsNullOrWhiteSpace ( detail.ParentFirstName) ||
				String.IsNullOrWhiteSpace ( detail.ParentLastName) ||
				String.IsNullOrWhiteSpace ( detail.ParentEmail) ||
				String.IsNullOrWhiteSpace ( detail.ParentPhone) ||
				String.IsNullOrWhiteSpace ( detail.DateFrom) ||
				String.IsNullOrWhiteSpace ( detail.DateTo) ||
				String.IsNullOrWhiteSpace ( detail.Reason) 
				//||String.IsNullOrWhiteSpace ( detail.DoctorCert) 
			) {
				DialogHelpers.ShowMessage ("", "Please complete all information");
				return;
			}

			if (!detail.ParentEmail.Contains ("@")) {
				DialogHelpers.ShowMessage ("", "Please enter the valid email");
				return;
			}

			alertView = new ShowProcessingAlertView ("", "Sending Report...");
			alertView.Show ();

			Console.WriteLine ("Absence Response : " + jsonReport);

			new Thread (DoSendAbsenceReport).Start();
		}

		public String formatTime(string s) {
			return s.Substring (0, 2) + "/" + s.Substring (3, 2) + "/" + s.Substring (6);
		}

		public String getPostData(AbsenceReportDetail detail) {
			String result = "";
			result += "SchoolId=" + detail.SchoolId + "&";
			result += "ParentFirstName=" + detail.ParentFirstName + "&";
			result += "ParentLastName=" + detail.ParentLastName + "&";
			result += "ParentEmail=" + detail.ParentEmail + "&";
			result += "ParentPhone=" + detail.ParentPhone + "&";
			result += "StudentFirstName=" + detail.StudentFirstName + "&";
			result += "StudentLastName=" + detail.StudentLastName + "&";
			result += "Class=" + detail.Class + "&";
			result += "DateFrom=" +  System.Net.WebUtility.UrlEncode(detail.DateFrom) + "&";
			result += "DateTo=" +  System.Net.WebUtility.UrlEncode(detail.DateTo) + "&";
			result += "Reason=" +  System.Net.WebUtility.UrlEncode(detail.Reason) + "&";
			result += "DoctorCertificate=" + detail.DoctorCertificate ;
			return result;
		}


		public void DoSendAbsenceReport() {
			var wc = new System.Net.WebClient();

			var request = WebRequest.Create (Host.GetSchoolAbsenceURL ());

			byte[] data = System.Text.Encoding.ASCII.GetBytes(jsonReport);

			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = data.Length;

			var requestStream = request.GetRequestStream ();

			requestStream.Write (data, 0, data.Length);
			requestStream.Close();

			var response = request.GetResponse ();
			var responseStream = response.GetResponseStream ();

			System.IO.StreamReader reader = new System.IO.StreamReader (responseStream);

			string result = reader.ReadToEnd ();
			Console.WriteLine ("Absence Response : " + result);

			String message = "";
			try
			{
				if ( String.IsNullOrWhiteSpace(result) ) {
					message = "Network connection error.";
				}
				else {
					var o = System.Json.JsonObject.Parse(result);
					message = o["ErrorMessage"];
				}

			} catch {
				message = "Bad response from server";
			}

			if ( message.Equals("Success") ) {
				message = "Absence report is submitted successfully.";
			}
			BeginInvokeOnMainThread(delegate {
				DialogHelpers.ShowMessage("", message);
				alertView.Close ();
			});
		}

		public void DoFinishEditing(object sender, EventArgs e) {
			NavigationItem.RightBarButtonItem = btnSubmit;
			View.EndEditing (true);
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			View.EndEditing (true);
		}

		//        bool moveViewUp = false;
		//        UIView activeview;
		//        nfloat bottom = 0f;
		//        nfloat offset = 0f;
		//        nfloat scroll_amount = 0f;
		//
		//        private void KeyBoardUpNotification(NSNotification notification)
		//        {
		//            // get the keyboard size
		//            RectangleF r = UIKeyboard.BoundsFromNotification (notification);
		//
		//            // Find what opened the keyboard
		//            activeview = null;
		//            foreach (UIView view in infoView.Subviews) {
		//                if (view.IsFirstResponder)
		//                    activeview = view;
		//            }
		//
		//
		//            // Calculate how far we need to scroll
		//            scroll_amount = r.Height ;
		//
		//            // Perform the scrolling
		//            if (scroll_amount > 0) {
		//                moveViewUp = true;
		//                ScrollTheView (moveViewUp);
		//            } else {
		//                moveViewUp = false;
		//            }
		//        }
		//
		//        private void KeyBoardDownNotification(NSNotification notification)
		//        {
		//            if(moveViewUp){ScrollTheView(false);}
		//        }
		//
		//        private void ScrollTheView(bool move)
		//        {
		//            if (activeview == null) {
		//                return;
		//            }
		//            // scroll the view up or down
		//
		//            RectangleF frame = scrollView.Frame;
		//
		//            if (move) {
		//                frame.Height -= scroll_amount;
		//            } else {
		//                frame.Height += scroll_amount;
		//                scroll_amount = 0;
		//            }
		//            scrollView.Frame = frame;
		//            RectangleF fieldFrame = activeview.Frame;
		//            fieldFrame.Height += 50;
		//            scrollView.ScrollRectToVisible (activeview.Frame, true);
		//        }
		//
		//        public UILabel GenerateLabel(string label, int top) {
		//            return null;
		//        }

		#region keyboard part

		protected     bool         moveViewUp = false;
		protected     nfloat         scroll_amount = 0.0f;
		protected     nfloat         bottom = 0.0f;
		protected     nfloat         offset = 10.0f;
		protected      NSObject     keyboardUpNotificationToken;
		protected     NSObject     keyboardDownNotificationToken;

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			keyboardUpNotificationToken = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, KeyBoardUpNotification);
			keyboardDownNotificationToken = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardDownNotification);

		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			if (keyboardUpNotificationToken != null)
				NSNotificationCenter.DefaultCenter.RemoveObserver(keyboardUpNotificationToken);
			if (keyboardDownNotificationToken != null)
				NSNotificationCenter.DefaultCenter.RemoveObserver(keyboardDownNotificationToken);
		}


		protected void KeyBoardDownNotification(NSNotification notification)
		{
			if (moveViewUp) { ScrollTheView(false); }
			moveViewUp = false;
		}


		private void KeyBoardUpNotification(NSNotification notification)
		{
			if (moveViewUp)
				return;

			UITextField targetTextField = null;
			UITextView    targetTextView = null;
			foreach (UIView subview in this.infoView) {

				if (subview.IsFirstResponder) {
					if (subview is UITextField) {
						targetTextField = subview as UITextField;
					} else if (subview is UITextView) {
						targetTextView = subview as UITextView;
					}
				}
			}
			UIView activeField;
			if (targetTextField != null) {
				activeField = targetTextField;
			} else if (targetTextView != null)
				activeField = targetTextView;
			else
				return;

			//get Max View height
			var ParentViewHeight = infoView.Frame.Y + infoView.Frame.Height + NavigationController.NavigationBar.Frame.Height;
			//get the keyboard size
			CGRect r = UIKeyboard.BoundsFromNotification(notification);

			//get y pos to show in View
			var Pos2Display = View.Frame.Height - r.Height + offset;

			//get target view botton in infoview
			var TargetBottominParent = infoView.Frame.Height - activeField.Frame.Y - activeField.Frame.Height;

			//Calculate how fore we need to scroll
			scroll_amount = ParentViewHeight - Pos2Display - TargetBottominParent - scrollView.ContentOffset.Y;
			//Perform the scrolling
			if (scroll_amount > 0)
			{
				moveViewUp = true;
				ScrollTheView(moveViewUp);
			}
			else
			{
				moveViewUp = false;
			}
		}

		protected void ScrollTheView(bool move)
		{
			UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration(0.3);

			CGRect frame = infoView.Frame;
			if (move)
			{
				frame.Y -= scroll_amount;
			}
			else
			{
				frame.Y += scroll_amount;
				scroll_amount = 0;
			}

			infoView.Frame = frame;
			UIView.CommitAnimations();
		}

		#endregion

	}
}
