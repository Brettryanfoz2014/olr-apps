using System;
using CoreGraphics;
using MobileUtilities.Forms;
using UIKit;

namespace Schools
{
	public class NoticeDetailForm : ViewEditFormController<Notice>
	{
		public NoticeDetailForm () : base()
		{
			Title = "Notice";
		}
		
		public override void SetupForm ()
		{
			Section general = new Section ("Details");
			general.AddInfoText(_item.GetLocalMessageDate ().ToShortDateString() + " " + _item.GetLocalMessageDate ().ToShortTimeString(), _item.NoticeMessage);
			sections.Add (general);
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}
	}
}


